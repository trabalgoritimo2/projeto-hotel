/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#include <stdio.h>
#include <stdlib.h>
#include "views/usuarioView.h"
#include "structs/moduloCadastroStructs.h"
#include "models/configuracaoModel.h"

int main(){
    Usuario user;
    int tipoArquivo = verificaOpcaoArmazenamentoUsuario();
    if(tipoArquivo != 0){
        //Criando os arquivos necessários
        criarArquivos(tipoArquivo);
        //Criando arquivos que armazena as sequencias dos registros
        criarArquivosSeq(tipoArquivo);
        //Criando o usuário administrador
        criarUsuarioAdm(tipoArquivo);
        //Chamando a tela de login do sistema até o usuario fazer login
        do {
            user = login();
            if (user.usuCodigo == -1){
                printf("Usuário e/ou senha inválidos. Tente novamente \n");
            }
        } while(user.usuCodigo == -1);
        mostrarMenu(user);
        return 0;
    }
    return 1;
}