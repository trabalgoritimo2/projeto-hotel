PROJ_NAME = projeto_hotel
CC = gcc
FLAGS = -c
C_SOURCE=$(wildcard ./controllers/*.c)
H_SOURCE=$(wildcard ./views/*.h) $(wildcard ./models/*.h) $(wildcard )
OBJ=$(subst .c,.o,$(subst controllers,objects,$(C_SOURCE))) objects/main.o

buildNtest: build test

build: $(PROJ_NAME)

all: $(PROJ_NAME)

$(PROJ_NAME): $(OBJ)
	@ echo 'Building binaries'
	$(CC) $^ -o $@

./objects/%.o: ./controllers/%.c $(H_SOURCE)
	@ echo 'Building objects: $<'
	$(CC) $(FLAGS) $< -o $@

./objects/main.o: main.c $(H_SOURCE)
	@ echo 'Building main object'
	$(CC) $(FLAGS) $< -o $@

objFolder:
ifeq ($(OS),Windows_NT)
	if not exist ./objects mkdir objects
else
	mkdir -p objects
endif

clean:
ifeq ($(OS),Windows_NT)
	if exist objects\*.o del objects\*.o
	if exists projeto_hotel.exe del projeto_hotel.exe
	if exists projeto_hotel del projeto_hotel
else
	rm ./objects/*.o
	rm projeto_hotel
	rm projeto_hotel.exe
endif

test:
ifeq ($(OS),Windows_NT)
	./projeto_hotel.exe
else
	./projeto_hotel
endif
