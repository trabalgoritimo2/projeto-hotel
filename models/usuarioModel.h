/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#pragma once 
#include "../structs/moduloCadastroStructs.h"

/**
* Tela do Menu Principal
* @param Usuario logado no sistenma
* @return void
*/
void mostrarMenu (Usuario user);

/**
* Busca o usuário por usuUsuario
* @param char *usuUsuario: ponteiro usuUsuario
* @return usuario encontrado com o usuUsuario desejado
*/
Usuario buscarUsuarioPorNomeUsuario(char *usuUsuario);


/**
* Cadastrar usuário na base de dados
* @param Usuario usuario: usuário a ser cadastrado
* @return 1- se cadastrou com sucesso 0: se houve algum problema
*/
int cadastrarUsuario(Usuario usuario);

/**
* Obtém todos os usuários armazenados no sistema
* @param
* @return Usuario
*/
Usuario *listarUsuarios();

/**
* Apaga o usuário de acordo com o usuUsuario
* @param char *usuUsuario: ponteiro usuUsuario
* @return 1- se deletou com sucesso 0: se houve algum problema
*/
int deletarUsuario(char *usuUsuario);

/**
* Atualiza os dados do usuário
* @param Usuario usuario: usuario a ser atualizado
* @return usuário atualizado
*/
Usuario atualizarUsuario(Usuario usuario, char *usuUsuario);

/**
* Quantidade de usuários cadastrados no sistema
* @param 
* @return int: quantidade de usuários cadastrados no sistema
*/
int qtdeRegistrosUsuario();

/**
* Abre menu do usuário
* @param
* @return
*/
void abrirMenuUsuario ();