/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#pragma once

/**
 * Tela de cadastrar produto
 * @param hospede Objeto para salvar
 * @return int  valor para tratamento de erro
 */
void produtos();

/**
 * Tela de cadastrar produto
 * @param hospede Objeto para salvar
 * @return int  valor para tratamento de erro
 */
int cadastrarProduto();

/**
 * Realiza a exclusão de um produto;
 * @param 
 * @return int  //retorna 1 se o cod do produto NAO existe ainda; 0- se não existe
 */
int deletarProduto();

/**
 * Realiza a edição de um produto;
 * @param 
 * @return 
 */
int editar_produto();

/**
 * Realiza a pesquisa de produtos
 * @param 
 * @return 
 */
void pesquisar_produto();

/**
 * Tela de listagem dos produtos cadastrados no sistema
 * @param 
 * @return 
 */
void listarProdutos();

/**
 * Salva o produto em arquivo (txt/bin)
 * @param 
 * @return 
 */
int salvar_produtos(Prod_Disp produto);

/**
 * Busca todos os produtos cadastrados no sistema
 * @param 
 * @return uma lista com os produtos cadastrados no sistema
 */
Prod_Disp *carregar_produtos();

/**
* Obtém a quantidade de registros gravados nos arquivos de produto
* @param
* @return int: quantidade de registros 
*/
int qtdeRegistrosProduto();

/**
* Busca o produto por codigo
* @param char codigo: codigo do produto
* @return produto encontrado com o código desejado
*/
Prod_Disp buscarProdutoPorCodigo(int codigo);

/**
* Salva a entrada de produtos industrilizados no hotel: atualiza caixa, estoque
* @param Entrada_Produtos_Item *itens: lista com os itens do produto
* @param int qtdeItens: quantidade de itens de produto em cada entrada
* @param int qtdeTotal: quantidade de produtos cadastrados
* @param Entrada_Produtos entrada: dados da entrada de produtos
* @param Pagamento pagamento: dados com a forma de pagamento
* @param Fornecedor fornecedor: dados do fornecedor dos produtos
* @param char *nomeFantasia: nome fantasia do hotel
* @return 1- se foi salvo com sucesso 0: erro ao executar a ação
*/
int salvarEntradaProdutos(Entrada_Produtos_Item *itens, int qtdeItens, int qtdeTotal, Entrada_Produtos entrada, Pagamento pagamento, Fornecedor fornecedor, char *nomeFantasia);


/**
* Lança pagamentos e/ou contas a pagar
* @param Entrada_Produtos entrada: dados da entrada de produtos
* @param Pagamento pagamento: dados da forma de pagamento
* @return 
*/
void montarContasPagar(Entrada_Produtos entrada, Pagamento pagamento);

/**
* Monta e printa na tela nota fiscal da entrada de produtos
* @param Entrada_Produtos_Item *itens: lista com os itens do produto
* @param int qtdeItens: quantidade de itens de produto em cada entrada
* @param Entrada_Produtos entrada: dados da entrada de produtos
* @param Fornecedor fornecedor: dados do fornecedor dos produtos
* @return 
*/
void montarNotaFiscal(Entrada_Produtos_Item *itens, int qtde, Entrada_Produtos entrada, Fornecedor fornecedor);

int ReduzProdutos(int id, int qte);