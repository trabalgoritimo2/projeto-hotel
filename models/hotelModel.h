/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#pragma once


/*
* Tela de cadastro do hotel
* @param
* @return
*/
int cadastrar_hotel();

/*
* Cadastra informações referentes ao endereço do hotel
* @param
* @return
*/
void dados_endereco(Hotel hotel);

/*
* Lista todos os dados do hoteç
* @param
* @return
*/
void listar_hotel();

/**
* Busca informações do hotel de acordo com a escolha do usuário
* @param
* @return
*/
Hotel buscarHotelPorNomeFantasia(char *nomeFantasia);

/**
* Tela de edição do cadastro do hotel
* @param
* @return
*/
void editar_hotel();

/**
* Salva as informações em arquivo binário ou txt
* @param Hotel hotel : hotel a ser salvo
* @return
*/
void salvar_hotel(Hotel hotel);

/**
* Exibe todas as informações cadastradas sobre o hotel
* @param
* @return array com hoteis cadastrados
*/
Hotel *carregar_hotel();

/**
* Obtém a quantidade de registros gravados nos arquivos do hotel
* @param
* @return int: quantidade de registros 
*/
int qtdeRegistrosHotel();


/**
* Printa menu com informações do hotel
* @param
* @return
*/
void informacoesHotel();


/**
* Atualiza os dados do hotel
* @param Usuario usuario: usuario a ser atualizado
* @return 1- se atualizar hotel com sucesso 0: se der algum erro
*/
int atualizarHotel(Hotel hotel);