/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#pragma once
#include "../structs/moduloCadastroStructs.h"

/*
* Mostra a mensagem de erro padrão ao tentar abrir o arquivo
*/
void mostrarMensagemErroArquivo ();

/*
* Mostra a mensagem de erro padrão para quando o usuário escolher um tipo de arquivo inválido
*/
void mostrarMensagemErroTipoArquivo ();

/*
* Mostra a mensagem de erro padrão para quando o usuário escolher uma opção inválida no menu
*/
void mostrarMensagemErroMenu ();

/*
* Mostra a mensagem de erro padrão para quando o registro já existe no armazenamento do sistema
*/
void mostrarMensagemRegistroExiste ();

/*
* Mostra a mensagem de erro padrão para quando quiser realizar uma ação sem estar logado no sistema
*/
void mostrarMensagemUsuarioDeslogado ();

/*
* Mostra a mensagem de retornando menu
*/
void mostrarRetornandoMenu();


int sequenceArquivoBinario(char nomeArquivo[]);

int sequenceArquivoTXT(char nomeArquivo[]);

/**
* Compara se uma data é posterior ou anterior a outra
* @param Data data1: data de comparação
* @param Data data2: data de referencia
* @return -1 se data1 é anterior a data2, 1 se data1 é posterior, 0 se elas são iguais
*/
int compararDatas(Data data1, Data data2);

/**
* Compara se 2 datas coincidem
* @param Data data1Inicio: a data inicial do "primeiro" periodo de tempo
* @param Data data1Fim: a data final do "primeiro" periodo de tempo
* @param Data data2Inicio: a data inicial do "segundo" periodo de tempo
* @param Data data2Fim: a data final do "segundo" periodo de tempo
* @return 1 se elas coincide, 0 se não há conflito
*/
int dataPeriodoCoincide(Data data1Inicio, Data data1Fim, Data data2Inicio, Data data2Fim);

/*
* Recebe input para uma string (usa fgets em vez de scanf para ser mais segura)
*/
void inputString(char s[], int len);

/*
* cria e retorna uma estrutura DataTime com todos os valores 0
*/
DataTime dataTimeZero();

/*
* gera um objeto Data preenchido com o tempo atual
*/
Data gerarDataAtual();

/*
* gera um objeto DataTime preenchido com o tempo atual
*/
DataTime gerarDataEHoraAtual();

/**
* Calcula quantos dias há entre di e df
* di DEVE ser anterior a df, caso não, a função nunca termina
* @param Data di: Data inicial
* @param Data df: Data final
* @return: quantidade de dias
*/
int calculaDiasEntreDatas(DataTime dataInicial, DataTime dataFinal);
