/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#pragma once

// enum para diferenciar os retornos da função opcaoArmazenamentoUsuario()
enum tipoArquivos { TipoArquivoBinario = 1, TipoArquivoTexto = 2 };

/**
* Cria os arquivos de armazenamento de todo o sistema de acordo com o tipo de arquivo
* @param int tipoArquivo: 1: Binário 2: TXT
* @return
*/
void criarArquivos(int tipoArquivo);

/**
* Cria o usuário administrador com permissão à todos os módulos
* @param int tipoArquivo: 1: Binário 2: TXT
* @return
*/
void criarUsuarioAdm(int tipoArquivo);

/**
* Cria os arquivos de armazenamento das sequencias de todo o sistema de acordo com o tipo de arquivo
* @param int tipoArquivo: 1: Binário 2: TXT
* @return
*/
void criarArquivosSeq(int tipoArquivo);

/**
* Verifica qual o tipo de arquivo que o usuário escolheu para armazenar os dados do sistema
* E se ele ainda não escolheu, mostra a tela para o usuário escolher
* @param
* @return int tipoArquivo: 1: Binário 2: TXT
*/
int verificaOpcaoArmazenamentoUsuario();

/**
* Verifica qual o tipo de arquivo que o usuário escolheu para armazenar os dados do sistema
* @param
* @return int tipoArquivo: 1: Binário 2: TXT
*/
int opcaoArmazenamentoUsuario();
