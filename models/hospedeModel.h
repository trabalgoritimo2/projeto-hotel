/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#include <stdio.h>
#include <stdlib.h>
#include "../structs/moduloCadastroStructs.h"


/**
 * Cadastrar hóspede - Chamando função
 * @param hospede Objeto para salvar
 * @return int  valor para tratamento de erro
 */
int cadastrarHospContoller(Hospede hospede);

/**
 * Atualiza hóspede - Chamando função
 * @param hospede Objeto para atualizar
 * @param cod código do hóspede a ser atualizado
 * @return int  valor para tratamento de erro
 */
int atualizarHospedeController(int cod, Hospede hospede);

/**
 * Lista todos os hóspedes cadastrados no sistema - Chamando função
 * @param
 * @return
 */
void listarHospController();

/**
 * Deleta hóspede por código - Chamando função
 * @param cod código do hóspede a ser deletado
 * @return int  valor para tratamento de erro
 */
int deletarHospedeController(int cod);

/**
 * Salva os dados referentes a hóspedes
 * @param hospede Objeto para adicionar nos arquivos
 * @return int  valor para tratamento de erro
 */
int addFileHospede(Hospede hospede);

/**
 * Função para editar hospede
 * @param codigo do hospede que sera editado
 * @param hospede dados novos do hospede
 * @return int
 */
int editarHospede(int cod, Hospede hospede);

/**
* Lista os dados de todos os hóspedes cadastrados no sistema
* @param
* @return
*/
void listarHospedes();

/**
 * Verifica se o Usuario deseja realmente excluir o registro
 * @param codigo por paramentro
 * @return int para tratar erro
 */
int excluirHospede(int cod);

/**
 * Informa a quantidade de hóspede cadastrados no sistema
 * @param
 * @return int retorna a quantidade de hospede
 */
int contarHospedes();

/**
* Lê os arquivos e tranforma em ponteiro
* @param counter numero de registros nos arquivos
* @return
*/
void carregarHospede(int counter);

/**
* Exclui o hospede cadastrado por parâmeto
* @param codigo
* @return void
*/
void excluirHospedePorCodigo(int codigo);

/**
* Printa menu com informações do produto
* @param
* @return
*/
void printaInformacoesProduto();

/*
* Carrega uma lista de hospedes do arquivo, em um vetor local, e armazena a quantidade de hospedes na variavel passada
* @param: int* counter: endereço de uma variavel para armazenar a quantidade de hóspedes
* @return: endereço de um vetor dinamicamente alocado, contendo as informações dos hóspedes
*/
Hospede* loadHospedeList(int* counter);

/*
* retorna o hospede com o nome descrito, ou um hospede com endereço -1 caso este não exista
* param char* nome: O nome do hospede
/**
* Busca hospede com o nome descrito
* @param char *nome: O nome do hospede
* @return Hospede com o nome desejado ou um hospede com endereço -1 caso este não exista
*/
Hospede encontrarHospedePorNome(char* nome);

/**
* Busca hospede com o cpf informado
* @param char *cpf: cpf do hospede
* @return Hospede com o cpf desejado ou um hospede com endereço -1 caso este não exista
*/
Hospede encontrarHospedePorCPF(char* cpf);

/**
* Busca hospede com o codigo informado
* @param char *codigo: codigo do hospede
* @return Hospede com o codigo desejado ou um hospede com endereço -1 caso este não exista
*/
Hospede encontrarHospedePorCodigo(int codigo);
