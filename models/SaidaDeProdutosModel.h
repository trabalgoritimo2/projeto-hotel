/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#pragma once 
#include "../structs/moduloCadastroStructs.h"

void listarVendas();

int cadastrarVenda(Venda venda, Venda_Produtos *venda_Produtos,int quantidadeVendaProdutos);

Conta_Hospede buscarContaHospede(int hosCodigo);

int cadastrarContaHospede(Conta_Hospede conta_Hospede);

int atualizarContaHospede(Conta_Hospede conta_Hospede);

int apagarContaHospede(Conta_Hospede conta_Hospede);