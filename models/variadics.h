#pragma once

void startXML(char* path);

int isTableInXML(char* path, char* table);

void deleteTable(char* path, char* table);

void createTable(char* path, char* table);

int isTableInXML(char* path, char* table);

void registerToXML(char* path, char* table, char* formats, ...);

int registersInTable(char* path, char* table);

void readFromXML(char* path, char* table, int index, char* formats, ...);
