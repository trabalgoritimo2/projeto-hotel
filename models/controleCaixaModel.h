/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#pragma once
#include "../structs/moduloCadastroStructs.h"

/**
* Cadastra o historico de caixa e atualiza o caixa do hotel
* @param Historico_Caixa historicoCaixa: historicoCaixa que deseja cadastrar no sistema
* @return 1- se cadastrou com sucesso 0: se houve algum problema
*/
int cadastrarHistorico (Historico_Caixa historicoCaixa);

/**
* Atualiza o valor do caixa do hotel a partir de um historico de caixa
* @param Historico_Caixa historicoCaixa: historicoCaixa já cadastrado, int tipoArmazenamento: 1- binário 2-txt
* @return 1- se atualizou com sucesso 0: se houve algum problema
*/
int atualizarValorCaixa(Historico_Caixa historicoCaixa, int tipoArmazenamento);

/**
* Cadastra contas a receber ou a pagar
* @param Contas_Receber_Pagar conta: conta que deseja cadastrar no sistema
* @return 1- se cadastrou com sucesso 0: se houve algum problema
*/
int cadastrarContasAReceberPagar (Contas_Receber_Pagar conta);

/**
* Monta a estrutura Historico_Caixa de acordo com os parâmetros informados, sem salvar nos arquivos
* @param float valor: valor que será abatido e/ou somado ao caixa
* @param char entrada_saida[] : se é "entrada" ou "saida" de valor do caixa
* @param int id_origem: //id_reserva, id_venda, id_entrada_produtos
* @param char tipo[]: // diaria, venda, pagamento
* @return Historico_Caixa a estrutura montada
*/
Historico_Caixa montaHistoricoCaixa(float valor,char entrada_saida[], int id_origem, char tipo[]);

/**
* Monta a estrutura Contas_Receber_Pagar de acordo com os parâmetros informados e salva
* @param Pagamento pagamento: estrutura com as informações sobre o pagamento
* @param char tipo[]: // diaria, venda, pagamento
* @param char entrada_saida[] : se é "entrada" ou "saida" de valor do caixa
* @param float valorTotal
* @param int id_origem: //id_reserva, id_venda, id_entrada_produtos
* @return 1- operação feita com sucesso, 0- erro
*/
int montaESalvaContaReceberPagar(Pagamento pagamento, char tipo[], char status[], char entrada_saida[], float valorTotal, int id_origem);

/**
*  Mostra ao usuário o menu de pagamento.
* @param 
* @return 
*/
int intro_pagamento();

/**
* Função para realizar o pagamento em cartão de crédito.
* @param 
* @return
*/
int pag_cartao();

/**
* Função para realizar o pagamento em dinheiro.
* @param 
* @return
*/
int pag_dinheiro();

/**
* Função para salvar os pagamentos.
* @param 
* @return
*/
int salvar_pagamento(Historico_Caixa pagamentos);

/**
* Função para buscar os pagamentos.
* @param 
* @return
*/
int carregar_pagamentos();

/**
*  Função para verificar a validade do pagamento. Em caso de estar vencido,
*  ela irá jogar tal valor no caixa.
* @param 
* @return
*/
void verificaValidadePagamento();

/**
* Função para realizar o pagamento no cartão.
* @param 
* @return
*/
int carregarPagamentosAPagar();

/**
* Função para mostrar o saldo do caixa.
* @param 
* @return
*/
int mostrarSaldoCaixa();

/**
* Função para mostrar o histórico de Lançamentos e Retiradas.
* @param 
* @return
*/
int mostrarHistoricoLancRetirada();

/**
* Função para realizar o pagamento no cartão.
* @param tipoArmazenamento tipoArmazenamento: 1- binário 2-txt ; Data atual: struct que passa o dia,mes e ano.
* @return
*/
int atualizarValorCaixa2(int tipoArmazenamento, Data atual);


int carregarCReceberPagar();

int atualizarContasAReceber(Contas_Receber_Pagar *contas, int tamanho);