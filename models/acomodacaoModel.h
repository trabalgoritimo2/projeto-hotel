/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#pragma once
#include "../structs/moduloCadastroStructs.h"

enum acoPropriedades {acoPropCodigo=0, acoPropDescricao, acoPropCaracteristicas, acoPropCategoria};
enum acoFiltros {acoFiltCodigo=0, acoFiltDescricao, acoFiltCaracteristicas, acoFiltCategoria};

enum paramFlags {
    ParData = 1 << 0,
    ParCategCodigo = 1 << 1,
    ParCaracteristicas = 1 << 2,
    ParQtPessoas = 1 << 3,
    ParValorMaximo = 1 << 4
};

/**
* Menu para criação de uma nova acomodação
* @param
* @return void
*/
int cadastrarAcomodacao();

/**
* Mostra na tela todas as acomodações registradas
* @param
* @return void
*/
void mostrarAcomodacao();

/**
* Menu de acesso para editar uma acomodação
* @param
* @return void
*/
void editarAcomodacao();

/**
* Menu para excluir uma acomodação
* @param
* @return void
*/
void excluirAcomodacao();

/**
* Menu para mostrar acomodações filtradas pelo parametro dado
* @param
* @return void
*/
void pesquisarAcomodacao();

/**
* Carrega as acomodações registradas para serem manipuladas
* @param int counter: a quantidade de acomodações para serem carregadas (use contarAcomodacoes() para contar todas)
* @return pointer de um vetor de acomodações
*/
Acomodacao* carregarAcomodacao(int counter);

/**
* Retorna a quantidade de acomodacoes registradas
* @param
* @return int: quantidade de acomodações registradas
*/
int contarAcomodacoes();

/**
* Salva o vetor de acomodações em disco
* @param Acomodação *lista: vetor de acomodações para serem salvas
* @param int counter: quantidade de acomodações no vetors
* @return void
*/
void salvarAcomodacao(Acomodacao *lista, int counter);

/**
* Função para printar um vetor de acomodações. Para mostrar 1 acomodação, use escreverAcomodacao(&acom, 1);
* @param Acomodacao *acom: vetor de categorias
* @param int counter: quantidade de acomodações para printar
* @return void
*/
void escreverAcomodacao(Acomodacao *acom, int counter);

/**
* Edita a acomodação referida pelo codigo
* @param int codigo: codigo da acomodação a ser editada
* @param int propriedade: a propriedade que será editada (definido por enum acoPropriedades)
* @param char edição[]: a nova informação da propriedade (valores numericos são passados como string)
* @return void
*/
int editarAcomodacaoPorPropriedade(int codigo, int propriedade, char edicao[]);

/**
* Busca por uma acomodação registrada com o código enviado
* @param int codigo: codigo da acomodação que se procura
* @return 1 se a acomodação for encontrada
*/
int acomodacaoExiste(int codigo);

/**
* Excluir a acomodação com o código passado
* @param int codigo: o codigo da acomodação que irá ser excluida
* @return void
*/
void excluirAcomodacaoPorCodigo(int codigo);

/**
* Copia o conteúdo do segundo argumento para o primeiro argumento
* @param Acomodacao* pra: acomodação para onde os dados serão copiados
* @param Acomodacao* de: acomodação de onde virão os dados
* @return void
*/
void copiarAcomodacao(Acomodacao* pra, Acomodacao* de);

/**
* Mostra na tela as acomodações filtradas
* @param int filtro: a propriedade usada para separar acomodações (definida no enum acoFiltros)
* @param char* param: a informação que define a filtragem
* @return void
*/
void filtrarAcomodacoes(int filtro, char* param);

/**
* Essa função verifica se alguma acomodação tem uma copia de categoria desatualizada com as categorias atuais
* @param
* @return void
*/
void mostrarAcomocacoesDesatualizadas();

/**
* Função para selecionar acomodações baseado em diversos parametros
* @param int* filtroCount: referencia de um inteiro para registrar a quantidade de acomodações que passam pelo filtro
* @param ParamPesq params: estrutura com as flags e dados da pesquisa
* @return Endereço do vetor de acomodaçẽos filtradas
*/
Acomodacao* filtrarAcomodacaoPorFlags(int* filtroCount, ParamPesq params);

/**
* Retorna a string passada com todas as letras em maiusculo
* @param char* lower: string para ser convertida
* @return nova string em maiusculo
*/
char* strToUpper(char *lower);

/**
* Checa se a string sub está contida na string universal (mesmo se as letras forem diferentes entre maiusculas e minusculas)
* @param char* universal: string de comparação
* @param char* sub: string que se tentará encontrar
* @return 1 se encontrar e 0 se não encontrar
*/
int strContains(char* universal, char* sub);

/**
* Busca acomodação por código
* @param int codigo: código desejado
* @return Acomodacao acomodação: encontrada de acordo com código desejado, se não encontrar, retorna acomodação com o código -1
*/
Acomodacao buscarAcomodacaoPorCodigo(int codigo);
