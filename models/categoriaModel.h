/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#pragma once
#include "../structs/moduloCadastroStructs.h"

enum catPropriedades {catPropCodigo=0, catPropDescricao, catPropValor, catPropQtPessoas};
enum catFiltros {catFiltCodigo=0, catFiltDescricao, catFiltValor, catFiltQtPessoas};

/**
* Cria uma nova categoria e manda para criarCategoriaPorValor salvar
* @param
* @return void
*/
void criarCategoria();

/**
* Mostra na tela todas as categorias registradas
* @param
* @return void
*/
void mostrarCategorias();

/**
* Menu para exclusão de uma categoria
* @param 
* @return void
*/
void excluirCategoria();

/**
* Menu para editar uma categoria
* @param 
* @return void
*/
int editarCategoria();

/**
* Menu que pede um filtro para filtrar as categorias
* @param
* @return void
*/
void pesquisarCategorias();

/**
* Cria uma categoria com osvalores passados
* @param char *descricao: descricao da categoria
* @param float valorDaDiaria: valor de 1 dia
* @param int quantidadeDePessoas: maximo de pessoas que comporta
* @return void
*/
void criarCategoriaPorValor(char *descricao, float valorDaDiaria, int quantidadeDePessoas);

/**
* mostra no terminal a categoria que foi passada
* @param Categoria *cate: vetor com as categorias para serem mostradas
* @param int count: quantidade de categorias no vetor passado
* @return void
*/
void escreverCategoria(Categoria *cate, int count);

/**
* Excluir a categoria com o código especificado]
* @param codigo da categoria
* @return 1- ocorrer algum erro
*/
int excluirCategoriaPorCodigo(int codigo);

/**
* Aplica a edição passada na propriedade indicada no parametro, na categoria de codigo passado
* @param int codigo: codido da categoria
* @param int propriedade: a propriedade que se edita (deve ser um de catPropriedades)
* @param char *edicao: a nova informação da categoria (numeros devem ser passados em forma de string)
* @return void
*/
int editarCategoriaPorValor(int codigo, int propriedade, char *edicao);

/**
* mostra as categorias filtradas com o parametro e filtro passados
* @param int filtro: filtro para pesquisa (deve ser um enum de catFiltros)
* @param char* param: parametro de filtragem
* @return void
*/
void filtrarCategorias(int filtro, char* param);

/**
* Carrega as categorias do banco de dados
* @param int count: quantidade de categorias presentes (use contarCategorias() para contar)
* @return Pointer de categoria com as informações carregadas
*/
Categoria* carregarCategorias();

/**
* Conta a quantidade de categorias regostradas no banco de dados
* @param 
* @return numero de categorias registradas
*/
int contarCategorias();

/**
* Salva as categorias no arquivo
* @param Categoria *lista: pointer contendo as categorias a serem salvas
* @param int count: quantidade de categorias presentes no vetor
* @return void
*/
void salvarCategorias(Categoria *lista, int count);

/**
* Retorna uma categoria baseada no código. A categoria deve existir (use categoriaExiste() para conferir)
* @param int codigo: codigo da categoria buscada
* @return Cópia da categoria buscada
*/
Categoria buscarCategoria(int codigo);

/**
* procura confirmar se o codigo de parametro coincide com alguma categoria registrada
* @param int codigo: codigo da categoria a procurar
* @return 1- se existir, 0- se não for encontrada
*/
int categoriaExiste( int codigo);

/**
* copia o conteúdo do segundo argumento para o primeiro argumento
* @param Categoria* pra: pointer para onde os valores irão ser copiados
* @param Categoria* de: pointer de onde os valores irão ser copiados
* @return void
*/
void copiarCategoria(Categoria* pra, Categoria* de);

/**
* recebe uma string tokenizada, uma linha do arquivo categoria.txt, e transforma em um objeto categoria
* @param char catString[]: string tokenizada
* @param Categoria *cate: pointer para onde as informações serão copiadas
* @return 1- se deletou com sucesso 0: se houve algum problema
*/
void strPraCat(char *catString, Categoria *cate);
