/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#pragma once
#include <stdio.h>
#include <stdlib.h>
#include "../structs/moduloCadastroStructs.h"

/**
* Obtém a sequência de código do fornecedor no arquivo Binário
* @param
* @return int: o código do próximo registro
*/
int sequenceFornecedorArquivoBinario();

/**
* Obtém a sequência de código do fornecedor no arquivo TXT
* @param
* @return int: o código do próximo registro
*/
int sequenceFornecedorArquivoTXT();

/**
 * Cadastrar fornecedor - Chamando a função
 * @param fornecedor fornecedor a ser cadastrado
 * @return int: 1- sucesso 0-erro
 */
int cadastrarFornecedorController(Fornecedor fornecedor);

/**
 * Editar fornecedor - Chamando a função
 * @param fornecedor fornecedor a ser cadastrado
 * @return int: 1- sucesso 0-erro
 */
int atualizarFornecedorController(int cod, Fornecedor hospede);

/**
 * Listar fornecedor - Chamando a função
 * @param fornecedor fornecedor a ser cadastrado
 * @return int: 1- sucesso 0-erro
 */
int listarForController();

/**
 * Salva fornecedores ao arquivo txt/binario
 * @param fornecedor que vai ser adicionado ao arquivo
 * @return int valor para tratamento de erro
 */
int addFileFornecedor(Fornecedor fornecedor);

/**
 * Função para editar Fornecedor
 * @param cod do fornecedor que sera editado
 * @param fornecedor dados novos do fornecedor
 * @return int: 1-sucesso 0-error
 */
int editarFornecedor(int cod, Fornecedor fornecedor);

/**
 * Mostra os fornecedores ao usuario
 * @param
 * @return
 */
void listarFornecedor();

/**
 * @brief Conta a quantidade de fornecedores nos arquivos
 * @param
 * @return int retorna o valor da quatidade de fornecedores
 */
int contarfornecedor();

/**
 * Tranforma os dados do arquivo em Ponteiro
 * @param counter quantidade de Fornecedores existem salvos
 * @return
 */
void carregarfornecedor(int counter);

/**
 * Tranforma os dados do arquivo em Ponteiro
 * @param counter quantidade de Fornecedores existem salvos
 * @return
 */
Fornecedor* loadFornecedorList(int* count);

/**
* Exclui o fornecedor do registro com o codigo passado por paramentro
* @param codigo
* @return
*/
void excluirfornecedorPorCodigo(int codigo);


Fornecedor buscaFornecedorPorCNPJ(char *forCnpj);
