/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#pragma once
#include "../structs/moduloCadastroStructs.h"

enum statusPagamentoReserva { status_pago = 1, status_aguardando_pagamento = 2 };

/**
* Cadastrar uma reserva na base de dados
* @param Reserva reserva: reserva a ser cadastrada
* @return 1- se cadastrou com sucesso 0: se houve algum problema
*/
int cadastrarReserva(Reserva reserva);

/**
* Buscar reserva
* @param Reserva a ser cadastrada e/ou encontrada
* @return Reserva: se não for encontrado retorna reserva.resCodigo = -1
*/
Reserva buscarReserva (Reserva reservaNova);

/**
* Mostra na tela as acomodaçoes disponiveis para reservar
* @param Reserva reservaNova: a reserva a ser cadastrada
* @param int catCodigo: codigo da categoria da acomodação desejada
* @return void
*/
void buscarAcomodacoesDisponiveis(Reserva reservaNova, int catCodigo);

/**
* Busca reserva já cadastrada
* @param resDataInicio, resDataFim, resHospedeId
* @return Reserva: se não for encontrado retorna reserva.resCodigo = -1
*/
Reserva buscarReservaRealizadas (Data resDataInicio, Data resDataFim, int resHospedeId);

/**
* Buscar acomodação por código
* @param int resAcomodacaoId: codigo da acomodacao
* @return Acomodacao que possui o código desejado
*/
Acomodacao buscarAcomodacaoReserva(int resAcomodacaoId);

/**
* Cancelar a reserva por código
* @param int codigo: codigo da reserva que se deseja cancelar
* @return void
*/
void cancelarReservaPorCodigo(int codigo);

/**
* Cria um objeto de reserva baseado na string toketizada
* @param char* line: informações de reserva em texto
* @return Objeto de reserva criado
*/
Reserva strPraResv(char* line);

/*
* Salva a reserva em um arquivo de texto
* @param FILE* file: arquivo para salvar
* @param Reserva reserva: a reserva que será salva
* @return void
*/
void salvarReservaEmArquivoTexto(FILE* file, Reserva reserva);

/**
* Lista todas as reservas cadastradas no sistema
* @param
* @return void
*/
void listarReservas();

/**
* verifica se a reserva com o ID passado existe
* @param int id: numero de ID da reserva que se busca
* @return 1 se a reserva com esse ID existir, 0 do contrário
*/
int reservaExiste(int id);

/**
* Carrega as reservas gravadas no arquivo
* @param int* counter: endereço de uma variavel usada para armazenar a quantidade de reservas registradas
* @return endereço de um vetor de reservas de tamanho especificado em 'counter', ou NULL se der errado
*/
Reserva* carregarReservas(int* counter);

/**
* Salva as reservas no arquivo
* @param Reserva* reservas: o vetor de informações para serem salvas
* @param int counter: a quantidade de itens presentes no vetor de categorias
* @return
*/
void salvarReservas(Reserva* reservas, int counter);

/*
* encontra todas as reservas associadas ao hospede com id hosCodigo, que tem data final marcadas para hoje ou após
*/
Reserva* encontrarReservasDoHospede(int hosCodigo, int* encontradas);
