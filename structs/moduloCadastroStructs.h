/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#pragma once

/*
 *
 *  Esse arquivo possui todas as structs necessárias no Módulo de Cadastro
 *
 */

/*
 * Struct Endereço
 */
typedef struct {
    char endRua[40];
    char endBairro[40];
    char endCidade[40];
    char endEstado[40];
    int endCep;
    char endComplemento[35];
    int endNumero;
} Endereco;


/*
 * Struct Hotel
 */
typedef struct
{
    int codigo;
    char nome_fantasia[55];
    char razao_social[40];
    int inscricao_estadual;
    int cnpj;
    int telefone;
    char email[254];
    char nome_responsavel[100];
    int telefone_responsavel;
    char horario_checkIn[6];
    char horario_checkOut[6];
    int margem_lucro;
    Endereco endereco;
} Hotel;

/*
*  Struct endereço do Hotel.
*/
struct Ender {
    char Rua[40];
    int Numero;
    char Bairro[40];
    char Cidade[40];
    char Estado[40];
    int Cep;
    char Complemento[35];
};

/*
 * Struct Hóspede
 */
typedef struct {
    int hospCodigo;
    char hospNome[50];
    char hospCpf[14];
    char hospTelefone[15];
    char hospEmail[30];
    char hospSexo;
    char hospEstadoCivil[30];
    char hospDataNascimento[11];
    Endereco hospEndereco;
} Hospede;

/*
 * Struct Categoria
 */
typedef struct {
    int catCodigo;
    char catDescricao[100];
    float catValorDiaria;
    int catQuantidadePessoas;
} Categoria;

/*
 * Struct Acomodação
 */
typedef struct {
    int acoCodigo;
    char acoDescricao[100];
    char acoCaracteristicas[100];
    Categoria acoCategoria;
} Acomodacao;

/*
 * Struct Produto
 */
typedef struct
{
    int codigo;
    char descricao[40];
    int estoque;
    int min_estoque;
    float preco_custo;
    float preco_venda;
} Prod_Disp;

/*
 * Struct Fornecedor
 */
typedef struct {
    int forCodigo;
    char forNomeFantasia[30];
    char forRazaoSocial[30];
    int forInscricaoEstadual;
    char forCnpj[14];
    Endereco forEndereco;
    int forTelefone;
    char forEmail[30];
} Fornecedor;

/*
 * Struct Usuário
 */
typedef struct {
    int usuCodigo;
    char usuNome[200];
    char usuUsuario[100];
    char usuSenha[30];
    int usuPermissao;
} Usuario;

typedef struct {
    int dia;
    int mes;
    int ano;
} Data;


typedef struct {
    int dia;
    int mes;
    int ano;
    int hora;
    int minuto;
    int segundo;
} DataTime;

/*
 * Struct Reserva
 */
typedef struct {
    int resCodigo;
    Data resDataInicio;
    Data resDataFim;
    int resHospedeId;
    int resAcomodacaoId;
    int resStatus_pagamento; // pago ou aguardando_pagamento
    DataTime resDataTime_chegada;
    DataTime resDataTime_saida;
} Reserva;

/*
* Struct para facilitar parametragem de pesquisa de reserva
*/
typedef struct {
    Data dtInicial;
    Data dtFinal;
    int categCodigo;
    char caracteristicas[100];
    int qtPessoas;
    float valorMaximo;
    int flags;
} ParamPesq;

/*
 * Struct Conta_Hospede
 */
typedef struct {
    int codigo;
    int id_hospede;
    float valorTotal;
} Conta_Hospede;

/*
 * Struct Historico_Caixa
 */
typedef struct {
    int codigo;
    Data data;
    char entrada_saida[50]; // entrada, saida
    char tipo[50]; // diaria, venda, pagamento
    int id_origem; //id_reserva, id_venda, id_entrada_produtos
    float valor;
} Historico_Caixa;

/*
 * Struct Contas_Receber_Pagar
 */
typedef struct {
    int codigo;
    float valor;
    Data data;
    char entrada_saida[50];//entrada, saida
    char tipo[50]; // diaria, venda, pagamento
    int id_origem; //id_reserva, id_venda, id_entrada_produtos
    char status[40]; // lancado, aguardando_lancamento
    Data dataVencimento;
} Contas_Receber_Pagar;

/*
 * Struct Entrada_Produtos
 */
typedef struct {
    int codigo;
    int id_fornecedor;
    float valor_total;
    float imposto;
    float preco_frete;
} Entrada_Produtos;

/*
 * Struct Entrada_Produtos_Item
 */
typedef struct {
    int codigo;
    int id_produto;
    int id_entrada_produtos;
    int quantidade;
    float valor_compra;
    float preco_venda;
} Entrada_Produtos_Item;

/*
 * Struct Venda
 */
typedef struct {
    int codigo;
    Data data;
    int id_hospede;
    float valor_total;
} Venda;

/*
 * Struct Venda_Produtos
 */
typedef struct {
    int codigo;
    int id_venda;
    int id_produto;
    int quantidade;
} Venda_Produtos;

/*
 * Struct Venda_Produtos
 */
typedef struct {
    int isEntrada;
    float valorEntrada;
    int formaPagamento;
    int numeroParcelas;
    Data dataVencimento;
} Pagamento;
