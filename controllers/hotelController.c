/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../structs/moduloCadastroStructs.h"
#include "../models/configuracaoModel.h"
#include "../models/funcoesModel.h"
#include "../models/hotelModel.h"

#define SUCCESS 1;
#define ERROR 0;
/**
* Tela de cadastro do hotel
* @param
* @return
*/
int cadastrar_hotel() {
    Hotel hotel;
    //apresentação da área de cadastro do hotel;
    printf("=============================\n");
    printf("|       CADASTRAR HOTEL     |\n");
    printf("=============================\n");
    printf("Preencha de acordo com os dados do Hotel.");
    //Cadastro do Nome Fantasia;
    printf("\n");
    printf("\nNome Fantasia(Lim. 55 caract.): \n");
    fgets(hotel.nome_fantasia,55,stdin);
    hotel.nome_fantasia[strcspn(hotel.nome_fantasia, "\n")] = 0;
    //Cadastro da Razão Social;
    printf("\n");
    printf("Razão Social(Lim. 40 caract.): \n");
    fgets(hotel.razao_social,55,stdin);
    setbuf(stdin,NULL);
    hotel.razao_social[strcspn(hotel.razao_social, "\n")] = 0;
    //Cadastro da Inscrição Estadual;
    printf("\n");
    printf("Inscrição Estadual: ");
    scanf("%d",&hotel.inscricao_estadual);
    //Cadastro do CNPJ;
    printf("\n");
    printf("CNPJ: ");
    scanf("%d",&hotel.cnpj);
    //Cadastro do Endereço Completo;
    printf("\n"); //CAMPOS;
    printf("----------Campos do Endereço--------");
    setbuf(stdin,NULL);
    printf("\n");
    printf("\nPreencha abaixo os campos relacionados ao endereço: ");
    printf("\nRua: ");
    fgets(hotel.endereco.endRua,40,stdin);
    setbuf(stdin, NULL);
    hotel.endereco.endRua[strcspn(hotel.endereco.endRua, "\n")] = 0;
    printf("\nNúmero: ");
    scanf("%d%*c",&hotel.endereco.endNumero);
    printf("\nBairro: ");
    fgets(hotel.endereco.endBairro,40,stdin);
    hotel.endereco.endBairro[strcspn(hotel.endereco.endBairro, "\n")] = 0;
    printf("\nCidade: ");
    fgets(hotel.endereco.endCidade,40,stdin);
    hotel.endereco.endCidade[strcspn(hotel.endereco.endCidade, "\n")] = 0;
    printf("\nEstado: ");
    fgets(hotel.endereco.endEstado,40,stdin);
    hotel.endereco.endEstado[strcspn(hotel.endereco.endEstado, "\n")] = 0;
    printf("\nCEP: ");
    scanf("%d%*c",&hotel.endereco.endCep);
    printf("\nComplemento: ");
    fgets(hotel.endereco.endComplemento,35,stdin);
    hotel.endereco.endComplemento[strcspn(hotel.endereco.endComplemento, "\n")] = 0;
    setbuf(stdin,NULL);
    //Cadastro do telefone;
    printf("\nTelefone: ");
    scanf("%d",&hotel.telefone);
    //Cadastro do E-mail;
    printf("\nEmail: ");
    setbuf(stdin,NULL);
    fgets(hotel.email,254,stdin);
    setbuf(stdin,NULL);
    hotel.email[strcspn(hotel.email, "\n")] = 0;
    //Cadastro do Nome do Responsável;
    printf("\nNome do responsável: ");
    fgets(hotel.nome_responsavel,100,stdin);
    setbuf(stdin,NULL);
    hotel.nome_responsavel[strcspn(hotel.nome_responsavel, "\n")] = 0;
    //Cadastro do Telefone do Responsável;
    printf("\nTelefone do responsável: ");
    scanf("%d",&hotel.telefone_responsavel);
    setbuf(stdin,NULL);
    //Cadastro do horário de chech-in;
    printf("\n");
    printf("Preencha com os dados do Check-In: \n");
    printf("Informe a hora: \n");
    inputString(hotel.horario_checkIn, 6);
    //Cadastro do horário de check-out;
    printf("\n");
    printf("Preencha com os dados do Check-Out: \n");
    printf("Informe a hora: \n");
    inputString(hotel.horario_checkOut, 6);
    //margem de lucro;
    printf("\nInforme a margem de lucro: ");
    scanf("%d",&hotel.margem_lucro);
    salvar_hotel(hotel);
}

/**
* Lista todos os dados do hotel
* @param
* @return
*/
void listar_hotel() {
    Hotel *hoteis;
    hoteis = carregar_hotel();
    int size = qtdeRegistrosHotel();

    for(int i=0;i<size;i++) {
        printf("\n\t\t DADOS DO HOTEL");
        printf("\nNome Fantasia: %s",hoteis[i].nome_fantasia);
        printf("\nRazao Social: %s",hoteis[i].razao_social);
        printf("\nInscrição Estadual: %d",hoteis[i].inscricao_estadual);
        printf("\nCNPJ: %d",hoteis[i].cnpj);
        printf("\nTelefone: %d",hoteis[i].telefone);
        printf("\nEmail: %s",hoteis[i].email);
        printf("\nTelefone do responsável: %d",hoteis[i].telefone_responsavel);
        printf("\nNome do responsável: %s",hoteis[i].nome_responsavel);
        printf("\n----------Campos do Endereço--------");
        printf("\nRua: %s",hoteis[i].endereco.endRua);
        printf("\nNúmero: %d",hoteis[i].endereco.endNumero);
        printf("\nBairro: %s",hoteis[i].endereco.endBairro);
        printf("\nCidade: %s",hoteis[i].endereco.endCidade);
        printf("\nEstado: %s",hoteis[i].endereco.endEstado);
        printf("\nCEP: %d",hoteis[i].endereco.endCep);
        printf("\nComplemeto: %s",hoteis[i].endereco.endComplemento);
        printf("\n----------Horários------------");
        printf("\nCheck-In: às %s hora(s).",hoteis[i].horario_checkIn);
        printf("\nCheck-Out: às %s hora(s).",hoteis[i].horario_checkOut);
        printf("\n-------Margem de Lucro---------");
        printf("\nMargem de lucro: %d%% \n",hoteis[i].margem_lucro);
    }
}

/**
* Busca o hotel por nome fantasia do hotel
* @param char *nomeFantasia: ponteiro nomeFantasia
* @return hotel encontrado com o nomeFantasia desejado
*/
Hotel buscarHotelPorNomeFantasia(char *nomeFantasia) {
    int tipoArquivo = opcaoArmazenamentoUsuario();
    FILE *file = NULL;
    Hotel hotel;
    switch (tipoArquivo) {
        case 1:
            file = fopen("persist/hotel.bin", "rb+");
            // Verificando se o arquivo foi aberto de forma correta
            if (file != NULL) {
                // Percorrendo cada linha procurando o nome fantasia do hotel
                while (fread(&hotel, sizeof (Hotel), 1, file)) {
                    if (strcmp(hotel.nome_fantasia, nomeFantasia) == 0) {
                        break;
                    }
                }
                // Fechando o arquivo
                fclose(file);
                // Se id for encontrado, então retorna o hotel
                if (strcmp(hotel.nome_fantasia, nomeFantasia) == 0) {
                    return hotel;
                }
                // Retorna hotel vazio com id == -1
                Hotel hotelVazio;
                hotelVazio.codigo = -1;
                return hotelVazio;
            }
            break;
        case 2:
            file = fopen("persist/hotel.txt", "r+");
            //verificando se o arquivo foi aberto de forma correta
            if (file != NULL) {
                char hotelString[600];
                // Percorrendo cada linha procurando o nome fantasia do hotel
                while (fgets(hotelString, 600, file) != NULL) {
                    char *token = strtok(hotelString, ";");
                    for (int i = 0; token != NULL; i++) {
                        switch (i) {
                            case 0:
                                hotel.codigo = atoi(token);
                                break;
                            case 1:
                                strcpy(hotel.nome_fantasia, token);
                                break;
                            case 2:
                                strcpy(hotel.razao_social, token);
                                break;
                            case 3:
                                hotel.inscricao_estadual = atoi(token);
                                break;
                            case 4:
                                hotel.cnpj = atoi(token);
                                break;
                            case 5:
                                strcpy(hotel.endereco.endRua, token);
                                break;
                            case 6:
                                hotel.endereco.endNumero = atoi(token);
                                break;
                            case 7:
                                strcpy(hotel.endereco.endBairro, token);
                                break;
                            case 8:
                                strcpy(hotel.endereco.endCidade, token);
                                break;
                            case 9:
                                strcpy(hotel.endereco.endEstado, token);
                                break;
                            case 10:
                                hotel.endereco.endCep = atoi(token);
                                break;
                            case 11:
                                strcpy(hotel.endereco.endComplemento, token);
                                break;
                            case 12:
                                hotel.telefone = atoi(token);
                                break;
                            case 13:
                                strcpy(hotel.email, token);
                                break;
                            case 14:
                                strcpy(hotel.nome_responsavel, token);
                                break;
                            case 15:
                                hotel.telefone_responsavel = atoi(token);
                                break;
                            case 16:
                                strcpy(hotel.horario_checkIn, token);
                                break;
                            case 17:
                                strcpy(hotel.horario_checkOut, token);
                                break;
                            case 18:
                                hotel.margem_lucro = atoi(token);
                                break;
                            default:
                                break;
                        }
                        token = strtok(NULL, ";");
                    }
                    if (strcmp(hotel.nome_fantasia, nomeFantasia) == 0) {
                        break;
                    }
                }
                fclose(file);

                // Se id for encontrado, então retorna o hotel
                if (strcmp(hotel.nome_fantasia, nomeFantasia) == 0) {
                    return hotel;
                }
                // Retorna hotel vazio com id == -1
                Hotel hotelVazio;
                hotelVazio.codigo = -1;
                return hotelVazio;
            }
            break;
        default:
            mostrarMensagemErroTipoArquivo();
            break;
    }
    if (file == NULL) {
        mostrarMensagemErroArquivo();
    }
    nomeFantasia = NULL;
}

/**
* Tela de edição do cadastro do hotel
* @param
* @return
*/
void editar_hotel() {
    int info;
    int info2;
    char *nomeFantasia;
    Hotel hotel;
    printf("Informe o nome fantasia do hotel que você deseja editar: \n");
    fgets(nomeFantasia, 55, stdin);
    nomeFantasia[strcspn(nomeFantasia, "\n")] = 0;
    setbuf(stdin, NULL);
    hotel = buscarHotelPorNomeFantasia(nomeFantasia);
    if(hotel.codigo != -1) {
        do {
            informacoesHotel();
            scanf("%d",&info);
            setbuf(stdin,NULL);
            switch (info) {
                case 1:
                    printf("\nNome Fantasia(Lim. 55 caract.): \n");
                    fgets(hotel.nome_fantasia,55,stdin);
                    setbuf(stdin,NULL);
                    hotel.nome_fantasia[strcspn(hotel.nome_fantasia, "\n")] = 0;
                    break;
                case 2:
                    printf("Razão Social(Lim. 40 caract.): \n");
                    fgets(hotel.razao_social,55,stdin);
                    setbuf(stdin,NULL);
                    hotel.razao_social[strcspn(hotel.razao_social, "\n")] = 0;
                    break;
                case 3:
                    printf("Inscrição Estadual: ");
                    scanf("%d",&hotel.inscricao_estadual);
                    break;
                case 4:
                    printf("CNPJ: ");
                    scanf("%d",&hotel.cnpj);
                    break;
                case 5:
                    do {
                        printf("----------Campos do Endereço--------");
                        printf("\nInforme sua escolha: ");
                        printf("\n1 - Rua.");
                        printf("\n2 - Número.");
                        printf("\n3 - Bairro.");
                        printf("\n4 - Cidade.");
                        printf("\n5 - Estado.");
                        printf("\n6 - CEP.");
                        printf("\n7 - Complemento.");
                        printf("\n8 - Sair.");
                        printf("\n--> ");
                        scanf("%d",&info2);
                        setbuf(stdin,NULL);

                        switch (info2) {
                            case 1:
                                printf("\nRua: ");
                                fgets(hotel.endereco.endRua,40,stdin);
                                setbuf(stdin, NULL);
                                hotel.endereco.endRua[strcspn(hotel.endereco.endRua, "\n")] = 0;
                                break;
                            case 2:
                                printf("\nNúmero: ");
                                scanf("%d%*c",&hotel.endereco.endNumero);
                                break;
                            case 3:
                                printf("\nBairro: ");
                                fgets(hotel.endereco.endBairro,40,stdin);
                                setbuf(stdin, NULL);
                                hotel.endereco.endBairro[strcspn(hotel.endereco.endBairro, "\n")] = 0;
                                break;
                            case 4:
                                printf("\nCidade: ");
                                fgets(hotel.endereco.endCidade,40,stdin);
                                setbuf(stdin, NULL);
                                hotel.endereco.endCidade[strcspn(hotel.endereco.endCidade, "\n")] = 0;
                                break;
                            case 5:
                                printf("\nEstado: ");
                                fgets(hotel.endereco.endEstado,40,stdin);
                                setbuf(stdin, NULL);
                                hotel.endereco.endEstado[strcspn(hotel.endereco.endEstado, "\n")] = 0;
                                break;
                            case 6:
                                printf("\nCEP: ");
                                scanf("%d%*c",&hotel.endereco.endCep);
                                setbuf(stdin, NULL);
                                break;
                            case 7:
                                printf("\nComplemento: ");
                                fgets(hotel.endereco.endComplemento,35,stdin);
                                setbuf(stdin, NULL);
                                hotel.endereco.endComplemento[strcspn(hotel.endereco.endComplemento, "\n")] = 0;
                                break;
                            default:
                                if (info2 != 8) {
                                    printf("\nErro: Opção Inválida!");
                                }
                        }
                    } while (info2 != 8);
                    break;

                case 6:
                    printf("\nTelefone: ");
                    scanf("%d",&hotel.telefone);
                    break;
                case 7:
                    printf("\nEmail: ");
                    setbuf(stdin,NULL);
                    fgets(hotel.email,254,stdin);
                    hotel.email[strcspn(hotel.email, "\n")] = 0;
                    break;
                case 8:
                    printf("\nNome do responsável: ");
                    fgets(hotel.nome_responsavel,100,stdin);
                    setbuf(stdin,NULL);
                    hotel.nome_responsavel[strcspn(hotel.nome_responsavel, "\n")] = 0;
                    break;
                case 9:
                    printf("\nTelefone do responsável: ");
                    scanf("%d",&hotel.telefone_responsavel);
                    break;
                case 10:
                    printf("\nPreencha com os dados do Check-In: ");
                    printf("\nInforme a hora(Ex.: 07:00): ");
                    inputString(hotel.horario_checkIn, 6);
                    break;
                case 11:
                    printf("\nPreencha com os dados do Check-Out: ");
                    printf("\nInforme a hora(Ex.: 18:30):  ");
                    inputString(hotel.horario_checkOut, 6);
                    break;
                case 12:
                    printf("\nInforme a Margem de Lucro: ");
                    scanf("%d",&hotel.margem_lucro);
                    break;
                default:
                    if (info != 13) {
                        printf("\nErro: Opção Inválida!");
                    }
                break;
            }
        } while (info != 13);
    }
    atualizarHotel(hotel);
}

/**
* Salva as informações em arquivo binário ou txt
* @param Hotel hotel : hotel a ser salvo
* @return
*/
void salvar_hotel(Hotel hotel) {
    int tipoFile = opcaoArmazenamentoUsuario();
    FILE *file = NULL;

    if (tipoFile == 1) {
        //arq bin;
        file = fopen("persist/hotel.bin","wb"); //abre o arq;
        hotel.codigo = sequenceArquivoBinario("persist/seq_hotel.bin");
        if (file == NULL){
            mostrarMensagemErroArquivo();
            exit(1);
        }
        fwrite(&hotel,sizeof(Hotel),1,file);
        fclose(file);

    }else if (tipoFile == 2) {
        //arq txt;
        file = fopen("persist/hotel.txt","w"); //abre o arq;
        hotel.codigo = sequenceArquivoTXT("persist/seq_hotel.txt");
        if (file == NULL) {
            mostrarMensagemErroArquivo();
            return;
        }

        fprintf(file, "%d;", hotel.codigo);
        fprintf(file, "%s;", hotel.nome_fantasia);
        fprintf(file, "%s;", hotel.razao_social);
        fprintf(file, "%d;", hotel.inscricao_estadual);
        fprintf(file, "%d;", hotel.cnpj);
        //endereço completo;
        fprintf(file, "%s;", hotel.endereco.endRua);
        fprintf(file, "%d;", hotel.endereco.endNumero);
        fprintf(file, "%s;", hotel.endereco.endBairro);
        fprintf(file, "%s;", hotel.endereco.endCidade);
        fprintf(file, "%s;", hotel.endereco.endEstado);
        fprintf(file, "%d;", hotel.endereco.endCep);
        fprintf(file, "%s;", hotel.endereco.endComplemento);
        fprintf(file, "%d;", hotel.telefone);
        fprintf(file, "%s;", hotel.email);
        fprintf(file, "%s;", hotel.nome_responsavel);
        fprintf(file, "%d;", hotel.telefone_responsavel);
        //check-in e check-out;
        fprintf(file, "%s;", hotel.horario_checkIn);
        fprintf(file, "%s;", hotel.horario_checkOut);
        //margem de lucro;
        fprintf(file, "%d;\n", hotel.margem_lucro);
        fclose(file); //fecha o arq;
    }

}

/**
* Obtém a quantidade de registros gravados nos arquivos do hotel
* @param
* @return int: quantidade de registros
*/
int qtdeRegistrosHotel() {
    int tipoArquivo = opcaoArmazenamentoUsuario();
    FILE *file = NULL;
    int size = 0;
    if (tipoArquivo == 1) {
        file = fopen("persist/hotel.bin", "rb+");
        if (file != NULL) {
            Hotel hotel;
            while (fread(&hotel, sizeof (Hotel), 1, file)) {
                size++;
            }
            fclose(file);
        }

    } else if (tipoArquivo == 2) {
        file = fopen("persist/hotel.txt", "r+");

        if (file != NULL) {
            char hotelString[500];
            while (fgets(hotelString, 500, file) != NULL) {
                size++;
            }
            fclose(file);
        }
    }
    return size;
}

/**
* Exibe todas as informações cadastradas sobre o hotel
* @param
* @return array com hoteis cadastrados
*/
Hotel *carregar_hotel() {
    int tipoArquivo = opcaoArmazenamentoUsuario();
    FILE *file = NULL;
    Hotel *hoteis;
    char *tokenTimeCheckIn = NULL;
    char *tokenTimeCheckOut = NULL;
    int size = qtdeRegistrosHotel(tipoArquivo);
    //    List to be returned
    hoteis = (Hotel *) malloc(sizeof (Hotel) * size);
    Hotel tempHotel;
    switch (tipoArquivo) {
        case 1:
            file = fopen("persist/hotel.bin", "rb+");
            if (file != NULL) {
                int index = 0;
                while (fread(&tempHotel, sizeof (Hotel), 1, file)) {
                    hoteis[index] = tempHotel;
                    index++;
                }
                fclose(file);
                return hoteis;
            }
            break;
        case 2:
            file = fopen("persist/hotel.txt", "r+");
            if (file != NULL) {
                int index = 0;
                char hotelString[1300];
                while (fgets(hotelString, 1300, file) != NULL) {
                    char *token = strtok(hotelString, ";");
                    for (int i = 0; token != NULL; i++) {
                        switch (i) {
                            case 0:
                                tempHotel.codigo = atoi(token);
                                break;
                            case 1:
                                strcpy(tempHotel.nome_fantasia, token);
                                break;
                            case 2:
                                strcpy(tempHotel.razao_social, token);
                                break;
                            case 3:
                                tempHotel.inscricao_estadual = atoi(token);
                                break;
                            case 4:
                                tempHotel.cnpj = atoi(token);
                                break;
                            case 5:
                                strcpy(tempHotel.endereco.endRua, token);
                                break;
                            case 6:
                                tempHotel.endereco.endNumero = atoi(token);
                                break;
                            case 7:
                                strcpy(tempHotel.endereco.endBairro, token);
                                break;
                            case 8:
                                strcpy(tempHotel.endereco.endCidade, token);
                                break;
                            case 9:
                                strcpy(tempHotel.endereco.endEstado, token);
                                break;
                            case 10:
                                tempHotel.endereco.endCep = atoi(token);
                                break;
                            case 11:
                                strcpy(tempHotel.endereco.endComplemento, token);
                                break;
                            case 12:
                                tempHotel.telefone = atoi(token);
                                break;
                            case 13:
                                strcpy(tempHotel.email, token);
                                break;
                            case 14:
                                strcpy(tempHotel.nome_responsavel, token);
                                break;
                            case 15:
                                tempHotel.telefone_responsavel = atoi(token);
                                break;
                            case 16:
                                strcpy(tempHotel.horario_checkIn, token);
                                break;
                            case 17:
                                strcpy(tempHotel.horario_checkOut, token);
                                break;
                            case 18:
                                tempHotel.margem_lucro = atoi(token);
                                break;
                            default:
                                break;
                        }
                        token = strtok(NULL, ";");
                    }
                    hoteis[index] = tempHotel;
                    index++;
                }
                fclose(file);
                return hoteis;
            }
            break;
        default:
            break;
    }

    if (file == NULL) {
        mostrarMensagemErroArquivo();
    }
    return NULL;
}

/**
* Printa menu com informações do hotel
* @param
* @return
*/
void informacoesHotel() {
    printf("\nEscolha o número da informação: ");
    printf("\n1  - Nome Fantasia.");
    printf("\n2  - Razão Social.");
    printf("\n3  - Inscrição Estadual.");
    printf("\n4  - CNPJ.");
    printf("\n5  - Endereço Completo.");
    printf("\n6  - Telefone.");
    printf("\n7  - E-mail.");
    printf("\n8  - Nome do Responsável.");
    printf("\n9  - Telefone do Responsável.");
    printf("\n10 - Horário de Check-in.");
    printf("\n11 - Horário de Check-out.");
    printf("\n12 - Margem de Lucro.");
    printf("\n13 - Sair.");
    printf("\n--> ");
}

/**
* Atualiza os dados do hotel
* @param Usuario usuario: usuario a ser atualizado
* @return 1- se atualizar hotel com sucesso 0: se der algum erro
*/
int atualizarHotel(Hotel hotel) {
    int tipoArquivo = opcaoArmazenamentoUsuario();
    FILE *file = NULL;
    FILE *newFile = NULL;
    switch (tipoArquivo) {
        case 1:
            file = fopen("persist/hotel.bin", "rb+");
            if (file != NULL) {
                Hotel tempHotel;
                while (fread(&tempHotel, sizeof (Hotel), 1, file)) {
                    if (tempHotel.codigo == hotel.codigo) {
                        fseek(file, -(long) sizeof (Hotel), 1);
                        fwrite(&hotel, sizeof (Hotel), 1, file);
                    }
                }
            }
            // Fechando o arquivo
            fclose(file);
            break;
        case 2:
            file = fopen("persist/hotel.txt", "r+");
            newFile = fopen("persist/tempHotel.txt", "w+");
            if (file != NULL && newFile != NULL) {
                char hotelString[700];
                while ((fgets(hotelString, 700, file)) != NULL) {
                    char aux[700];
                    strcpy(aux, hotelString);
                    char *token = strtok(hotelString, ";");
                    Hotel tempHotel;
                        for (int i = 0; token != NULL; i++) {
                            switch (i) {
                                case 0:
                                    tempHotel.codigo = atoi(token);
                                    break;
                                case 1:
                                    strcpy(tempHotel.nome_fantasia, token);
                                    break;
                                case 2:
                                    strcpy(tempHotel.razao_social, token);
                                    break;
                                case 3:
                                    tempHotel.inscricao_estadual = atoi(token);
                                    break;
                                case 4:
                                    tempHotel.cnpj = atoi(token);
                                    break;
                                case 5:
                                    strcpy(tempHotel.endereco.endRua, token);
                                    break;
                                case 6:
                                    tempHotel.endereco.endNumero = atoi(token);
                                    break;
                                case 7:
                                    strcpy(tempHotel.endereco.endBairro, token);
                                    break;
                                case 8:
                                    strcpy(tempHotel.endereco.endCidade, token);
                                    break;
                                case 9:
                                    strcpy(tempHotel.endereco.endEstado, token);
                                    break;
                                case 10:
                                    tempHotel.endereco.endCep = atoi(token);
                                    break;
                                case 11:
                                    strcpy(tempHotel.endereco.endComplemento, token);
                                    break;
                                case 12:
                                    tempHotel.telefone = atoi(token);
                                    break;
                                case 13:
                                    strcpy(tempHotel.email, token);
                                    break;
                                case 14:
                                    strcpy(tempHotel.nome_responsavel, token);
                                    break;
                                case 15:
                                    tempHotel.telefone_responsavel = atoi(token);
                                    break;
                                case 16:
                                    strcpy(tempHotel.horario_checkIn, token);
                                    break;
                                case 17:
                                    strcpy(tempHotel.horario_checkOut, token);
                                    break;
                                case 18:
                                    tempHotel.margem_lucro = atoi(token);
                                    break;
                                default:
                                    break;
                            }
                            token = strtok(NULL, ";");
                        }
                        if (tempHotel.codigo == hotel.codigo) {
                            fprintf(newFile, "%d;", hotel.codigo);
                            fprintf(newFile, "%s;", hotel.nome_fantasia);
                            fprintf(newFile, "%s;", hotel.razao_social);
                            fprintf(newFile, "%d;", hotel.inscricao_estadual);
                            fprintf(newFile, "%d;", hotel.cnpj);
                            //endereço completo;
                            fprintf(newFile, "%s;", hotel.endereco.endRua);
                            fprintf(newFile, "%d;", hotel.endereco.endNumero);
                            fprintf(newFile, "%s;", hotel.endereco.endBairro);
                            fprintf(newFile, "%s;", hotel.endereco.endCidade);
                            fprintf(newFile, "%s;", hotel.endereco.endEstado);
                            fprintf(newFile, "%d;", hotel.endereco.endCep);
                            fprintf(newFile, "%s;", hotel.endereco.endComplemento);
                            fprintf(newFile, "%d;", hotel.telefone);
                            fprintf(newFile, "%s;", hotel.email);
                            fprintf(newFile, "%s;", hotel.nome_responsavel);
                            fprintf(newFile, "%d;", hotel.telefone_responsavel);
                            //check-in e check-out;
                            fprintf(newFile, "%s;", hotel.horario_checkIn);
                            fprintf(newFile, "%s;", hotel.horario_checkOut);
                        } else {
                            fputs(aux, newFile);
                        }
                    }
                }

            // Fechando os arquivos
            fclose(file);
            fclose(newFile);
            //Apagando o arquivo original
            remove("persist/hotel.txt");
            //Renomeando o arquivo
            rename("persist/tempHotel.txt", "persist/hotel.txt");
            break;
        }
        return SUCCESS;
}
