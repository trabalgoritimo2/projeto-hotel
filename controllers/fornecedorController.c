/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../structs/moduloCadastroStructs.h"
#include "../models/fornecedorModel.h"
#include "../models/funcoesModel.h"
#include "../models/configuracaoModel.h"

Fornecedor *fornecedorList;
#define SUCCESS 1;
#define ERROR 0;

// TODO: duas funções de excluir fornecedor?
// TODO: cadastrar arquivos em binários e txt
// TODO: gerar o id do fornecedor utilizando arquivo seq_fornecedor

/**
* Obtém a sequência de código do fornecedor no arquivo TXT
* @param
* @return int: o código do próximo registro
*/
int sequenceFornecedorArquivoTXT() {
    FILE *file = NULL;
    file = fopen("persist/seq_fornecedor.txt", "r+");
    if(file != NULL){
        char seqFornecedor[2];
        int seqFornecedorInt = 0;
        fgets(seqFornecedor, 2, file);
        seqFornecedorInt = atoi(strtok(seqFornecedor, ";"));
        fclose(file);
        seqFornecedorInt++;
        file = fopen("persist/seq_fornecedor.txt", "w");
        fprintf(file, "%d;", seqFornecedorInt);
        fclose(file);
        return seqFornecedorInt;
    }
    mostrarMensagemErroArquivo();
    return ERROR;
}

/**
* Obtém a sequência de código do fornecedor no arquivo Binário
* @param
* @return int: o código do próximo registro
*/
int sequenceFornecedorArquivoBinario() {
    FILE *file = NULL;
    file = fopen("persist/seq_fornecedor.bin", "r+b");
    if(file != NULL){
        int seqFornecedor;
        fread(&seqFornecedor, sizeof (int), 1, file);
        fclose(file);
        seqFornecedor++;
        file = fopen("persist/seq_fornecedor.bin", "wb");
        fwrite(&seqFornecedor, sizeof (int), 1, file);
        fclose(file);
        return seqFornecedor;
    }
    mostrarMensagemErroArquivo();
    return ERROR;
}

/**
 * Cadastrar fornecedor - Chamando a função
 * @param fornecedor fornecedor a ser cadastrado
 * @return int: 1- sucesso 0-erro
 */
int cadastrarFornecedorController(Fornecedor fornecedor) {
    return addFileFornecedor(fornecedor);
}

/**
 * Editar fornecedor - Chamando a função
 * @param fornecedor fornecedor a ser cadastrado
 * @return int: 1- sucesso 0-erro
 */
int atualizarFornecedorController(int cod, Fornecedor fornecedor) {
    return editarFornecedor(cod, fornecedor);
}

/**
 * Listar fornecedor - Chamando a função
 * @param fornecedor fornecedor a ser cadastrado
 * @return int: 1- sucesso 0-erro
 */
int listarForController() {
    listarFornecedor();
}

/**
 * Salva fornecedores ao arquivo txt/binario
 * @param fornecedor que vai ser adicionado ao arquivo
 * @return int valor para tratamento de erro
 */
int addFileFornecedor(Fornecedor fornecedor) {
    FILE *file;
    int opc = opcaoArmazenamentoUsuario();
    if(opc == 1){
        int fornecedorCount = contarfornecedor();
        fornecedor.forCodigo = fornecedorCount + 1;
        file = fopen("persist/fornecedor.bin", "a+b");
        if (file == NULL) {
            mostrarMensagemErroArquivo();
            fwrite(&fornecedor, sizeof(Fornecedor), 1, file);
        }
        fclose(file);
    } else if(opc == 2) {
        file = fopen("persist/fornecedor.txt", "a+");
        int fornecedorCount = contarfornecedor();
        fornecedor.forCodigo = sequenceFornecedorArquivoTXT();
        carregarfornecedor(fornecedorCount);
        printf("%d\n", fornecedorCount);
        printf("%d", fornecedorCount);
        if (file == NULL) {
            mostrarMensagemErroArquivo();
            return 0;
        }
        fprintf(file, "%d;%s;%s;%d;%s;%d;%s;%s;%s;%s;%s;%d;%s;%d;%d;\n",
                fornecedor.forCodigo,
                fornecedor.forNomeFantasia,
                fornecedor.forRazaoSocial,
                fornecedor.forInscricaoEstadual,
                fornecedor.forCnpj,
                fornecedor.forTelefone,
                fornecedor.forEmail,
                fornecedor.forEndereco.endRua,
                fornecedor.forEndereco.endBairro,
                fornecedor.forEndereco.endCidade,
                fornecedor.forEndereco.endEstado,
                fornecedor.forEndereco.endCep,
                fornecedor.forEndereco.endComplemento,
                fornecedor.forEndereco.endNumero, fornecedorCount);
        fclose(file);
        return 1;
    }
    return 0;
}


/**
 * @brief Conta a quantidade de fornecedores nos arquivos
 * @param
 * @return int retorna o valor da quatidade de fornecedores
 */
int contarfornecedor() {
    int filetype = opcaoArmazenamentoUsuario();
    FILE *file;
    int counter = 0;
    if (filetype == 1) {
        file = fopen("persist/fornecedor.bin", "rb");
        if (file == NULL) {
            mostrarMensagemErroArquivo();
            file = fopen("persist/fornecedor.bin", "w+b");
        }
        fseek(file, 0, SEEK_END);
        int count = ftell(file) / sizeof(Fornecedor);
        fclose(file);
        return count;
    } else if (filetype == 2) {
        file = fopen("persist/fornecedor.txt", "r");
        if (file == NULL) {
            printf("o arquivo de fornecedor(texto) não pôde ser aberto. Criando novo arquivo.\n");
            file = fopen("persist/fornecedor.txt", "w+");
        }
        char line[300];
        while (fgets(line, 300, file)) {
            counter++;
        }
        fclose(file);
    }
    return counter;
}

/**
 * Mostra os fornecedores ao usuario
 * @param
 * @return
 */
void listarFornecedor() {
    int fornecedorCount = contarfornecedor();
    carregarfornecedor(fornecedorCount);
    for (int i = 0; i < fornecedorCount; i++) {
        printf("Código: %d\nNome: %s\nCNPJ: %s\n", fornecedorList[i].forCodigo, fornecedorList[i].forNomeFantasia, fornecedorList[i].forCnpj);
    }
}

/**
 * Tranforma os dados do arquivo em Ponteiro
 * @param counter quantidade de Fornecedores existem salvos
 * @return
 */
void carregarfornecedor(int counter) {
    int filetype = opcaoArmazenamentoUsuario();
    Fornecedor *lista;
    FILE *file;
    lista = calloc(counter, sizeof(Fornecedor));
    if (filetype == 1) {
        file = fopen("persist/fornecedor.bin", "rb");
        if (file == NULL) {
            mostrarMensagemErroArquivo();
            return;
        }
        fread(lista, sizeof(Fornecedor), counter, file);
        fclose(file);
    } else if (filetype == 2) {
        file = fopen("persist/fornecedor.txt", "r");
        if (file == NULL) {
            printf("o arquivo (texto) não pôde ser aberto. Criando um novo arquivo.\n");
            file = fopen("persist/fornecedor.txt", "w+");
        } else {
            char line[300];
            for (int i = 0; i < counter; i++) {
                fgets(line, 300, file);
                char *forNomeFantasia = strtok(line, ";");
                strcpy(lista[i].forNomeFantasia, forNomeFantasia);
                char *forRazaoSocial = strtok(NULL, ";");
                strcpy(lista[i].forRazaoSocial, forRazaoSocial);
                lista[i].forInscricaoEstadual = atoi(strtok(NULL, ";"));
                char *forCnpj = strtok(NULL, ";");
                strcpy(lista[i].forCnpj, forCnpj);
                lista[i].forTelefone = atoi(strtok(NULL, ";"));
                char *dataNasc = strtok(NULL, ";");
                strcpy(lista[i].forEmail, dataNasc);
                char *rua = strtok(NULL, ";");
                strcpy(lista[i].forEndereco.endRua, rua);
                char *bairro = strtok(NULL, ";");
                strcpy(lista[i].forEndereco.endBairro, bairro);
                char *cidade = strtok(NULL, ";");
                strcpy(lista[i].forEndereco.endCidade, dataNasc);
                char *estado = strtok(NULL, ";");
                strcpy(lista[i].forEndereco.endEstado, estado);
                int cep = atoi(strtok(NULL, ";"));
                lista[i].forEndereco.endCep = cep;
                char *complemento = strtok(NULL, ";");
                strcpy(lista[i].forEndereco.endComplemento, complemento);
                lista[i].forEndereco.endNumero = atoi(strtok(NULL, ";"));
                lista[i].forCodigo = atoi(strtok(NULL, ";\n"));
            }
        }
    }
    fclose(file);
    fornecedorList = lista;
}

/**
 * Tranforma os dados do arquivo em Ponteiro
 * @param counter quantidade de Fornecedores existem salvos
 * @return
 */
Fornecedor* loadFornecedorList(int* count) {
    int filetype = opcaoArmazenamentoUsuario();
    Fornecedor *lista;
    FILE *file;
    int counter = contarfornecedor();
    lista = calloc(counter, sizeof(Fornecedor));
    if (filetype == 1) {
        file = fopen("persist/fornecedor.bin", "rb");
        if (file == NULL) {
            mostrarMensagemErroArquivo();
            return NULL;
        }
        fread(lista, sizeof(Fornecedor), counter, file);
        fclose(file);
    } else if (filetype == 2) {
        file = fopen("persist/fornecedor.txt", "r");
        if (file == NULL) {
            printf("o arquivo (texto) não pôde ser aberto. Criando um novo arquivo.\n");
            file = fopen("persist/fornecedor.txt", "w+");
        } else {
            char line[300];
            for (int i = 0; i < counter; i++) {
                fgets(line, 300, file);
                lista[i].forCodigo = atoi(strtok(line, ";\n"));
                char *forNomeFantasia = strtok(NULL, ";");
                strcpy(lista[i].forNomeFantasia, forNomeFantasia);
                char *forRazaoSocial = strtok(NULL, ";");
                strcpy(lista[i].forRazaoSocial, forRazaoSocial);
                lista[i].forInscricaoEstadual = atoi(strtok(NULL, ";"));
                char *forCnpj = strtok(NULL, ";");
                strcpy(lista[i].forCnpj, forCnpj);
                lista[i].forTelefone = atoi(strtok(NULL, ";"));
                char *dataNasc = strtok(NULL, ";");
                strcpy(lista[i].forEmail, dataNasc);
                char *rua = strtok(NULL, ";");
                strcpy(lista[i].forEndereco.endRua, rua);
                char *bairro = strtok(NULL, ";");
                strcpy(lista[i].forEndereco.endBairro, bairro);
                char *cidade = strtok(NULL, ";");
                strcpy(lista[i].forEndereco.endCidade, dataNasc);
                char *estado = strtok(NULL, ";");
                strcpy(lista[i].forEndereco.endEstado, estado);
                int cep = atoi(strtok(NULL, ";"));
                lista[i].forEndereco.endCep = cep;
                char *complemento = strtok(NULL, ";");
                strcpy(lista[i].forEndereco.endComplemento, complemento);
                lista[i].forEndereco.endNumero = atoi(strtok(NULL, ";"));
            }
        }
    }
    fclose(file);
    (*count) = counter;
    return lista;
}

/**
 * Função para editar Fornecedor
 * @param cod do fornecedor que sera editado
 * @param fornecedor dados novos do fornecedor
 * @return int: 1-sucesso 0-error
 */
int editarFornecedor(int cod, Fornecedor fornecedor) {
    int fornecedorCount = contarfornecedor();
    carregarfornecedor(fornecedorCount);
    int opc = opcaoArmazenamentoUsuario();
    Fornecedor *oldFornecedor = fornecedorList;
    FILE *file;
    if(opc == 1) {
        file = fopen("persist/fornecedor.bin", "wb");
        if(file == NULL){
            mostrarMensagemErroArquivo();
            return 0;
        }
        fclose(file);
    } else {
        file = fopen("persist/fornecedor.txt", "w");
        if(file == NULL) {
            mostrarMensagemErroArquivo();
            return 0;
        }
        fclose(file);
    }
    for (int i = 0; i < fornecedorCount; i++) {
        if (oldFornecedor[i].forCodigo == cod) {
            oldFornecedor[i] = fornecedor;
        };
    }
    for (int j = 0; j < fornecedorCount; j++) {
        addFileFornecedor(oldFornecedor[j]);
    }
}

/**
 * Função para excluir Fornecedor
 * @param cod do fornecedor que sera editado
 * @param fornecedor dados novos do fornecedor
 * @return int: 1-sucesso 0-error
 */
int excluirfornecedor(int cod) {
    setbuf(stdin, NULL);
    char escolha;
    printf("Tem certeza que deseja excluir o registro desse fornecedor? (s ou n) ");
    scanf("%c", &escolha);
    setbuf(stdin, NULL);
    if (escolha == 's' || escolha == 'S') {
        excluirfornecedorPorCodigo(cod);
    } else {
        printf("A acomodação desejada não pôde ser encontrada\n");
    }
}

/**
* Exclui o fornecedor do registro com o codigo passado por paramentro
* @param codigo
* @return
*/
void excluirfornecedorPorCodigo(int codigo) {
    int count = contarfornecedor();
    int opc = opcaoArmazenamentoUsuario();
    carregarfornecedor(count);
    FILE *file = NULL;
    if (opc == 1) {
        file = fopen("persist/fornecedor.bin", "w");
        if (file == NULL) {
            mostrarMensagemErroArquivo();
            return;
        }
        fclose(file);
    } else if (opc == 2) {
        file = fopen("persist/fornecedor.txt", "w");
        if (file == NULL) {
            return;
        }
        fclose(file);

        for (int i = 0; i < count; i++) {
            if (fornecedorList[i].forCodigo != codigo) {
                addFileFornecedor(fornecedorList[i]);
            }
        }
    }
}


/**
* Busca o usuário por usuUsuario
* @param char *usuUsuario: ponteiro usuUsuario
* @return usuario encontrado com o usuUsuario desejado
*/
Fornecedor buscaFornecedorPorCNPJ(char *forCnpj) {
    int tipoArquivo = opcaoArmazenamentoUsuario();
    FILE *file = NULL;
    Fornecedor fornecedor;
    switch (tipoArquivo) {
        case 1:
            file = fopen("persist/fornecedor.bin", "rb+");
            // Verificando se o arquivo foi aberto de forma correta
            if (file != NULL) {
                // Percorrendo cada linha procurando o nome de usuário
                while (fread(&fornecedor, sizeof (Fornecedor), 1, file)) {
                    if (strcmp(fornecedor.forCnpj, forCnpj) == 0) {
                        break;
                    }
                }
                // Fechando o arquivo
                fclose(file);
                // Se id for encontrado, então retorna o usuario
                if (strcmp(fornecedor.forCnpj, forCnpj) == 0) {
                    return fornecedor;
                }
                // Retorna fornecedor vazio com id == -1
                Fornecedor fornecedorVazio;
                fornecedor.forCodigo = -1;
                return fornecedorVazio;
            }
            break;
        case 2:
            file = fopen("persist/fornecedor.txt", "r+");
            //verificando se o arquivo foi aberto de forma correta
            if (file != NULL) {
                char fornecedorString[600];
                // Percorrendo cada linha procurando o nome de usuário
                while (fgets(fornecedorString, 600, file) != NULL) {
                    char *token = strtok(fornecedorString, ";");
                    for (int i = 0; token != NULL; i++) {
                        switch (i) {
                            case 0:
                                fornecedor.forCodigo = atoi(token);
                                break;
                            case 2:
                                strcpy(fornecedor.forRazaoSocial, token);
                                break;
                            case 4:
                                strcpy(fornecedor.forCnpj, token);
                                break;
                        }

                        token = strtok(NULL, ";");
                    }
                    if (strcmp(fornecedor.forCnpj, forCnpj) == 0) {
                        break;
                    }
                }
                fclose(file);

                // Se id for encontrado, então retorna o usuario
                if (strcmp(fornecedor.forCnpj, forCnpj) == 0) {
                    return fornecedor;
                }
                // Retorna usuario vazio com id == -1
                Fornecedor fornecedorVazio;
                fornecedorVazio.forCodigo = -1;
                return fornecedorVazio;
            }
            break;
        default:
            mostrarMensagemErroTipoArquivo();
            break;
    }
    if (file == NULL) {
        mostrarMensagemErroArquivo();
    }
    forCnpj = NULL;
}
