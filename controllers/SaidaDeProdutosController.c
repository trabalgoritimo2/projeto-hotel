/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../structs/moduloCadastroStructs.h"
#include "../models/funcoesModel.h"
#include "../models/configuracaoModel.h"
#include "../models/produtosModel.h"
#include "../models/SaidaDeProdutosModel.h"
#include "../models/hospedeModel.h"

#define SUCCESS 1
#define ERROR 0


void listarVendas() {
    int tipoArquivo = opcaoArmazenamentoUsuario();
    FILE *file;
    int qtdeVendas = 0;
    char line[300];
    Venda *vendas;
    Hospede hospede;
    switch (tipoArquivo){
        case 1:
            file = fopen("persist/venda.bin", "rb");
            fseek(file,0,SEEK_END);
            qtdeVendas = ftell(file)/sizeof(Venda);
            rewind(file);
            vendas = malloc(sizeof(Venda)*qtdeVendas);
            fread(vendas,sizeof(Venda),qtdeVendas,file);
            printf("=====================\n");
            printf("|       VENDAS      |\n");
            printf("=====================\n");
            
            for(int i=0;i<qtdeVendas;i++){
               printf("--------------------------- \n");
               printf("Código: %d \n",vendas[i].codigo);
               printf("Data: %d-%d-%d \n",vendas[i].data.dia,vendas[i].data.mes,vendas[i].data.ano);
               hospede = encontrarHospedePorCodigo(vendas[i].id_hospede);
               printf("Hóspede: %d: %s \n",hospede.hospCodigo,hospede.hospNome);
               printf("Valor total: R$%.2f \n",vendas[i].valor_total);
               printf("--------------------------- \n");
            }
            fclose(file);
            break;
        case 2:
            file = fopen("persist/venda.txt", "r");
            while(fgets(line,300,file)) {
                qtdeVendas++;
            }
            rewind(file);
            vendas = malloc(sizeof(Venda)*qtdeVendas);
            for(int i=0;i<qtdeVendas;i++) {
                fgets(line,300,file);
                vendas[i].codigo = atoi(strtok(line,";"));
                char *data = strtok(NULL, ";");
                vendas[i].id_hospede = atoi(strtok(NULL,";"));
                vendas[i].valor_total = atof(strtok(NULL,";"));
                vendas[i].data.dia = atoi(strtok(data,"-"));
                vendas[i].data.mes = atoi(strtok(NULL,"-"));
                vendas[i].data.ano = atoi(strtok(NULL,"-"));
            }
            printf("=====================\n");
            printf("|       VENDAS      |\n");
            printf("=====================\n");
            for(int i=0;i<qtdeVendas;i++){
               printf("--------------------------- \n");
               printf("Código: %d \n",vendas[i].codigo);
               printf("Data: %d-%d-%d \n",vendas[i].data.dia,vendas[i].data.mes,vendas[i].data.ano);
               hospede = encontrarHospedePorCodigo(vendas[i].id_hospede);
               printf("Hóspede: %d: %s \n",hospede.hospCodigo,hospede.hospNome);
               printf("Valor total: R$%.2f \n",vendas[i].valor_total);
               printf("--------------------------- \n");
            }
            free(vendas);
            fclose(file);
        default:
            mostrarMensagemErroMenu();
            break;
    }

}


int atualizarContaHospede(Conta_Hospede conta_Hospede) {
    int tipoArquivo = opcaoArmazenamentoUsuario();
    FILE *file;
    int qtdeContas = 0;
    char line[60];
    Conta_Hospede *contas;
    switch (tipoArquivo){
        case 1:
            file = fopen("persist/conta_hospede.bin", "rb");
            fseek(file,0,SEEK_END);
            qtdeContas = ftell(file)/sizeof(Conta_Hospede);
            rewind(file);
            contas = malloc(sizeof(Conta_Hospede)*qtdeContas);
            fread(contas,sizeof(Conta_Hospede),qtdeContas,file);
            for(int i=0;i<qtdeContas;i++){
                if(contas[i].id_hospede == conta_Hospede.id_hospede && contas[i].codigo == conta_Hospede.codigo){
                    contas[i] = conta_Hospede;
                }
            }
            fclose(file);
            file = fopen("persist/conta_hospede.bin", "wb");
            fwrite(contas,sizeof(Conta_Hospede),qtdeContas,file);
            free(contas);
            fclose(file);
            return SUCCESS;
            break;
        case 2:
            file = fopen("persist/conta_hospede.txt", "r");
            while(fgets(line,60,file)) {
                qtdeContas++;
            }
            rewind(file);
            contas = malloc(sizeof(Conta_Hospede)*qtdeContas);
            for(int i=0;i<qtdeContas;i++) {
                fgets(line,60,file);
                contas[i].codigo = atoi(strtok(line,";"));
                contas[i].id_hospede = atoi(strtok(NULL,";"));
                contas[i].valorTotal = atof(strtok(line,";"));
                if(contas[i].id_hospede == conta_Hospede.id_hospede && contas[i].codigo == conta_Hospede.codigo){
                    contas[i] = conta_Hospede;
                }
            }
            fclose(file);
            file = fopen("persist/conta_hospede.txt", "w");
            for(int i=0;i<qtdeContas;i++) {
                fprintf(file, "%d;", contas[i].codigo);
                fprintf(file, "%d;", contas[i].id_hospede);
                fprintf(file, "%f;\n", contas[i].valorTotal);
            }
            free(contas);
            fclose(file);
            return SUCCESS;
        default:
            mostrarMensagemErroMenu();
            return ERROR;
            break;
    }
}

Conta_Hospede buscarContaHospede (int hosCodigo) {
    int tipoArquivo = opcaoArmazenamentoUsuario();
    FILE *file;
    int qtdeContas = 0;
    char line[60];
    Conta_Hospede contaHospede;
    contaHospede.codigo = -1;
    Conta_Hospede *contas;
    switch (tipoArquivo){
        case 1:
            file = fopen("persist/conta_hospede.bin", "rb");
            fseek(file,0,SEEK_END);
            qtdeContas = ftell(file)/sizeof(Conta_Hospede);
            rewind(file);
            contas = malloc(sizeof(Conta_Hospede)*qtdeContas);
            fread(contas,sizeof(Conta_Hospede),qtdeContas,file);
            for(int i=0;i<qtdeContas;i++){
                if(contas[i].id_hospede == hosCodigo){
                    contaHospede = contas[i];
                    break;
                }
            }
            free(contas);
            fclose(file);
            return contaHospede;
            break;
        case 2:
            file = fopen("persist/conta_hospede.txt", "r");
            while(fgets(line,60,file)) {
                qtdeContas++;
            }
            rewind(file);
            contas = malloc(sizeof(Conta_Hospede)*qtdeContas);
            for(int i=0;i<qtdeContas;i++) {
                fgets(line,60,file);
                contas[i].codigo = atoi(strtok(line,";"));
                contas[i].id_hospede = atoi(strtok(NULL,";"));
                contas[i].valorTotal = atof(strtok(NULL,";"));
                if(contas[i].id_hospede == hosCodigo){
                    contaHospede = contas[i];
                    break;
                }
            }
            free(contas);
            fclose(file);
            return contaHospede;
        default:
            mostrarMensagemErroMenu();
            break;
    }
}

int cadastrarVenda(Venda venda, Venda_Produtos *venda_Produtos,int quantidadeVendaProdutos) {
    int tipoArquivo = opcaoArmazenamentoUsuario();
    FILE *file = NULL;
    FILE *file2 = NULL;

    switch (tipoArquivo){
        case 1:
            file = fopen("persist/venda.bin", "a+b");
            file2 = fopen("persist/venda_produtos.bin", "a+b");
            // Verificando se o arquivo foi aberto de forma correta
            if (file != NULL && file2 != NULL) {
                fwrite(&venda, sizeof (Venda), 1, file);
                for(int i=0;i<quantidadeVendaProdutos;i++) {
                    venda_Produtos[i].codigo = sequenceArquivoBinario("persist/seq_venda_produtos.bin");
                    fwrite(&venda_Produtos[i], sizeof (Venda_Produtos), 1, file2);
                }
                fclose(file);
                fclose(file2);
                return SUCCESS;
            }
            mostrarMensagemErroArquivo();
            return ERROR;
            break;
    case 2:
        file = fopen("persist/venda.txt", "a+");
        file2 = fopen("persist/venda_produtos.txt", "a+");
        if(file != NULL && file2 != NULL){
            fprintf(file, "%d;", venda.codigo);
            fprintf(file, "%d-%d-%d;", venda.data.dia,venda.data.mes,venda.data.ano);
            fprintf(file, "%d;", venda.id_hospede);
            fprintf(file, "%f;\n", venda.valor_total);

            for(int i=0;i<quantidadeVendaProdutos;i++) {
                venda_Produtos[i].codigo = sequenceArquivoTXT("persist/seq_venda_produtos.txt");
                fprintf(file2, "%d;", venda_Produtos[i].codigo);
                fprintf(file2, "%d;", venda_Produtos[i].id_produto);
                fprintf(file2, "%d;", venda_Produtos[i].id_venda);
                fprintf(file2, "%d;\n", venda_Produtos[i].quantidade);
            }
            fclose(file);
            fclose(file2);
            return SUCCESS;
        }
        mostrarMensagemErroArquivo();
        return ERROR;
    default:
        mostrarMensagemErroMenu();
        break;
    }
}

int cadastrarContaHospede(Conta_Hospede conta_Hospede) {
    int tipoArquivo = opcaoArmazenamentoUsuario();
    FILE *file = NULL;

    switch (tipoArquivo){
        case 1:
            file = fopen("persist/conta_hospede.bin", "a+b");
            // Verificando se o arquivo foi aberto de forma correta
            if (file != NULL) {
                conta_Hospede.codigo = sequenceArquivoBinario("persist/seq_conta_hospede.bin");
                fwrite(&conta_Hospede, sizeof (Conta_Hospede), 1, file);
                fclose(file);
                return SUCCESS;
            }
            mostrarMensagemErroArquivo();
            return ERROR;
            break;
    case 2:
        file = fopen("persist/conta_hospede.txt", "a+");
        if(file != NULL){
            conta_Hospede.codigo = sequenceArquivoTXT("persist/seq_conta_hospede.txt");
            fprintf(file, "%d;", conta_Hospede.codigo);
            fprintf(file, "%d;", conta_Hospede.id_hospede);
            fprintf(file, "%f;\n", conta_Hospede.valorTotal);
            fclose(file);
            return SUCCESS;
        }
        mostrarMensagemErroArquivo();
        return ERROR;
    default:
        mostrarMensagemErroMenu();
        break;
    }
}


int apagarContaHospede(Conta_Hospede conta_Hospede) {
    int tipoArquivo = opcaoArmazenamentoUsuario();
    FILE *file;
    int qtdeContas = 0;
    char line[60];
    Conta_Hospede *contas;
    switch (tipoArquivo){
        case 1:
            file = fopen("persist/conta_hospede.bin", "rb");
            fseek(file,0,SEEK_END);
            qtdeContas = ftell(file)/sizeof(Conta_Hospede);
            rewind(file);
            contas = malloc(sizeof(Conta_Hospede)*qtdeContas);
            fread(contas,sizeof(Conta_Hospede),qtdeContas,file);
            fclose(file);
            file = fopen("persist/conta_hospede.bin", "wb");
            for(int i=0;i<qtdeContas;i++){
                if(contas[i].id_hospede != conta_Hospede.id_hospede && contas[i].codigo != conta_Hospede.codigo){
                    fwrite(&contas[i],sizeof(Conta_Hospede),1,file);
                }
            }
            free(contas);
            fclose(file);
            return SUCCESS;
            break;
        case 2:
            file = fopen("persist/conta_hospede.txt", "r");
            while(fgets(line,60,file)) {
                qtdeContas++;
            }
            rewind(file);
            contas = malloc(sizeof(Conta_Hospede)*qtdeContas);
            for(int i=0;i<qtdeContas;i++) {
                fgets(line,60,file);
                contas[i].codigo = atoi(strtok(line,";"));
                contas[i].id_hospede = atoi(strtok(NULL,";"));
                contas[i].valorTotal = atof(strtok(line,";"));
            }
            fclose(file);
            file = fopen("persist/conta_hospede.txt", "w");
            for(int i=0;i<qtdeContas;i++) {
                if(contas[i].id_hospede != conta_Hospede.id_hospede && contas[i].codigo != conta_Hospede.codigo){
                    fprintf(file, "%d;", contas[i].codigo);
                    fprintf(file, "%d;", contas[i].id_hospede);
                    fprintf(file, "%f;\n", contas[i].valorTotal);
                }
            }
            free(contas);
            fclose(file);
            return SUCCESS;
        default:
            mostrarMensagemErroMenu();
            return ERROR;
            break;
    }
}
