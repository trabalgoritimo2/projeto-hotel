/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../structs/moduloCadastroStructs.h"
#include "../models/reservaModel.h"
#include "../models/funcoesModel.h"
#include "../models/configuracaoModel.h"
#include "../models/acomodacaoModel.h"
#include "../models/hospedeModel.h"

#define SUCCESS 1;
#define ERROR 0;

/**
* Compara se 2 reservas estão em conflito de data e acomodação
* @param Reserva reservaNova: a reserva que se deseja fazer
* @param Reserva reservaExistente: a reserva que já está registrada
* @return 1 se elas coincide, 0 se não há conflito
*/
int reservasCoincidem(Reserva reservaNova, Reserva reservaExistente) {
    // se não é a mesma acomodação, não está atrapalhando
    if (reservaNova.resAcomodacaoId != reservaExistente.resAcomodacaoId){
            return 0;
    }
    // se a data de inicio é anterior a data de inicio da referencia
    if (compararDatas(reservaNova.resDataInicio,reservaExistente.resDataInicio) == -1) {
        // então a data final deve ser menor que a data de inicio da referencia
        if (compararDatas(reservaNova.resDataFim, reservaExistente.resDataInicio) == -1) {
            return 0; // rev1 termina antes de começar rev2
        } else {
            return 1; // rev1 termina enquanto rev2 ainda não
        }
        // se a data de inicio é posterior a data final da referencia
    } else if (compararDatas(reservaNova.resDataInicio, reservaExistente.resDataFim) == 1) {
        return 0; // rev1 começa depois que rev2 termina
    }
    return 1;
}

/**
* Compara se as informações de uma reserva são iguais as informações passadas
* @param Reserva reserva: a reserva a ser comparada
* @param Data resDataInicio: data de inicio a se comparar
* @param Data resDataFim: data final a se comparar
* @param int resHospedeId: ID do hospede que reservou
* @return 1 se todas as informações forem iguais, 0 do contrario
*/
int compararReservas(Reserva reserva, Data resDataInicio, Data resDataFim, int resHospedeId) {
    return (reserva.resHospedeId == resHospedeId
                && reserva.resDataInicio.dia == resDataInicio.dia
                && reserva.resDataInicio.mes == resDataInicio.mes
                && reserva.resDataInicio.ano == resDataInicio.ano
                && reserva.resDataFim.dia == resDataFim.dia
                && reserva.resDataFim.mes == resDataFim.mes
                && reserva.resDataFim.ano == resDataFim.ano);
}

/**
* Buscar reserva
* @param Reserva a ser cadastrada e/ou encontrada
* @return Reserva: se não for encontrado retorna reserva.resCodigo = -1
*/
Reserva buscarReserva (Reserva reservaNova) {
    int tipoArquivo = opcaoArmazenamentoUsuario();
    FILE *file = NULL;
    Reserva reserva;
    char *saveptr1 = NULL;
    char *tokenDataInicio = NULL;
    char *saveptr2 = NULL;
    char *tokenDateFim = NULL;

    switch (tipoArquivo) {
        case 1:
            file = fopen("persist/reserva.bin", "rb+");
            if (file != NULL) {
                // Percorrendo cada linha procurando a data início, data fim e código do Hospede
                while (fread(&reserva, sizeof (Reserva), 1, file)) {
                    // Se encontrar, para o while
                    if (reservasCoincidem(reservaNova,reserva)) {
                        break;
                    }
                }
                // Fechando o arquivo
                fclose(file);
                // Se for encontrado, retorna a reserva
                if (reservasCoincidem(reservaNova,reserva)) {
                    return reserva;
                }
                // Se não for encontrado, retorna reserva.resCodigo = -1
                Reserva reservaVazio;
                reservaVazio.resCodigo = -1;
                return reservaVazio;
            }
            break;
        case 2:
            file = fopen("persist/reserva.txt", "r+");
            if (file != NULL) {
                char reservaString[500];
                // Percorrendo cada linha procurando a data início, data fim e código do Hospede
                while (fgets(reservaString, 500, file) != NULL) {
                    reserva = strPraResv(reservaString);
                    
                    // Se encontrar, para o while
                    if (reservasCoincidem(reservaNova,reserva)) {
                        break;
                    }
                }
                // Fechando o arquivo
                fclose(file);
                // Se for encontrado, retorna a reserva
                if (reservasCoincidem(reservaNova,reserva)) {
                    return reserva;
                }
               // Se não for encontrado, retorna reserva.resCodigo = -1
                Reserva reservaVazio;
                reservaVazio.resCodigo = -1;
                return reservaVazio;
            }
            break;
        default:
            mostrarMensagemErroTipoArquivo();
            break;
    }
    if (file == NULL) {
        mostrarMensagemErroArquivo();
    }
}

/**
* Busca reserva já cadastrada
* @param resDataInicio, resDataFim, resHospedeId
* @return Reserva: se não for encontrado retorna reserva.resCodigo = -1
*/
Reserva buscarReservaRealizadas (Data resDataInicio, Data resDataFim, int resHospedeId) {
    int tipoArquivo = opcaoArmazenamentoUsuario();
    FILE *file = NULL;
    Reserva reserva;
    char *saveptr1 = NULL;
    char *tokenDataInicio = NULL;
    char *saveptr2 = NULL;
    char *tokenDateFim = NULL;

    switch (tipoArquivo) {
        case 1:
            file = fopen("persist/reserva.bin", "rb+");
            if (file != NULL) {
                // Percorrendo cada linha procurando a data início, data fim e código do Hospede
                while (fread(&reserva, sizeof (Reserva), 1, file)) {
                    // Se encontrar, para o while
                    if (compararReservas(reserva,resDataInicio, resDataFim, resHospedeId)) {
                        break;
                    }
                }
                // Fechando o arquivo
                fclose(file);
                // Se for encontrado, retorna a reserva
                if (compararReservas(reserva,resDataInicio, resDataFim, resHospedeId)) {
                    return reserva;
                }
                // Se não for encontrado, retorna reserva.resCodigo = -1
                Reserva reservaVazio;
                reservaVazio.resCodigo = -1;
                return reservaVazio;
            }
            break;
        case 2:
            file = fopen("persist/reserva.txt", "r+");
            if (file != NULL) {
                char reservaString[500];
                // Percorrendo cada linha procurando a data início, data fim e código do Hospede
                while (fgets(reservaString, 500, file) != NULL) {
                    reserva = strPraResv(reservaString);

                    // Se encontrar, para o while
                    if (compararReservas(reserva,resDataInicio, resDataFim, resHospedeId)) {
                        break;
                    }
                }
                // Fechando o arquivo
                fclose(file);
                // Se for encontrado, retorna a reserva
                if (compararReservas(reserva,resDataInicio, resDataFim, resHospedeId)) {
                    return reserva;
                }
               // Se não for encontrado, retorna reserva.resCodigo = -1
                Reserva reservaVazio;
                reservaVazio.resCodigo = -1;
                return reservaVazio;
            }
            break;
        default:
            mostrarMensagemErroTipoArquivo();
            break;
    }
    if (file == NULL) {
        mostrarMensagemErroArquivo();
    }
}

/**
* Cadastrar uma reserva na base de dados
* @param Reserva reserva: reserva a ser cadastrada, int tipoArquivo: 1- Binário 2- TXT
* @return 1- se cadastrou com sucesso 0: se houve algum problema
*/
int cadastrarReserva (Reserva reserva) {
    int tipoArquivo = opcaoArmazenamentoUsuario();
    FILE *file = NULL;
    reserva.resStatus_pagamento = 0;
    reserva.resDataTime_chegada = dataTimeZero();
    reserva.resDataTime_saida = dataTimeZero();
    switch(tipoArquivo){
        case 1:
            file = fopen("persist/reserva.bin", "a+b");
            // Verificando se o arquivo foi aberto de forma correta
            if (file != NULL) {
                // Buscando o código do próximo registro no arquivo binário
                reserva.resCodigo = sequenceArquivoBinario("persist/seq_reserva.bin");
                if (reserva.resCodigo != 0) {
                    fwrite(&reserva, sizeof (Reserva), 1, file);
                    fclose(file);
                    return SUCCESS;
                }
            }
            mostrarMensagemErroArquivo();
            return ERROR;
        break;
        case 2:
            file = fopen("persist/reserva.txt", "a+");
            // Verificando se o arquivo foi aberto de forma correta
            if(file != NULL){
            // Buscando o código do próximo registro no arquivo TXT
                reserva.resCodigo = sequenceArquivoTXT("persist/seq_reserva.txt");
                if(reserva.resCodigo != 0){
                    salvarReservaEmArquivoTexto(file, reserva);
                    fclose(file);
                    return SUCCESS;
                }
            }
            mostrarMensagemErroArquivo();
            return ERROR;
            break;
        default:
            mostrarMensagemErroTipoArquivo();
            return ERROR;
            break;
    }
}

/**
* Buscar acomodação por código
* @param int resAcomodacaoId: codigo da acomodacao
* @return Acomodacao que possui o código desejado
*/
Acomodacao buscarAcomodacaoReserva(int resAcomodacaoId) {
    int acomodacaoCount = contarAcomodacoes();
    int index = 0;
    Acomodacao* todasAcomodacoes = carregarAcomodacao(acomodacaoCount);
    for (int i=0; i<acomodacaoCount; i++){
        if (todasAcomodacoes[i].acoCodigo == resAcomodacaoId){
            return todasAcomodacoes[i];
        }
    }
}

/**
* Mostra na tela as acomodaçoes disponiveis para reservar
* @param Reserva reservaNova: a reserva a ser cadastrada
* @param int catCodigo: codigo da categoria da acomodação desejada
* @return void
*/
void buscarAcomodacoesDisponiveis(Reserva reservaNova, int catCodigo) {
    int acomodacaoCount = contarAcomodacoes();
    int countFiltradas = 0;
    int index = 0;
    int index2 = 0;
    Acomodacao* todasAcomodacoes = carregarAcomodacao(acomodacaoCount);
    Acomodacao* acomodacoesFiltradas = NULL;

    for (int i=0; i<acomodacaoCount; i++){
        if (todasAcomodacoes[i].acoCategoria.catCodigo == catCodigo) {
            countFiltradas++;
        }
    }
    acomodacoesFiltradas = (Acomodacao *) malloc(sizeof (Acomodacao) * countFiltradas);

    for (int i=0; i<acomodacaoCount; i++){
        if (todasAcomodacoes[i].acoCategoria.catCodigo == catCodigo){
            acomodacoesFiltradas[index] = todasAcomodacoes[i];
            index++;
        }
    }

    for(int i=0;i<countFiltradas;i++) {
        reservaNova.resAcomodacaoId = acomodacoesFiltradas[i].acoCodigo;
        Reserva reserva;
        reserva = buscarReserva(reservaNova);
        if(reserva.resCodigo == -1) {
            printf("\n \n ACOMODAÇÃO %d:", acomodacoesFiltradas[i].acoCodigo);
            printf("\n Características: %s", acomodacoesFiltradas[i].acoCaracteristicas);
            printf("\n Descrição: %s \n", acomodacoesFiltradas[i].acoDescricao);
        }
    }
}

/**
* Cancelar a reserva por código
* @param int codigo: codigo da reserva que se deseja cancelar
* @return void
*/
void cancelarReservaPorCodigo(int codigo) {
    FILE* file = NULL;
    int tipoArquivo = opcaoArmazenamentoUsuario();
    Reserva* resvReg = NULL;
    int qtReservas = 0;

    if (tipoArquivo == 1){
        file = fopen("persist/reserva.bin", "rb");
        if (file == NULL){
            mostrarMensagemErroArquivo();
            return;
        }

        fseek(file, 0, SEEK_END);
        qtReservas = ftell(file) / sizeof(Reserva);
        rewind(file);

        resvReg = malloc(sizeof(Reserva) * qtReservas);
        fread(resvReg, sizeof(Reserva), qtReservas, file);
        fclose(file);

        // abre, para salvar as alterações
        file = fopen("persist/reserva.bin", "wb");
        if (file == NULL){
            mostrarMensagemErroArquivo();
            return;
        }

        for (int i=0; i<qtReservas; i++){
            if (resvReg[i].resCodigo != codigo){
                fwrite(&resvReg[i], sizeof(Reserva), 1, file);
            }
        }
        fclose(file);
        free(resvReg);
    }

    if (tipoArquivo == 2){
        file = fopen("persist/reserva.txt", "r");
        if (file == NULL){
            mostrarMensagemErroArquivo();
            return;
        }

        // conta reservas registradas
        char line[300];
        while (fgets(line, 300, file))
        {
            qtReservas++;
        }
        rewind(file);

        resvReg = malloc(sizeof(Reserva) * qtReservas);
        // carrega as informações do arquivo
        for (int i=0; i< qtReservas; i++){
            fgets(line, 300, file);
            resvReg[i] = strPraResv(line);
        }
        rewind(file);
        fclose(file);

        // abre, para salvar as alterações
        file = NULL;
        file = fopen("persist/reserva.txt", "w");
        if (file == NULL){
            mostrarMensagemErroArquivo();
            return;
        }

        // salva uma por uma,mas se o proximo codigo for o requisitado, pula a proxima
        for (int i=0; i<qtReservas; i++){
            if (resvReg[i].resCodigo != codigo){
                salvarReservaEmArquivoTexto(file, resvReg[i]);
            }
        }
        fclose(file);
        free(resvReg);
    }
}

/**
* Cria um objeto de reserva baseado na string toketizada
* @param char* line: informações de reserva em texto
* @return Objeto de reserva criado
*/
Reserva strPraResv(char* line) {
    Reserva resv;
    resv.resCodigo = atoi(strtok(line, ";\n"));
    resv.resHospedeId = atoi(strtok(NULL, ";\n"));
    resv.resAcomodacaoId = atoi(strtok(NULL, ";\n"));
    resv.resStatus_pagamento = atoi(strtok(NULL, ";\n"));
    char* dataI = strtok(NULL, ";\n");
    char* dataF = strtok(NULL, ";\n");
    char* dataTimeChe = strtok(NULL, ";\n");
    char* dataTimeSai = strtok(NULL, ";\n");

    resv.resDataInicio.dia = atoi(strtok(dataI, "-\n"));
    resv.resDataInicio.mes = atoi(strtok(NULL, "-\n"));
    resv.resDataInicio.ano = atoi(strtok(NULL, "-\n"));

    resv.resDataFim.dia = atoi(strtok(dataF, "-\n"));
    resv.resDataFim.mes = atoi(strtok(NULL, "-\n"));
    resv.resDataFim.ano = atoi(strtok(NULL, "-\n"));

    char* dataChe = strtok(dataTimeChe, "/\n");
    char* horaChe = strtok(NULL, "/\n");
    resv.resDataTime_chegada.dia = atoi(strtok(dataChe, "-\n"));
    resv.resDataTime_chegada.mes = atoi(strtok(NULL, "-\n"));
    resv.resDataTime_chegada.ano = atoi(strtok(NULL, "-\n"));

    resv.resDataTime_chegada.hora = atoi(strtok(horaChe, ":\n"));
    resv.resDataTime_chegada.minuto = atoi(strtok(NULL, ":\n"));
    resv.resDataTime_chegada.segundo = atoi(strtok(NULL, ":\n"));

    char* dataSai = strtok(dataTimeSai, "/\n");
    char* horaSai = strtok(NULL, "/\n");
    resv.resDataTime_saida.dia = atoi(strtok(dataSai, "-\n"));
    resv.resDataTime_saida.mes = atoi(strtok(NULL, "-\n"));
    resv.resDataTime_saida.ano = atoi(strtok(NULL, "-\n"));

    resv.resDataTime_saida.hora = atoi(strtok(horaSai, ":\n"));
    resv.resDataTime_saida.minuto = atoi(strtok(NULL, ":\n"));
    resv.resDataTime_saida.segundo = atoi(strtok(NULL, ":\n"));

    return resv;
}

/*
* Salva a reserva em um arquivo de texto
* @param FILE* file: arquivo para salvar
* @param Reserva reserva: a reserva que será salva
* @return void
*/
void salvarReservaEmArquivoTexto(FILE* file, Reserva reserva) {
    if (file != NULL) {
        fprintf(file, "%d;", reserva.resCodigo);
        fprintf(file, "%d;", reserva.resHospedeId);
        fprintf(file, "%d;", reserva.resAcomodacaoId);
        fprintf(file, "%d;", reserva.resStatus_pagamento);
        fprintf(file, "%d-%d-%d;", reserva.resDataInicio.dia,reserva.resDataInicio.mes,reserva.resDataInicio.ano);
        fprintf(file, "%d-%d-%d;", reserva.resDataFim.dia,reserva.resDataFim.mes,reserva.resDataFim.ano);
        fprintf(file, "%d-%d-%d/%d:%d:%d;"
                    ,reserva.resDataTime_chegada.dia
                    ,reserva.resDataTime_chegada.mes
                    ,reserva.resDataTime_chegada.ano
                    ,reserva.resDataTime_chegada.hora
                    ,reserva.resDataTime_chegada.minuto
                    ,reserva.resDataTime_chegada.segundo
                    );
        fprintf(file, "%d-%d-%d/%d:%d:%d;"
                    ,reserva.resDataTime_saida.dia
                    ,reserva.resDataTime_saida.mes
                    ,reserva.resDataTime_saida.ano
                    ,reserva.resDataTime_saida.hora
                    ,reserva.resDataTime_saida.minuto
                    ,reserva.resDataTime_saida.segundo
                    );
        fprintf(file, "\n");
    }
}

/**
* Mostra na tela todas as reservas registradas
* @param
* @return void
*/
void listarReservas() {
    FILE* file = NULL;
    int tipoArquivo = opcaoArmazenamentoUsuario();
    Reserva* resvReg = NULL;
    Acomodacao acomodacao;
    Hospede hospede;
    int qtReservas = 0;
    printf("=================================\n");
    printf("|       RESERVAS REALIZADAS     |\n");
    printf("=================================\n");
    if (tipoArquivo == 1) {
        file = fopen("persist/reserva.bin", "rb");
        if (file == NULL){
            mostrarMensagemErroArquivo();
            return;
        }
        fseek(file, 0, SEEK_END);
        qtReservas = ftell(file) / sizeof(Reserva);
        rewind(file);

        resvReg = malloc(sizeof(Reserva) * qtReservas);
        fread(resvReg, sizeof(Reserva), qtReservas, file);
        fclose(file);
        for(int i=0; i<qtReservas; i++){
            printf("Código: %d\n", resvReg[i].resCodigo);
            hospede = encontrarHospedePorCodigo(resvReg[i].resHospedeId);
            printf("Hóspede: %d\n", hospede.hospNome);
            acomodacao = buscarAcomodacaoPorCodigo(resvReg[i].resAcomodacaoId);
            printf("Acomodacao: %d : %s \n", acomodacao.acoCodigo, acomodacao.acoDescricao);
            printf("Data de inicio: %d-%d-%d\n", resvReg[i].resDataInicio.dia,resvReg[i].resDataInicio.mes,resvReg[i].resDataInicio.ano);
            printf("Data de fim: %d-%d-%d \n\n", resvReg[i].resDataFim.dia,resvReg[i].resDataFim.mes,resvReg[i].resDataFim.ano);
        }
        printf("%d Reservas registradas\n", qtReservas);
        free(resvReg);
    }

    if (tipoArquivo == 2){
        file = fopen("persist/reserva.txt", "r");
        if (file == NULL){
            mostrarMensagemErroArquivo();
            return;
        }

        // conta reservas registradas
        char line[300];
        while (fgets(line, 300, file))
        {
            qtReservas++;
        }
        rewind(file);

        resvReg = malloc(sizeof(Reserva) * qtReservas);
        // carrega as informações do arquivo
        for (int i=0; i< qtReservas; i++){
            fgets(line, 300, file);
            resvReg[i] = strPraResv(line);
        }
        fclose(file);

        printf("%d Reservas registradas\n", qtReservas);
        for(int i=0; i<qtReservas; i++){
            printf("\nCódigo: %d\n", resvReg[i].resCodigo);
            hospede = encontrarHospedePorCodigo(resvReg[i].resHospedeId);
            printf("Hóspede: %d: %s\n",hospede.hospCodigo, hospede.hospNome);
            acomodacao = buscarAcomodacaoPorCodigo(resvReg[i].resAcomodacaoId);
            printf("Acomodação: %d : %s \n", acomodacao.acoCodigo, acomodacao.acoDescricao);
            printf("Data de inicio: %d-%d-%d\n", resvReg[i].resDataInicio.dia,resvReg[i].resDataInicio.mes,resvReg[i].resDataInicio.ano);
            printf("Data de fim: %d-%d-%d\n\n", resvReg[i].resDataFim.dia,resvReg[i].resDataFim.mes,resvReg[i].resDataFim.ano);
        }
        free(resvReg);
    }
}

/**
* verifica se a reserva com o ID passado existe
* @param int id: numero de ID da reserva que se busca
* @return 1 se a reserva com esse ID existir, 0 do contrário
*/
int reservaExiste(int id) {
    int counter = 0;
    Reserva* reservas = carregarReservas(&counter);
    for (int i = 0; i < counter; i++) {
        // se a reserva registrada tiver o ID igual ao passado, retorna verdadeiro
        if (reservas[i].resCodigo == id) {
            free(reservas);
            return 1;
        }
    }
    // se nenhuma for encontrada, retorna falso
    free(reservas);
    return 0;
}

/**
* Carrega as reservas gravadas no arquivo
* @param int* counter: endereço de uma variavel usada para armazenar a quantidade de reservas registradas
* @return endereço de um vetor de reservas de tamanho especificado em 'counter', ou NULL se der errado
*/
Reserva* carregarReservas(int* counter) {
    FILE* file = NULL;
    int tipoArquivo = opcaoArmazenamentoUsuario();
    Reserva* reservas = NULL;
    int qtReservas = 0;

    if (tipoArquivo == TipoArquivoBinario) {
        file = fopen("persist/reserva.bin", "rb");
        if (file == NULL){
            mostrarMensagemErroArquivo();
            return NULL;
        }
        fseek(file, 0, SEEK_END);
        qtReservas = ftell(file) / sizeof(Reserva);
        rewind(file);

        reservas = malloc(sizeof(Reserva) * qtReservas);
        fread(reservas, sizeof(Reserva), qtReservas, file);
        fclose(file);
    }
    else if (tipoArquivo == 2){
        file = fopen("persist/reserva.txt", "r");
        if (file == NULL){
            mostrarMensagemErroArquivo();
            return NULL;
        }

        // conta reservas registradas
        char line[300];
        while (fgets(line, 300, file))
        {
            qtReservas++;
        }
        rewind(file);

        reservas = malloc(sizeof(Reserva) * qtReservas);
        // carrega as informações do arquivo
        for (int i=0; i< qtReservas; i++){
            fgets(line, 300, file);
            reservas[i] = strPraResv(line);
        }
        fclose(file);
    }
    if (reservas != NULL){
        *counter = qtReservas;
    }
    return reservas;
}

/**
* Salva as reservas no arquivo
* @param Reserva* reservas: o vetor de informações para serem salvas
* @param int counter: a quantidade de itens presentes no vetor de categorias
* @return
*/
void salvarReservas(Reserva* reservas, int counter) {
    FILE* file = NULL;
    int tipoArquivo = opcaoArmazenamentoUsuario();

    if (tipoArquivo == TipoArquivoBinario) {
        file = fopen("persist/reserva.bin", "wb");
        if (file == NULL) {
            mostrarMensagemErroArquivo();
        }

        fwrite(reservas, sizeof(Reserva), counter, file);
        fclose(file);
    }
    else if (tipoArquivo == TipoArquivoTexto) {
        file = fopen("persist/reserva.txt", "w");
        if (file == NULL) {
            mostrarMensagemErroArquivo();
        }

        for (int i=0; i<counter; i++) {
            salvarReservaEmArquivoTexto(file, reservas[i]);
        }
        fclose(file);
    }
}

/*
* encontra todas as reservas associadas ao hospede com id hosCodigo, que tem data final marcadas para hoje ou após
*/
Reserva* encontrarReservasDoHospede(int hosCodigo, int* encontradas) {
    //carrega as reservas do arquivo
    int reservaCount;
    Reserva* reservas = carregarReservas(&reservaCount);
    // prepara para separar as reservas do hospede
    int reservasHospedeCount = 0;
    Reserva* reservasDoHospede = NULL;
    Data dataAtual = gerarDataAtual();
    for (int i=0; i<reservaCount; i++) {
        // procura por reservas com o mesmo ID do hospede, e que a data final seja posterior ou iguais ao dia de hoje
        if (reservas[i].resHospedeId == hosCodigo) {
            if (compararDatas(reservas[i].resDataFim, dataAtual) >= 0 || reservas[i].resStatus_pagamento == status_aguardando_pagamento) {
                reservasHospedeCount++;
                reservasDoHospede = realloc(reservasDoHospede, sizeof(Reserva) * reservasHospedeCount);
                reservasDoHospede[reservasHospedeCount-1] = reservas[i];
            }
        }
    }
    free(reservas);
    *encontradas = reservasHospedeCount;
    return reservasDoHospede;
}
