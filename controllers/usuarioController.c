/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../structs/moduloCadastroStructs.h"
#include "../models/usuarioModel.h"
#include "../models/funcoesModel.h"
#include "../models/configuracaoModel.h"

#define SUCCESS 1;
#define ERROR 0;

//TODO: não matar a execução quando encontrar um usuário já cadastrado no sistema no momento do cadastro
//TODO: colocar 'dialog' de confirmação de exclusão do cadastro

/**
* Busca o usuário por usuUsuario
* @param char *usuUsuario: ponteiro usuUsuario
* @return usuario encontrado com o usuUsuario desejado
*/
Usuario buscarUsuarioPorNomeUsuario(char *usuUsuario) {
    int tipoArquivo = opcaoArmazenamentoUsuario();
    FILE *file = NULL;
    Usuario user;
    switch (tipoArquivo) {
        case 1:
            file = fopen("persist/usuario.bin", "rb+");
            // Verificando se o arquivo foi aberto de forma correta
            if (file != NULL) {
                // Percorrendo cada linha procurando o nome de usuário
                while (fread(&user, sizeof (Usuario), 1, file)) {
                    if (strcmp(user.usuUsuario, usuUsuario) == 0) {
                        break;
                    }
                }
                // Fechando o arquivo
                fclose(file);
                // Se id for encontrado, então retorna o usuario
                if (strcmp(user.usuUsuario, usuUsuario) == 0) {
                    return user;
                }
                // Retorna usuario vazio com id == -1
                Usuario usuarioVazio;
                usuarioVazio.usuCodigo = -1;
                return usuarioVazio;
            }
            break;
        case 2:
            file = fopen("persist/usuario.txt", "r+");
            //verificando se o arquivo foi aberto de forma correta
            if (file != NULL) {
                char usuarioString[300];
                // Percorrendo cada linha procurando o nome de usuário
                while (fgets(usuarioString, 300, file) != NULL) {
                    char *token = strtok(usuarioString, ";");
                    for (int i = 0; token != NULL; i++) {
                        switch (i) {
                            case 0:
                                user.usuCodigo = atoi(token);
                                break;
                            case 1:
                                strcpy(user.usuNome, token);
                                break;
                            case 2:
                                strcpy(user.usuUsuario, token);
                                break;
                            case 3:
                                strcpy(user.usuSenha, token);
                                break;
                            case 4:
                                 user.usuPermissao = atoi(token);
                                break;
                            default:
                                break;
                        }

                        token = strtok(NULL, ";");
                    }
                    if (strcmp(user.usuUsuario, usuUsuario) == 0) {
                        break;
                    }
                }
                fclose(file);

                // Se id for encontrado, então retorna o usuario
                if (strcmp(user.usuUsuario, usuUsuario) == 0) {
                    return user;
                } 
                // Retorna usuario vazio com id == -1
                Usuario usuarioVazio;
                usuarioVazio.usuCodigo = -1;
                return usuarioVazio;
            }
            break;
        default:
            mostrarMensagemErroTipoArquivo();
            break;
    }
    if (file == NULL) {
        mostrarMensagemErroArquivo();
    }
    usuUsuario = NULL;
}

/**
* Cadastrar usuário na base de dados
* @param Usuario usuario: usuário a ser cadastrado, int tipoArquivo: 1- Binário 2- TXT
* @return 1- se cadastrou com sucesso 0: se houve algum problema
*/
int cadastrarUsuario (Usuario usuario) {
    FILE *file = NULL;
    Usuario usuarioExiste;
    usuarioExiste = buscarUsuarioPorNomeUsuario(usuario.usuUsuario);
    int tipoArquivo = opcaoArmazenamentoUsuario();
    switch(tipoArquivo){
        case 1:
            // Verificando se já não existe um usuário cadastrado com o usuUsuario informado
            if (usuarioExiste.usuCodigo == -1) {
                file = fopen("persist/usuario.bin", "a+b");
                // Verificando se o arquivo foi aberto de forma correta
                if (file != NULL) {
                    // Buscando o código do próximo registro no arquivo binário
                    usuario.usuCodigo = sequenceArquivoBinario("persist/seq_usuario.bin");
                    if(usuario.usuCodigo != 0){
                        fwrite(&usuario, sizeof (Usuario), 1, file);
                        fclose(file);
                        return SUCCESS;
                    }
                }
                mostrarMensagemErroArquivo();
                return ERROR;
            }
            mostrarMensagemRegistroExiste();
            return ERROR;
        break;
        case 2:
            // Verificando se já não existe um usuário cadastrado com o usuUsuario informado
            if (usuarioExiste.usuCodigo == -1) {
                file = fopen("persist/usuario.txt", "a+");
                // Verificando se o arquivo foi aberto de forma correta
                if(file != NULL){
                    // Buscando o código do próximo registro no arquivo TXT
                    usuario.usuCodigo = sequenceArquivoTXT("persist/seq_usuario.txt");
                    if(usuario.usuCodigo != 0){
                        fprintf(file, "%d;", usuario.usuCodigo);
                        fprintf(file, "%s;", usuario.usuNome);
                        fprintf(file, "%s;", usuario.usuUsuario);
                        fprintf(file, "%s;", usuario.usuSenha);
                        fprintf(file, "%d;\n", usuario.usuPermissao);
                        fclose(file);
                        return SUCCESS;
                    }
                }
                mostrarMensagemErroArquivo();
                return ERROR;
            }
            mostrarMensagemRegistroExiste();
            return ERROR;
            break;
        default:
            mostrarMensagemErroTipoArquivo();
            return ERROR;
            break;
    }
}

/**
* Obtém a quantidade de registros gravados nos arquivos de usuário
* @param
* @return int: quantidade de registros 
*/
int qtdeRegistrosUsuario() {
    int tipoArquivo = opcaoArmazenamentoUsuario();
    FILE *file = NULL;
    int size = 0;
    if (tipoArquivo == 1) {
        file = fopen("persist/usuario.bin", "rb+");
        if (file != NULL) {
            Usuario usuario;
            while (fread(&usuario, sizeof (Usuario), 1, file)) {
                size++;
            }
            fclose(file);
        }

    } else if (tipoArquivo == 2) {
        file = fopen("persist/usuario.txt", "r+");

        if (file != NULL) {
            char usuarioString[500];
            while (fgets(usuarioString, 500, file) != NULL) {
                size++;
            }
            fclose(file);
        }
    }

    return size;
}

/**
* Obtém todos os usuários armazenados no sistema
* @param 
* @return Usuario
*/
Usuario *listarUsuarios () {
    int tipoArquivo = opcaoArmazenamentoUsuario();
    FILE *file = NULL;
    Usuario *usuarios;

    int size = qtdeRegistrosUsuario(tipoArquivo);

    //    List to be returned
    usuarios = (Usuario *) malloc(sizeof (Usuario) * size);

    Usuario tempUsuario;

    switch (tipoArquivo) {
        case 1:
            file = fopen("persist/usuario.bin", "rb+");

            if (file != NULL) {
                int index = 0;
                while (fread(&tempUsuario, sizeof (Usuario), 1, file)) {
                    usuarios[index] = tempUsuario;
                    index++;
                }

                fclose(file);

                return usuarios;
            }
            break;
        case 2:
            file = fopen("persist/usuario.txt", "r+");

            if (file != NULL) {
                int index = 0;
                char usuarioString[500];
                while (fgets(usuarioString, 500, file) != NULL) {

                    char *token = strtok(usuarioString, ";");
                    for (int i = 0; token != NULL; i++) {
                        switch (i) {
                            case 0:
                                tempUsuario.usuCodigo = atoi(token);
                                break;
                            case 1:
                                strcpy(tempUsuario.usuNome, token);
                                break;
                            case 2:
                                strcpy(tempUsuario.usuUsuario, token);
                                break;
                            case 3:
                                strcpy(tempUsuario.usuSenha, token);
                                break;
                            case 4:
                                tempUsuario.usuPermissao = atoi(token);
                                break;
                            default:
                                break;
                        }

                        token = strtok(NULL, ";");
                    }

                    usuarios[index] = tempUsuario;
                    index++;
                }

                fclose(file);

                return usuarios;
            }

            break;
        default:
            break;
    }

    if (file == NULL) {
        mostrarMensagemErroArquivo();
    }

    return NULL;
}

/**
* Apaga o usuário de acordo com o usuUsuario
* @param char *usuUsuario: ponteiro usuUsuario
* @return 1- se deletou com sucesso 0: se houve algum problema
*/
int deletarUsuario (char *usuUsuario) {
    int tipoArquivo = opcaoArmazenamentoUsuario(); 
    FILE *file = NULL;
    FILE *newFile = NULL;
    int qtdeInicial = qtdeRegistrosUsuario(tipoArquivo);
    int qtdeFinal;
    Usuario usuario = buscarUsuarioPorNomeUsuario(usuUsuario);
    // Verificando se o usuário está na base de dados
    if (usuario.usuCodigo != -1) {
        switch (tipoArquivo) {
            case 1:
                file = fopen("persist/usuario.bin", "rb+");
                newFile = fopen("persist/tempUsuario.bin", "wb+");
                rewind(file);

                if (file != NULL) {
                    Usuario tempUsuario;
                    // Percorrendo cada linha procurando o usuário
                    while (fread(&tempUsuario, sizeof (Usuario), 1, file)) {
                        if (!(tempUsuario.usuCodigo == usuario.usuCodigo)) {
                            fwrite(&tempUsuario, sizeof (Usuario), 1, newFile);
                        }
                    }
                }
                // Fechando os arquivos
                fclose(file);
                fclose(newFile);
                // Apagando o arquivo original          
                remove("persist/usuario.bin");
                 // Renomeando o arquivo
                rename("persist/tempUsuario.bin", "persist/usuario.bin");
                break;
            case 2:
                file = fopen("persist/usuario.txt", "r+");
                newFile = fopen("persist/tempUsuario.txt", "w+");
                if (file != NULL && newFile != NULL) {
                    char usuarioString[500];
                    // Percorrendo cada linha procurando o usuário
                    while ((fgets(usuarioString, 500, file)) != NULL) {
                        char aux[500];
                        strcpy(aux, usuarioString);
                        char *token = strtok(usuarioString, ";");
                        Usuario tempUsuario;
                        for (int i = 0; token != NULL; i++) {
                            switch (i) {
                                case 0:
                                    tempUsuario.usuCodigo = atoi(token);
                                    break;
                                case 1:
                                    strcpy(tempUsuario.usuNome, token);
                                    break;
                                case 2:
                                    strcpy(tempUsuario.usuUsuario, token);
                                    break;
                                case 3:
                                    strcpy(tempUsuario.usuSenha, token);
                                    break;
                                case 4:
                                    tempUsuario.usuPermissao = atoi(token);
                                    break;
                                default:
                                    break;
                            }

                            token = strtok(NULL, ";");
                        }

                        if (tempUsuario.usuCodigo == usuario.usuCodigo) {
                            //  Se os dados forem excluídos, não copie para o novo arquivo
                            continue;
                        } else {
                            fputs(aux, newFile);
                        }
                    }
                }
                // Fechando os arquivos
                fclose(file);
                fclose(newFile);
                // Apagando o arquivo original             
                remove("persist/usuario.txt");
                // Renomeando o arquivo
                rename("persist/tempUsuario.txt", "persist/usuario.txt");
                break;
        }
        usuUsuario = NULL;
        qtdeFinal = qtdeRegistrosUsuario(tipoArquivo);
        // verificando se realmente ocorreu a exclusão
        if(qtdeInicial > qtdeFinal){
            return SUCCESS;
        }
        return ERROR;
    }
    printf("\n Usuário não encontrado! \n");
    return ERROR;
}

/**
* Atualiza os dados do usuário
* @param Usuario usuario: usuario a ser atualizado
* @return usuário atualizado
*/
Usuario atualizarUsuario (Usuario usuario, char *usuUsuario) {
    int tipoArquivo = opcaoArmazenamentoUsuario();
    FILE *file = NULL;
    FILE *newFile = NULL;
    Usuario usuarioExistente;
    usuarioExistente = buscarUsuarioPorNomeUsuario(usuUsuario);
    if (usuarioExistente.usuCodigo == usuario.usuCodigo) {
        switch (tipoArquivo) {
            case 1:
                file = fopen("persist/usuario.bin", "rb+");
                if (file != NULL) {
                    Usuario tempUsuario;
                    while (fread(&tempUsuario, sizeof (Usuario), 1, file)) {
                        if (tempUsuario.usuCodigo == usuario.usuCodigo) {
                            fseek(file, -(long) sizeof (Usuario), 1);
                            fwrite(&usuario, sizeof (Usuario), 1, file);
                        }
                    }
                }
                // Fechando o arquivo
                fclose(file);
                break;
            case 2:
                file = fopen("persist/usuario.txt", "r+");
                newFile = fopen("persist/tempUsuario.txt", "w+");
                if (file != NULL && newFile != NULL) {
                    char usuarioString[500];
                    while ((fgets(usuarioString, 500, file)) != NULL) {
                        char aux[500];
                        strcpy(aux, usuarioString);
                        char *token = strtok(usuarioString, ";");
                        Usuario tempUsuario;
                        for (int i = 0; token != NULL; i++) {
                            switch (i) {
                                case 0:
                                    tempUsuario.usuCodigo = atoi(token);
                                    break;
                                case 1:
                                    strcpy(tempUsuario.usuNome, token);
                                    break;
                                case 2:
                                    strcpy(tempUsuario.usuUsuario, token);
                                    break;
                                case 3:
                                    strcpy(tempUsuario.usuSenha, token);
                                    break;
                                case 4:
                                    tempUsuario.usuPermissao = atoi(token);
                                    break;
                                default:
                                    break;
                            }
                            token = strtok(NULL, ";");
                        }
                        if (tempUsuario.usuCodigo == usuario.usuCodigo) {
                            fprintf(newFile, "%d;", usuario.usuCodigo);
                            fprintf(newFile, "%s;", usuario.usuNome);
                            fprintf(newFile, "%s;", usuario.usuUsuario);
                            fprintf(newFile, "%s;", usuario.usuSenha);
                            fprintf(newFile, "%d;\n", usuario.usuPermissao);
                        } else {
                            fputs(aux, newFile);
                        }
                    }
                }

                // Fechando os arquivos
                fclose(file);
                fclose(newFile);
                //Apagando o arquivo original
                remove("persist/usuario.txt");
                //Renomeando o arquivo
                rename("persist/tempUsuario.txt", "persist/usuario.txt");
                break;
        }
        return usuarioExistente;
    }
    printf("\n Usuário não encontrado \n");
    return usuario;
}