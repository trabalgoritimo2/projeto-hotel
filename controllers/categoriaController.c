/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../models/categoriaModel.h"
#include "../models/configuracaoModel.h"
#include "../models/funcoesModel.h"
#include "../structs/moduloCadastroStructs.h"

//TODO: colocar mensagem de categoria cadastrada com sucesso

/**
* Cria uma nova categoria e manda para criarCategoriaPorValor salvar
* @param
* @return void
*/
void criarCategoria() {
    char descricao[100]; // uma descrição básica do que é essa categoria
    float valorDaDiaria = 0;
    int quantidadeDePessoas = 0;
    int codigo = 1; // a geração de codigos começa com 1 e cresce quanto mais categorias existirem
    FILE* file = NULL;

    printf("================================\n");
    printf("|   CADASTRO DE CATEGORIA      |\n");
    printf("================================\n");
    printf("Digite a descrição da categoria de acomodação:\n");
    fgets(descricao, 99, stdin);
    descricao[strcspn(descricao, "\n")] = 0;
    setbuf(stdin, NULL);
    printf("Digite o valor da diária:\n");
    scanf("%f%*c", &valorDaDiaria);
    printf("Digite o máximo de pessoas que a acomodação comporta:\n");
    scanf("%d%*c", &quantidadeDePessoas);

    criarCategoriaPorValor(descricao, valorDaDiaria, quantidadeDePessoas);
}

/**
* Cria uma categoria com osvalores passados
* @param char *descricao: descricao da categoria
* @param float valorDaDiaria: valor de 1 dia
* @param int quantidadeDePessoas: maximo de pessoas que comporta
* @return void
*/
void criarCategoriaPorValor(char *descricao, float valorDaDiaria, int quantidadeDePessoas) {
    int categoriaCount = contarCategorias();
    Categoria* lista = carregarCategorias(categoriaCount);

    categoriaCount++;
    lista = realloc(lista, sizeof(Categoria) * categoriaCount);

    if (descricao[99] != '\0'){
        descricao[99] = '\0';
    }
    strcpy(lista[categoriaCount-1].catDescricao, descricao);
    if (lista[categoriaCount-1].catDescricao[99] != '\0'){
        lista[categoriaCount-1].catDescricao[99] = '\0';
    }
    lista[categoriaCount-1].catValorDiaria = valorDaDiaria;
    lista[categoriaCount-1].catQuantidadePessoas = quantidadeDePessoas;
    if (categoriaCount > 1){
        lista[categoriaCount-1].catCodigo = lista[categoriaCount-2].catCodigo+1;
    }
    else {
        lista[categoriaCount-1].catCodigo = 1;
    }

    salvarCategorias(lista, categoriaCount);
    free(lista);
}

/**
* Menu que pede um filtro para filtrar as categorias
* @param
* @return void
*/
void pesquisarCategorias() {
    int escolhaDeFiltro;
    char parametro[100];
    printf("================================\n");
    printf("|    PESQUISA DE CATEGORIA     |\n");
    printf("================================\n");
    printf("Digite a opção de filtragem da categoria:\n");
    printf("Opções possiveis:\n");
    printf("%d - Por descrição\n", catFiltDescricao);
    printf("%d - Por valor de diaria igual ou menor\n", catFiltValor);
    printf("%d - Por capacidade de pessoas igual ou maior\n", catFiltQtPessoas);
    scanf("%d%*c", &escolhaDeFiltro);
    
    printf("Digite o parametro de pesquisa: ");
    fgets(parametro, 99, stdin);
    parametro[strcspn(parametro, "\n")] = 0;
    setbuf(stdin, NULL);

    filtrarCategorias(escolhaDeFiltro, parametro);
}

/**
* Mostra as categorias filtradas com o parametro e filtro passados
* @param int filtro: filtro para pesquisa (deve ser um enum de catFiltros)
* @param char* param: parametro de filtragem
* @return void
*/
void filtrarCategorias(int filtro, char* param) {
    int categoriaCount = contarCategorias();
    Categoria* todasCategorias = carregarCategorias(categoriaCount);
    Categoria* categoriasFiltradas = NULL;
    int countFiltradas = 0;

    if (filtro == catFiltDescricao){
        for (int i=0; i<categoriaCount; i++){
            if (strncmp(todasCategorias[i].catDescricao, param, strlen(param)) == 0){
                countFiltradas++;
                categoriasFiltradas = realloc(categoriasFiltradas, sizeof(Categoria) * countFiltradas);
                copiarCategoria(&categoriasFiltradas[countFiltradas-1], &todasCategorias[i]);
            }
        }
    }
    else if (filtro == catFiltValor){
        float valor = atof(param);
        for (int i=0; i<categoriaCount; i++){
            if (todasCategorias[i].catValorDiaria <= valor){
                countFiltradas++;
                categoriasFiltradas = realloc(categoriasFiltradas, sizeof(Categoria) * countFiltradas);
                copiarCategoria(&categoriasFiltradas[countFiltradas-1], &todasCategorias[i]);
            }
        }
    }
    else if (filtro == catFiltQtPessoas){
        int qtPessoas = atoi(param);
        for (int i=0; i<categoriaCount; i++){
            if (todasCategorias[i].catQuantidadePessoas >= qtPessoas){
                countFiltradas++;
                categoriasFiltradas = realloc(categoriasFiltradas, sizeof(Categoria) * countFiltradas);
                copiarCategoria(&categoriasFiltradas[countFiltradas-1], &todasCategorias[i]);
            }
        }
    }

    printf("%d Categorias encontradas dentro dos parâmetros:\n", countFiltradas);
    escreverCategoria(categoriasFiltradas, countFiltradas);
    free(todasCategorias);
    free(categoriasFiltradas);
}

/**
* Mostra na tela todas as categorias registradas
* @param
* @return void
*/
void mostrarCategorias(){
    printf("\n ================================\n");
    printf(" |      LISTAR CATEGORIAS       |\n");
    printf(" ================================\n");
    int categoriaCount = contarCategorias();
    printf("%d Categorias registradas\n \n", categoriaCount);
    Categoria* listaDeCategoria = carregarCategorias(categoriaCount);
    escreverCategoria(listaDeCategoria, categoriaCount);
    free(listaDeCategoria);
}

/**
* Mostra no terminal a categoria que foi passada
* @param Categoria *cate: vetor com as categorias para serem mostradas
* @param int count: quantidade de categorias no vetor passado
* @return void
*/
void escreverCategoria(Categoria *cate, int count){
    for(int i=0; i<count; i++){
        printf(" Código: %d\n", cate[i].catCodigo);
        printf(" Descrição: %s\n", cate[i].catDescricao);
        printf(" Valor da diária: R$%.2f\n", cate[i].catValorDiaria);
        printf(" Quantidade máxima de pessoas: %d\n\n", cate[i].catQuantidadePessoas);
    }
}

/**
* Menu para editar uma categoria
* @param 
* @return void
*/
int editarCategoria() {
    int codigo=0;
    int propriedade;
    char edicao[100];
    printf("================================\n");
    printf("|     EDIÇÃO DE CATEGORIA      |\n");
    printf("================================\n");
    printf("Veja novamente as categorias registradas:\n");
    mostrarCategorias();
    printf("Digite o código da categoria que será editada: ");
    scanf("%d", &codigo);
    setbuf(stdin, NULL);
    printf("Digite o código da categoria a ser digitada:\n");
    printf("  Descrição - %d\n  Valor da diária - %d\n  Quantidade de pessoas - %d\n", catPropDescricao, catPropValor, catPropQtPessoas);
    scanf("%d", &propriedade);
    setbuf(stdin, NULL);
    printf("Digite o conteúdo novo: ");
    fgets(edicao, 99, stdin);
    edicao[strcspn(edicao, "\n")] = 0;
    setbuf(stdin, NULL);
    editarCategoriaPorValor(codigo, propriedade, edicao);
}

/**
* Aplica a edição passada na propriedade indicada no parametro, na categoria de codigo passado
* @param int codigo: codido da categoria
* @param int propriedade: a propriedade que se edita (deve ser um de catPropriedades)
* @param char *edicao: a nova informação da categoria (numeros devem ser passados em forma de string)
* @return void
*/
int editarCategoriaPorValor(int codigo, int propriedade, char *edicao) {
    int categoriaCount = contarCategorias();
    Categoria *listaDeCategorias = carregarCategorias(categoriaCount);
    printf("Editando propriedade %d\n", propriedade);
    for (int i=0; i<categoriaCount; i++){
        if (listaDeCategorias[i].catCodigo == codigo){
            // se encontrar o código pedido, edita o arquivo
            if (propriedade == catPropCodigo){
                printf("O código não pode ser alterado\n");
            }
            else if (propriedade == catPropDescricao){
                strcpy(listaDeCategorias[i].catDescricao, edicao);
            }
            else if (propriedade == catPropValor){
                listaDeCategorias[i].catValorDiaria = atof(edicao);
            }
            else if (propriedade == catPropQtPessoas){
                listaDeCategorias[i].catQuantidadePessoas = atoi(edicao);
            }

            break;
        }
    }
    salvarCategorias(listaDeCategorias, categoriaCount);
    free(listaDeCategorias);
}

/**
* Carrega as categorias do banco de dados
* @param int count: quantidade de categorias presentes (use contarCategorias() para contar)
* @return Pointer de categoria com as informações carregadas
*/
Categoria* carregarCategorias(int count) {
    int filetype = opcaoArmazenamentoUsuario();
    FILE* file = NULL;
    Categoria *listaDeCategoria; // vetor que armazena as categorias atuais
    listaDeCategoria = calloc(count, sizeof(Categoria));

    if (filetype == 1){
        // tenta abrir o arquivo de categorias
        file = fopen("persist/categoria.bin", "rb");
        if (file == NULL) {
            // se não existir o arquivo, paramos o funcionamento
            mostrarMensagemErroArquivo();
            exit(1); // 1 é o codigo de erro de falha na abertura do arquivo
        }

        fread(listaDeCategoria, sizeof(Categoria), count, file);
        fclose(file);
    } else if (filetype == 2){
        // tenta abrir o arquivo de categorias
        file = fopen("persist/categoria.txt", "r");
        if (file == NULL) {
            // se não existir o arquivo, paramos o funcionamento
            mostrarMensagemErroArquivo();
            exit(1); // 1 é o codigo de erro de falha na abertura do arquivo
        }

        char line[300];
        // cria o vetor de categorias atual
        for (int i=0; i<count; i++){
            // extrai a linha de categoria
            fgets(line, 300, file);
            // copia o codigo
            listaDeCategoria[i].catCodigo = atoi(strtok(line, ";\n"));
            // copia a descrição
            char *descricao = strtok(NULL, ";\n");
            strcpy(listaDeCategoria[i].catDescricao, descricao);
            // copia o valor da diaria
            listaDeCategoria[i].catValorDiaria = atof(strtok(NULL, ";\n"));
            //copia a quantidade de pessoas
            listaDeCategoria[i].catQuantidadePessoas = atoi(strtok(NULL, ";\n"));
        }
        int status = fclose(file);
    }

    return listaDeCategoria;
}

/**
* Salva as categorias no arquivo
* @param Categoria *lista: pointer contendo as categorias a serem salvas
* @param int count: quantidade de categorias presentes no vetor
* @return void
*/
void salvarCategorias(Categoria *lista, int count) {
    int filetype = opcaoArmazenamentoUsuario();
    FILE *file = NULL;
    if (filetype == 1) {
        file = fopen("persist/categoria.bin", "wb");
        if (file == NULL) {
            mostrarMensagemErroArquivo();
            exit(1);
        }
        fwrite(lista, sizeof(Categoria), count, file);
        fclose(file);
    } else if (filetype == 2) {
        file = fopen("persist/categoria.txt", "w");
        if (file == NULL){
            mostrarMensagemErroArquivo();
            exit(1);
        }
        for (int i=0; i<count; i++){
            // salva as informações tokenizadas com ;
            fprintf(file, "%d;", lista[i].catCodigo);
            fprintf(file, "%s;", lista[i].catDescricao);
            fprintf(file, "%f;", lista[i].catValorDiaria);
            fprintf(file, "%d;\n", lista[i].catQuantidadePessoas);
        }
        fclose(file);
    }
}

/**
* Conta a quantidade de categorias regostradas no banco de dados
* @param 
* @return numero de categorias registradas
*/
int contarCategorias() {
    int filetype = opcaoArmazenamentoUsuario();
    int catCount = 0;
    FILE *file;

    if (filetype == 1){
        file = fopen("persist/categoria.bin", "rb");
        if (file == NULL) {
            mostrarMensagemErroArquivo();
            file = fopen("persist/categoria.bin", "w+b");
        }

        fseek(file, 0, SEEK_END);
        catCount = ftell(file) / sizeof(Categoria);
        fclose(file);
    } else if (filetype == 2){
        file = fopen("persist/categoria.txt", "r");
        if (file == NULL) {
            // se não existir o arquivo, paramos o funcionamento
            mostrarMensagemErroArquivo();
            file = fopen("persist/categoria.txt", "w+");
        }

        char line[300];
        // em cada linha aumenta o contador de linhas
        while (fgets(line, 300, file) != NULL) {
            catCount++;
        }

        fclose(file);
    }

    return catCount;
}

/**
* Procura confirmar se o codigo de parametro coincide com alguma categoria registrada
* @param int codigo: codigo da categoria a procurar
* @return 1- se existir, 0- se não for encontrada
*/
int categoriaExiste(int codigo){
    printf("Verificando existencia da categoria\n");
    int categoriaCount = contarCategorias();
    Categoria* listaDeCategorias = carregarCategorias(categoriaCount);

    for(int i=0; i<categoriaCount; i++){
        if (listaDeCategorias[i].catCodigo == codigo){
            free(listaDeCategorias);
            return 1; // a categoria foi encontrada - retorna TRUE
        }
    }
    printf("Categoria não foi encontrada! \n");
    return 0; // se nenhuma for encontrada, retorna 0 (FALSE)
}

/**
* Retorna uma categoria baseada no código. A categoria deve existir (use categoriaExiste() para conferir)
* @param int codigo: codigo da categoria buscada
* @return Cópia da categoria buscada
*/
Categoria buscarCategoria(int codigo) {
    int count = contarCategorias();
    Categoria* lista = carregarCategorias(count);

    for (int i=0; i<count; i++){
        if (lista[i].catCodigo == codigo){
            Categoria catBuscada;
            copiarCategoria(&catBuscada, &lista[i]);
            free(lista);
            return catBuscada;
        }
    }
}

/**
* Copia o conteúdo do segundo argumento para o primeiro argumento
* @param Categoria* pra: pointer para onde os valores irão ser copiados
* @param Categoria* de: pointer de onde os valores irão ser copiados
* @return void
*/
void copiarCategoria(Categoria* pra, Categoria* de) {
    pra->catCodigo = de->catCodigo;
    strcpy(pra->catDescricao, de->catDescricao);
    pra->catQuantidadePessoas = de->catQuantidadePessoas;
    pra->catValorDiaria = de->catValorDiaria;
}

/**
* Recebe uma string tokenizada, uma linha do arquivo categoria.txt, e transforma em um objeto categoria
* @param char catString[]: string tokenizada
* @param Categoria *cate: pointer para onde as informações serão copiadas
* @return 1- se deletou com sucesso 0: se houve algum problema
*/
void strPraCat(char catString[], Categoria *cate) {
    cate->catCodigo = atoi(strtok(catString, ";\n"));
    char *descricao = strtok(NULL, ";\n");
    strcpy(cate->catDescricao, descricao);
    cate->catValorDiaria = atof(strtok(NULL, ";\n"));
    cate->catQuantidadePessoas = atoi(strtok(NULL, ";\n"));
}

/**
* Menu para exclusão de uma categoria
* @param 
* @return void
*/
void excluirCategoria(){
    int codigoEscolhido=0;
    char escolha;
    printf("================================\n");
    printf("|      DELETAR CATEGORIA       |\n");
    printf("================================\n");
    printf("Mostrando a lista atual de categorias\n");
    mostrarCategorias();
    printf("Digite o número do código da categoria que se deseja excluir: ");
    scanf("%d", &codigoEscolhido);
    setbuf(stdin, NULL);
    printf("O código digitado foi %d. Tem certeza que deseja excluir essa categoria? (s ou n) ", codigoEscolhido);
    scanf("%c", &escolha);
    if (escolha == 's' || escolha == 'S'){
        excluirCategoriaPorCodigo(codigoEscolhido);
        return;
    }
    printf("Cancelando a operação\n");
    return;
}

/**
* Excluir a categoria com o código especificado]
* @param codigo da categoria
* @return 1- ocorrer algum erro
*/
int excluirCategoriaPorCodigo(int codigo) {
    int categoriaCount = contarCategorias(); // quantidade de linhas ou categorias registradas
    int posicaoDoCodigo = -1; // a linha (ou posição) que se encontra a categoria com o codigo pedido
    Categoria *listaDeCategoria = carregarCategorias(categoriaCount); // vetor que armazena as categorias atuais
    Categoria *novaLista = calloc(categoriaCount-1, sizeof(Categoria)); // vetor que armazena as categorias após a exclusão

    for(int i=0; i<categoriaCount; i++){
        if(listaDeCategoria[i].catCodigo == codigo){
            posicaoDoCodigo = i;
            break;
        }
    }

    if (posicaoDoCodigo == -1){
        free(listaDeCategoria);
        free(novaLista);
        return 1; // se por algum acaso o código não for encontrado, termina a função
    }

    // copia todos os elementos anteriores a posição excluida
    for (int i=0; i < posicaoDoCodigo; i++){
        // copiarCa(&novaLista[i], &listaDeCategoria[i]);
        // novaLista[i] = listaDeCategoria[i];
        copiarCategoria(&novaLista[i], &listaDeCategoria[i]);
    }
    // copia todos os elementos DEPOIS da posição excluida
    for (int i=posicaoDoCodigo+1; i<categoriaCount; i++){
        copiarCategoria(&novaLista[i-1], &listaDeCategoria[i]);
    }

    salvarCategorias(novaLista, categoriaCount-1);
    free(listaDeCategoria);
    free(novaLista);
}