/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../models/categoriaModel.h"
#include "../models/acomodacaoModel.h"
#include "../models/configuracaoModel.h"
#include "../models/funcoesModel.h"
#include "../models/reservaModel.h"
#include "../structs/moduloCadastroStructs.h"
#include "ctype.h"

/**
* Menu para criação de uma nova acomodação
* @param
* @return void
*/
int cadastrarAcomodacao() {
    int acomodacaoCount = contarAcomodacoes();
    Acomodacao *listaDeAcomodacao = carregarAcomodacao(acomodacaoCount);
    char descricao[100];
    char caracteristicas[100];
    int codigoCategoria;
    int codigo = 1;
    int categoriaEncontrada = 0;

    printf("================================\n");
    printf("|   CADASTRO DE ACOMODAÇÃO     |\n");
    printf("================================\n");
    printf("As categorias gravadas são as seguintes:\n");
    mostrarCategorias();
    do {
        printf("Digite o código da categoria da nova acomodação: ");
        scanf("%d", &codigoCategoria);
        setbuf(stdin, NULL);
        if (categoriaExiste(codigoCategoria)){
            categoriaEncontrada = 1;
        }
        else {
            printf("Essa categoria não pôde ser encontrada\n");
        }
    } while (!categoriaEncontrada);

    printf("Digite a descrição dessa acomodação: ");
    fgets(descricao, 99, stdin);
    descricao[strcspn(descricao, "\n")] = 0;
    setbuf(stdin, NULL);

    printf("Digite quais as caracteristicas dessa acomodação (evite usar ponto-virugula): ");
    fgets(caracteristicas, 99, stdin);
    caracteristicas[strcspn(caracteristicas, "\n")] = 0;
    setbuf(stdin, NULL);

    acomodacaoCount++;
    listaDeAcomodacao = realloc(listaDeAcomodacao, sizeof(Acomodacao) * acomodacaoCount);

    strcpy(listaDeAcomodacao[acomodacaoCount-1].acoCaracteristicas, caracteristicas);
    strcpy(listaDeAcomodacao[acomodacaoCount-1].acoDescricao, descricao);
    listaDeAcomodacao[acomodacaoCount-1].acoCategoria = buscarCategoria(codigoCategoria);
    if (acomodacaoCount > 1){
        listaDeAcomodacao[acomodacaoCount-1].acoCodigo = listaDeAcomodacao[acomodacaoCount-2].acoCodigo+1;
    }
    else {
        listaDeAcomodacao[acomodacaoCount-1 ].acoCodigo = 1;
    }
    salvarAcomodacao(listaDeAcomodacao, acomodacaoCount);
    free(listaDeAcomodacao);
}

/**
* Função para printar um vetor de acomodações. Para mostrar 1 acomodação, use escreverAcomodacao(&acom, 1);
* @param Acomodacao *acom: vetor de categorias
* @param int counter: quantidade de acomodações para printar
* @return void
*/
void escreverAcomodacao(Acomodacao *acom, int counter){
    for (int i=0; i<counter; i++){
        printf(" Codigo: %d\n", acom[i].acoCodigo);
        printf(" Descrição: %s\n", acom[i].acoDescricao);
        printf(" Caracteristicas: %s\n", acom[i].acoCaracteristicas);
        printf(" Categoria:\n");
        printf("--------------------------- \n");
        printf(" Codigo: %d\n", acom[i].acoCategoria.catCodigo);
        printf(" Descrição: %s\n", acom[i].acoCategoria.catDescricao);
        printf(" Valor da diaria: %f\n", acom[i].acoCategoria.catValorDiaria);
        printf(" Quantidade de pessoas: %d\n\n", acom[i].acoCategoria.catQuantidadePessoas);
    }
}

/**
* Mostra na tela todas as acomodações registradas
* @param
* @return void
*/
void mostrarAcomodacao(){
    printf("================================\n");
    printf("|     LISTAR ACOMODAÇÕES       |\n");
    printf("================================\n");

    int acomodacaoCount = contarAcomodacoes();
    printf("%d acomodações registradas\n\n", acomodacaoCount);
    Acomodacao *listaDeAcomodacao = carregarAcomodacao(acomodacaoCount);

    escreverAcomodacao(listaDeAcomodacao, acomodacaoCount);
    free(listaDeAcomodacao);
}

/**
* Menu de acesso para editar uma acomodação
* @param
* @return void
*/
void editarAcomodacao(){
    printf("================================\n");
    printf("|      EDITAR ACOMODAÇÃO       |\n");
    printf("================================\n");

    int codigo;
    int propriedade;
    char edicao[100];

    printf("Digite o cÓdigo da acomodação a ser editada: ");
    scanf("%d%*c", &codigo);

    if(!acomodacaoExiste(codigo)){
        // se o código não for encontrado, encerra a função
        printf("O código digitado não foi encontrado\n");
        return;
    }

    printf("As propriedades a serem editadas são as seguintes:\n");
    printf("%d - Descrição\n%d - Características\n%d - Categoria\n", acoPropDescricao, acoPropCaracteristicas, acoPropCategoria);
    scanf("%d%*c", &propriedade);

    printf("Digite a nova informação: ");
    fgets(edicao, 99, stdin);
    edicao[strcspn(edicao, "\n")] = 0;
    setbuf(stdin, NULL);

    editarAcomodacaoPorPropriedade(codigo, propriedade, edicao);
}

/**
* Edita a acomodação referida pelo codigo
* @param int codigo: codigo da acomodação a ser editada
* @param int propriedade: a propriedade que será editada (definido por enum acoPropriedades)
* @param char edição[]: a nova informação da propriedade (valores numericos são passados como string)
* @return void
*/
int editarAcomodacaoPorPropriedade(int codigo, int propriedade, char edicao[]){
    int counter = contarAcomodacoes();
    Acomodacao* lista = carregarAcomodacao(counter);

    for (int i=0; i<counter; i++){
        if (lista[i].acoCodigo == codigo){
            switch (propriedade)
            {
            case acoPropDescricao:
                strcpy(lista[i].acoDescricao, edicao);
                break;
            case acoPropCaracteristicas:
                strcpy(lista[i].acoCaracteristicas, edicao);
                break;
            case acoPropCategoria:
                lista[i].acoCategoria = buscarCategoria(atoi(edicao));
                break;
            default:
                break;
            }
        }
    }

    salvarAcomodacao(lista, counter);
    free(lista);
}

/**
* Menu para excluir uma acomodação
* @param
* @return void
*/
void excluirAcomodacao(){
    printf("================================\n");
    printf("|     EXCLUIR ACOMODACAO       |\n");
    printf("================================\n");

    int codigoEscolha;
    char escolha;

    printf("Digite o código da acomodação a ser excluida: ");
    scanf("%d", &codigoEscolha);
    setbuf(stdin, NULL);

    if (acomodacaoExiste(codigoEscolha)){
        printf("Tem certeza que deseja excluir o registro dessa acomodação? (s ou n) ");
        scanf("%c", &escolha);
        setbuf(stdin, NULL);

        if (escolha == 's' || escolha == 'S'){
            excluirAcomodacaoPorCodigo(codigoEscolha);
        }
        return;
    }
    printf("A acomodação desejada não pôde ser encontrada\n");
    return;
}

/**
* Excluir a acomodação com o código passado
* @param int codigo: o codigo da acomodação que irá ser excluida
* @return void
*/
void excluirAcomodacaoPorCodigo(int codigo){
    int counter = contarAcomodacoes();
    int posicaoDoCodigo = -1; // -1 não é uma posição válida de vetor
    Acomodacao *lista = carregarAcomodacao(counter);
    Acomodacao *novaLista = calloc(counter-1, sizeof(Acomodacao));

    for(int i=0; i<counter; i++){
        if(lista[i].acoCodigo == codigo){
            posicaoDoCodigo = i;
            break;
        }
    }
    if (posicaoDoCodigo == -1){
        free(lista);
        free(novaLista);
        return; // se por algum acaso o código não for encontrado, termina a função
    }

    // copia todos os elementos anteriores a posição excluida
    for (int i=0; i < posicaoDoCodigo; i++){
        copiarAcomodacao(&novaLista[i], &lista[i]);
    }
    // copia todos os elementos DEPOIS da posição excluida
    for (int i=posicaoDoCodigo+1; i<counter; i++){
        copiarAcomodacao(&novaLista[i-1], &lista[i]);
    }

    salvarAcomodacao(novaLista, counter-1);
    free(lista);
    free(novaLista);
}

/**
* Menu para mostrar acomodações filtradas pelo parametro dado
* @param
* @return void
*/
void pesquisarAcomodacao() {
    ParamPesq escolhas;
    escolhas.flags = 0;
    char decisao;
    printf("================================\n");
    printf("|    PESQUISAR ACOMODACAO      |\n");
    printf("================================\n");

    // seta a flag de data para 0
    escolhas.flags &= ~(1 << 0);

    printf("Deseja pesquisar por codigo de categoria? (s ou n) ");
    scanf("%c%*c", &decisao);
    if (decisao == 's') {
        escolhas.flags |= ParCategCodigo;
        printf("Digite o codigo da categoria a ser buscada: ");
        scanf("%d%*c", &escolhas.categCodigo);
    }
    decisao = 0;
    printf("Deseja pesquisar por caracteristicas da acomodação? (s ou n) ");
    scanf("%c%*c", &decisao);
    if (decisao == 's') {
        escolhas.flags |= ParCaracteristicas;
        printf("Digite a caracteristica buscada: ");
        fgets(escolhas.caracteristicas, 99, stdin);
        escolhas.caracteristicas[strcspn(escolhas.caracteristicas, "\n")] = 0;
        setbuf(stdin, NULL);
    }
    decisao = 0;
    printf("Deseja pesquisar por quantidade minima de pessoas comportadas? (s ou n) ");
    scanf("%c%*c", &decisao);
    if (decisao == 's') {
        escolhas.flags |= ParQtPessoas;
        printf("Digite a quantidade de pessoas: ");
        scanf("%d%*c", &escolhas.qtPessoas);
    }
    decisao = 0;
    printf("Deseja pesquisar por valor máximo da diária? (s ou n) ");
    scanf("%c%*c", &decisao);
    if (decisao == 's' || decisao == 'S') {
        escolhas.flags |= ParValorMaximo;
        printf("Digite o valor de diária desejado: ");
        scanf("%f%*c", &escolhas.valorMaximo);
    }

    int filtroCounter = 0;
    Acomodacao* listaFiltrada = filtrarAcomodacaoPorFlags(&filtroCounter, escolhas);
    if (filtroCounter > 0) {
        printf("As acomodações selecionadas são:\n");
        escreverAcomodacao(listaFiltrada, filtroCounter);
    }
    else {
        printf("Nenhuma acomodação entra nos requisitos\n");
    }
    free(listaFiltrada);
}

/**
* Mostra na tela as acomodações filtradas
* @param int filtro: a propriedade usada para separar acomodações (definida no enum acoFiltros)
* @param char* param: a informação que define a filtragem
* @return void
*/
void filtrarAcomodacoes(int filtro, char* param) {
    int acomodacaoCount = contarAcomodacoes();
    Acomodacao* todasAcomodacoes = carregarAcomodacao(acomodacaoCount);
    int countFiltradas = 0;

    if (filtro == acoFiltDescricao){
        for (int i=0; i<acomodacaoCount; i++){
            if (strncmp(todasAcomodacoes[i].acoDescricao, param, strlen(param)) == 0){
                countFiltradas++;
                escreverAcomodacao(&todasAcomodacoes[i], 1);
            }
        }
    }
    else if (filtro == acoFiltCaracteristicas){
        for (int i=0; i<acomodacaoCount; i++){
            if (strstr(todasAcomodacoes[i].acoCaracteristicas, param)){
                countFiltradas++;
                escreverAcomodacao(&todasAcomodacoes[i], 1);
            }
        }
    }
    else if (filtro == acoFiltCategoria){
        int codCategoria = atoi(param);
        for (int i=0; i<acomodacaoCount; i++){
            if (todasAcomodacoes[i].acoCategoria.catCodigo == codCategoria){
                countFiltradas++;
                escreverAcomodacao(&todasAcomodacoes[i], 1);
            }
        }
    }

    if (countFiltradas == 0){
        printf("Não foram encontradas acomodacoes dentro dos parametros\n");
    }
    free(todasAcomodacoes);
}

/**
* Retorna a quantidade de acomodacoes registradas
* @param
* @return int: quantidade de acomodações registradas
*/
int contarAcomodacoes(){
    int filetype = opcaoArmazenamentoUsuario();
    FILE* file = NULL;
    int counter = 0;

    if (filetype == 1){
        file = fopen("persist/acomodacao.bin", "rb");
        if (file == NULL){
            mostrarMensagemErroArquivo();
            file = fopen("persist/acomodacao.bin", "w+b");
        }

        fseek(file, 0, SEEK_END);
        counter = ftell(file) / sizeof(Acomodacao);
        fclose(file);
    }
    else if (filetype == 2){
        file = fopen("persist/acomodacao.txt", "r");
        if (file == NULL){
            mostrarMensagemErroArquivo();
            file = fopen("persist/acomodacao.txt", "w+");
        }

        char line[300];
        while (fgets(line, 300, file)){
            counter++;
        }
        fclose(file);
    }

    return counter;
}

/**
* Copia o conteúdo do segundo argumento para o primeiro argumento
* @param Acomodacao* pra: acomodação para onde os dados serão copiados
* @param Acomodacao* de: acomodação de onde virão os dados
* @return void
*/
void copiarAcomodacao(Acomodacao* pra, Acomodacao* de) {
    strcpy(pra->acoCaracteristicas, de->acoCaracteristicas);
    strcpy(pra->acoDescricao, de->acoDescricao);
    pra->acoCodigo = de->acoCodigo;
    copiarCategoria(&pra->acoCategoria, &de->acoCategoria);
}

/**
* Busca por uma acomodação registrada com o código enviado
* @param int codigo: codigo da acomodação que se procura
* @return 1 se a acomodação for encontrada
*/
int acomodacaoExiste(int codigo) {
    int acomodacaoCount = contarAcomodacoes();
    Acomodacao* listaDeAcomodacao = carregarAcomodacao(acomodacaoCount);

    for (int i=0; i<acomodacaoCount; i++){
        if (listaDeAcomodacao[i].acoCodigo == codigo){
            free(listaDeAcomodacao);
            return 1; // é verdade que existe a acomodação requerida
        }
    }

    free(listaDeAcomodacao);
    return 0;
}

/**
* Salva o vetor de acomodações em disco
* @param Acomodação *lista: vetor de acomodações para serem salvas
* @param int counter: quantidade de acomodações no vetors
* @return void
*/
void salvarAcomodacao(Acomodacao *lista, int counter) {
    int filetype = opcaoArmazenamentoUsuario();
    FILE *file = NULL;

    if (filetype == 1){
        file = fopen("persist/acomodacao.bin", "wb");
        if (file == NULL){
            mostrarMensagemErroArquivo();
            exit(0);
        }
        fwrite(lista, sizeof(Acomodacao), counter, file);
        fclose(file);
    }
    else if (filetype == 2){
        file = fopen("persist/acomodacao.txt", "w");
        if (file == NULL){
            mostrarMensagemErroArquivo();
            exit(0);
        }

        for(int i=0; i<counter; i++){
            fprintf(file, "%d;", lista[i].acoCodigo);
            fprintf(file, "%s;", lista[i].acoDescricao);
            fprintf(file, "%s;", lista[i].acoCaracteristicas);
            fprintf(file, "("); // parte separada que delimita o salvamento dessa categoria no arquivo
            fprintf(file, "%d;", lista[i].acoCategoria.catCodigo);
            fprintf(file, "%s;", lista[i].acoCategoria.catDescricao);
            fprintf(file, "%f;", lista[i].acoCategoria.catValorDiaria);
            fprintf(file, "%d;", lista[i].acoCategoria.catQuantidadePessoas);
            fprintf(file, ")");
            fprintf(file, "\n");
        }
        fclose(file);
    }
}

/**
* Carrega as acomodações registradas para serem manipuladas
* @param int counter: a quantidade de acomodações para serem carregadas (use contarAcomodacoes() para contar todas)
* @return pointer de um vetor de acomodações
*/
Acomodacao* carregarAcomodacao(int counter) {
    int filetype = opcaoArmazenamentoUsuario();
    Acomodacao* lista;
    FILE *file;
    lista = calloc(counter, sizeof(Acomodacao));

    if (filetype == 1){
        file = fopen("persist/acomodacao.bin", "rb");
        if (file == NULL){
            mostrarMensagemErroArquivo();
            file = fopen("persist/acomodacao.bin", "w+b");
        }
        fread(lista, sizeof(Acomodacao), counter, file);
    }
    else if (filetype == 2){
        file = fopen("persist/acomodacao.txt", "r");
        if (file == NULL){
            mostrarMensagemErroArquivo();
            file = fopen("persist/acomodacao.txt", "w+");
        }

        char line[300];

        for (int i=0; i<counter; i++){
            fgets(line, 300, file);
            char* cod = strtok(line, ";\n");
            lista[i].acoCodigo = atoi(cod);

            char *descricao = strtok(NULL, ";\n");
            strcpy(lista[i].acoDescricao, descricao);

            char *caracteristicas = strtok(NULL, ";\n");
            strcpy(lista[i].acoCaracteristicas, caracteristicas);

            char* catLine = strtok(NULL, "()");
            strPraCat(catLine, &lista[i].acoCategoria);
        }
    }

    fclose(file);
    return lista;
}

/**
* Essa função verifica se alguma acomodação tem uma copia de categoria desatualizada com as categorias atuais
* @param
* @return void
*/
void mostrarAcomocacoesDesatualizadas() {
    int counter = contarAcomodacoes();
    Acomodacao* lista = carregarAcomodacao(counter);

    // TODO: resolver os problemas de desatualização
    for (int i=0; i<counter; i++){
        if (!categoriaExiste(lista[i].acoCodigo)){
            printf("INFORMAÇÂO IPORTANTE:\n");
            printf("a acomodação de ID = %d está desatualizada - Categoria não foi encontrada nos arquivos\n", lista[i].acoCodigo);
        }
        else {
            Categoria referencia = buscarCategoria(lista[i].acoCodigo);
            if (strcmp(lista[i].acoCategoria.catDescricao, referencia.catDescricao) != 0){
                printf("INFORMAÇÂO IMPORTANTE:\n");
                printf("a acomodação de ID = %d está desatualizada - descrição de categoria incompativel\n", lista[i].acoCodigo);
            }
            if (lista[i].acoCategoria.catValorDiaria != referencia.catValorDiaria){
                printf("INFORMAÇÂO IMPORTANTE:\n");
                printf("a acomodação de ID = %d está desatualizada - valor de diaria da categoria incompativel\n", lista[i].acoCodigo);
            }
            if (lista[i].acoCategoria.catQuantidadePessoas != referencia.catQuantidadePessoas){
                printf("INFORMAÇÂO IMPORTANTE:\n");
                printf("a acomodação de ID = %d está desatualizada - quantidade de pessoas comportadas da categoria incompativel\n", lista[i].acoCodigo);
            }
        }
    }
    free(lista);
}

/**
* Função para selecionar acomodações baseado em diversos parametros
* @param int* filtroCount: referencia de um inteiro para registrar a quantidade de acomodações que passam pelo filtro
* @param ParamPesq params: estrutura com as flags e dados da pesquisa
* @return Endereço do vetor de acomodaçẽos filtradas
*/
Acomodacao* filtrarAcomodacaoPorFlags(int* filtroCount, ParamPesq params) {
    int acoCount = contarAcomodacoes();
    Acomodacao* listaAcoms = carregarAcomodacao(acoCount);
    Acomodacao* filts = NULL;
    *filtroCount = 0;

    for (int i=0; i<acoCount; i++) {
        int valida = 0;

        if (params.flags & ParData) {
            // procura por todas as reservas cadastradas com ID de acomodação igual a acomodação atual
            // nesse caso, se o periodo das datas passadas coincide com o da reserva, ela é marcada como ocupada
            int acomodacaoOcupada = 0;
            int reservaCount = 0;
            Reserva* listaReserva = carregarReservas(&reservaCount);
            for (int j=0; j<reservaCount; j++) {
                if (listaReserva[j].resAcomodacaoId == listaAcoms[i].acoCodigo) {
                    // TODO: descobrir o que está errado
                    if (dataPeriodoCoincide(listaReserva[j].resDataInicio, listaReserva[j].resDataFim, params.dtInicial, params.dtFinal)) {
                        acomodacaoOcupada = 1;
                    }
                }
            }

            // se nenhuma reserva (para esta acomodação) estiver em data semelhante, então ela será válida
            if (!acomodacaoOcupada) {
                valida = 1;
            }
            else {
                valida = 0;
            }
        }

        if (params.flags & ParCategCodigo) {
            if (listaAcoms[i].acoCategoria.catCodigo == params.categCodigo)
                valida = 1;
            else
                valida = 0;
        }
        if (params.flags & ParQtPessoas){
            if (listaAcoms[i].acoCategoria.catQuantidadePessoas >= params.qtPessoas)
                valida = 1;
            else
                valida = 0;
        }
        if (params.flags & ParCaracteristicas){
            if (strContains(listaAcoms[i].acoCaracteristicas, params.caracteristicas) != 0){
                valida = 1;
            }
            else {
                valida = 0;
            }
        }
        if (params.flags & ParValorMaximo){
            if (listaAcoms[i].acoCategoria.catValorDiaria <= params.valorMaximo)
                valida = 1;
            else
                valida = 0;
        }

        if (valida) {
            (*filtroCount)++;
            filts = realloc(filts, (*filtroCount) * sizeof(Acomodacao));
            copiarAcomodacao(&filts[(*filtroCount) - 1], &listaAcoms[i]);
        }
    }

    if (listaAcoms != NULL) {
        free(listaAcoms);
    }
    return filts;
}

/**
* Retorna a string passada com todas as letras em maiusculo
* @param char* lower: string para ser convertida
* @return nova string em maiusculo
*/
char* strToUpper(char *lower) {
    char* upper = malloc(sizeof(char) * strlen(lower)+1);
    strcpy(upper, lower);
    for(int i=0; i<strlen(upper); i++) {
        upper[i] = toupper(upper[i]);
    }
    return upper;
}

/**
* Checa se a string sub está contida na string universal (mesmo se as letras forem diferentes entre maiusculas e minusculas)
* @param char* universal: string de comparação
* @param char* sub: string que se tentará encontrar
* @return 1 se encontrar e 0 se não encontrar
*/
int strContains(char* universal, char* sub) {
    int contains = 0;
    char* un = strToUpper(universal);
    char* sb = strToUpper(sub);
    printf("Universal: %s\n", un);
    printf("Substring: %s\n", sb);
    if (strstr(un, sb) != NULL)
        contains = 1;
    free(un);
    free(sb);
    return contains;
}

/**
* Busca acomodação por código
* @param int codigo: código desejado
* @return Acomodacao acomodação: encontrada de acordo com código desejado, se não encontrar, retorna acomodação com o código -1
*/
Acomodacao buscarAcomodacaoPorCodigo(int codigo) {
    int quantidade = contarAcomodacoes();
    Acomodacao *acomodacoes;
    Acomodacao acomodacao;
    acomodacoes = carregarAcomodacao(quantidade);

    for(int i=0;i<quantidade;i++) {
        if(acomodacoes[i].acoCodigo == codigo) {
            acomodacao = acomodacoes[i];
            free(acomodacoes);
            return acomodacao;
        }
    }
    acomodacao.acoCodigo = -1;
    free(acomodacoes);
    return acomodacao;
}