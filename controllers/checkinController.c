/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../models/checkinModel.h"
#include "../models/configuracaoModel.h"
#include "../models/funcoesModel.h"
#include "../models/reservaModel.h"
#include "../models/hospedeModel.h"
#include "../models/acomodacaoModel.h"
#include "../models/controleCaixaModel.h"
#include "../structs/moduloCadastroStructs.h"

void fazerCheckin() {
    Acomodacao acomodacao;
    // encontra o ID do hóspede
    printf("Digite o CPF do hóspede: ");
    char cpfDoHospede[12];
    inputString(cpfDoHospede, 12);
    Hospede hospede = encontrarHospedePorCPF(cpfDoHospede);
    if (hospede.hospCodigo > 0) {
        printf("Hospede encontrado. Nome: %s\n", hospede.hospNome);
        int reservasEncontradas = 0;
        Reserva* reservas = encontrarReservasDoHospede(hospede.hospCodigo, &reservasEncontradas);

        if (reservasEncontradas > 0) {
            printf("Reservas do hóspede:\n\n");
            for (int i=0; i<reservasEncontradas; i++) {
                printf(" Código da reserva: %d\n", reservas[i].resCodigo);
                acomodacao = buscarAcomodacaoPorCodigo(reservas[i].resAcomodacaoId);
                printf(" Acomodação: %d: %s \n", acomodacao.acoCodigo, acomodacao.acoDescricao);
                printf(" Data de inicio: %d/%d/%d\n", reservas[i].resDataInicio.dia, reservas[i].resDataInicio.mes, reservas[i].resDataInicio.ano);
                printf(" Data de término: %d/%d/%d\n", reservas[i].resDataFim.dia, reservas[i].resDataFim.mes, reservas[i].resDataFim.ano);
                printf("\n");
            }
        }
        else {
            printf("Não foram encontradas reservas para este hóspede nesta data.\n");
            free(reservas);
            return;
        }
    }
    else {
        printf("Não foi encontrado hospede com este CPF.\n");
        return;
    }

    // definir qual o metodo de encontrar a reserva, e encontrar a tal reserva
    int idReserva = 0;
    printf("Digite o código da reserva: ");
    scanf("%d%*c", &idReserva);

    int reservaCounter = 0;
    Reserva* reservas = carregarReservas(&reservaCounter);

    // verifica se a reserva existe
    int existente = 0;
    int indexID;
    for (int i = 0; i < reservaCounter; i++) {
        if (reservas[i].resCodigo == idReserva) {
            existente = 1;
            indexID = i;
            break;
        }
    }
    if (existente == 0) {
        printf("A reserva com id %d não pôde ser encontrada\n", idReserva);
        free(reservas);
        return;
    }

    // definir o horário de chegada da reserva
    reservas[indexID].resDataTime_chegada = gerarDataEHoraAtual();

    // gerar valor da reserva
    float valorDePagamento = 0; // substituir posteriormente
    reservas[indexID].resDataTime_saida = gerarDataEHoraAtual();
    reservas[indexID].resDataTime_saida.dia = reservas[indexID].resDataFim.dia;
    int quantidadeDias = calculaDiasEntreDatas(reservas[indexID].resDataTime_chegada, reservas[indexID].resDataTime_saida);
    valorDePagamento = quantidadeDias * acomodacao.acoCategoria.catValorDiaria;
    printf("O valor da reserva será de %.2f reais\n", valorDePagamento);

    // perguntar o modo e tempo de pagamento
    int vezDePagar = 0;
    printf("O cliente deseja pagar agora ou depois? \n1 - Pagar Agora; 2 - Pagar no check-out: \n");
    scanf("%d%*c", &vezDePagar);

    Pagamento pagamento;
    // setar a reserva como paga ou não paga
    if (vezDePagar == 1) { // receber pagamento agora
        printf("Forma de pagamento: \n");
        printf("1- À vista \n");
        printf("2- No cartão \n");
        scanf("%d%*c",&pagamento.formaPagamento);
        if(pagamento.formaPagamento == 2) {
            printf("Deseja dar uma entrada? 1- sim   2- não \n");
            scanf("%d%*c",&pagamento.isEntrada);
            if(pagamento.isEntrada == 1) {
                printf("Insira o valor da entrada: \n");
                scanf("%f",&pagamento.valorEntrada);
            }
            printf("Deseja dividir em quantas vezes? \n");
            scanf("%d",&pagamento.numeroParcelas);
            printf("Data de vencimento \n");
            printf("Dia:");
            scanf("%d",&pagamento.dataVencimento.dia);
            printf("Mês:");
            scanf("%d",&pagamento.dataVencimento.mes);
            printf("Ano:");
            scanf("%d",&pagamento.dataVencimento.ano);
            if(montaESalvaContaReceberPagar(pagamento,"diaria","aguardando_lancamento","entrada", valorDePagamento, reservas[indexID].resCodigo) == 1) {
                printf("Pagamento realizado com sucesso! \n");
            }
        } else if(pagamento.formaPagamento == 1) {
            Historico_Caixa historico_Caixa;
            historico_Caixa = montaHistoricoCaixa(valorDePagamento,"entrada",reservas[indexID].resCodigo,"diaria");
            if(cadastrarHistorico(historico_Caixa) == 1) {
                printf("Pagamento realizado com sucesso! \n");
            }
        }
        reservas[indexID].resStatus_pagamento = status_pago;
    }
    else if (vezDePagar == 2) { // pagamento será no check-out
        printf("Configurando reserva para pagamento no check-out\n");
        reservas[indexID].resStatus_pagamento = status_aguardando_pagamento;
    }

    reservas[indexID].resDataTime_chegada = gerarDataEHoraAtual();
    salvarReservas(reservas, reservaCounter);
    free(reservas);
    return;
}
