/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../structs/moduloCadastroStructs.h"
#include "../models/acomodacaoModel.h"
#include "../models/hospedeModel.h"
#include "../models/funcoesModel.h"
#include "../models/configuracaoModel.h"
#include <string.h>

Hospede *hospedeList;

// TODO: cadastrar arquivos em binários e txt
// TODO: gerar o id do hóspede utilizando arquivo seq_hospede

/**
 * Cadastrar hóspede - Chamando função
 * @param hospede Objeto para salvar
 * @return int  valor para tratamento de erro
 */
int cadastrarHospContoller(Hospede hospede) {
    return addFileHospede(hospede);
}

/**
 * Atualiza hóspede - Chamando função
 * @param hospede Objeto para atualizar
 * @param cod código do hóspede a ser atualizado
 * @return int  valor para tratamento de erro
 */
int atualizarHospedeController(int cod, Hospede hospede){
    editarHospede(cod, hospede);
    return 1;
}

/**
 * Lista todos os hóspedes cadastrados no sistema - Chamando função
 * @param
 * @return
 */
void listarHospController() {
    listarHospedes();
}

/**
 * Deleta hóspede por código - Chamando função
 * @param cod código do hóspede a ser deletado
 * @return int  valor para tratamento de erro
 */
int deletarHospedeController(int cod) {
    excluirHospede(cod);
    return 1;
}

/**
 * Salva os dados referentes a hóspedes
 * @param hospede Objeto para adicionar nos arquivos
 * @return int  valor para tratamento de erro
 */
int addFileHospede(Hospede hospede) {
    FILE *file = NULL;
    int opc = opcaoArmazenamentoUsuario();
    if (opc == 1) {
        file = fopen("persist/hospede.bin", "a+b");
        hospede.hospCodigo =sequenceArquivoTXT("persist/seq_hospede.bin");
        if (file == NULL) {
            mostrarMensagemErroArquivo();
            fwrite(&hospede, sizeof(Hospede), 1, file);
        }
        fclose(file);
    } else if (opc == 2) {
        file = fopen("persist/hospede.txt", "a+");
        hospede.hospCodigo = sequenceArquivoTXT("persist/seq_hospede.txt");
        if (file == NULL) {
            mostrarMensagemErroArquivo();
            return 0;
        }
        fprintf(file, "%s;%s;%s;%c;%s;%s;%s;%s;%s;%s;%s;%d;%s;%d;%d;\n",
                hospede.hospNome,
                hospede.hospCpf,
                hospede.hospEmail,
                hospede.hospSexo,
                hospede.hospEstadoCivil,
                hospede.hospDataNascimento,
                hospede.hospTelefone,
                hospede.hospEndereco.endRua,
                hospede.hospEndereco.endBairro,
                hospede.hospEndereco.endCidade,
                hospede.hospEndereco.endEstado,
                hospede.hospEndereco.endCep,
                hospede.hospEndereco.endComplemento,
                hospede.hospEndereco.endNumero,
                hospede.hospCodigo);
        fclose(file);
        return 1;
    }
    return 0;
}

/**
 * Informa a quantidade de hóspede cadastrados no sistema
 * @param
 * @return int retorna a quantidade de hospede
 */
int contarHospedes() {
    FILE *file;
    int counter = 0;
    int opc = opcaoArmazenamentoUsuario();
    if (opc == 1) {
        file = fopen("persist/hospede.bin", "rb");
        if (file == NULL) {
            mostrarMensagemErroArquivo();
            file = fopen("persist/hospede.bin", "w+b");
        }
        fseek(file, 0, SEEK_END);
        int count = ftell(file) / sizeof(Hospede);
        fclose(file);
        return count;
    } else if (opc == 2){
        file = fopen("persist/hospede.txt", "r");
        if (file == NULL) {
            printf("o arquivo de hospede(texto) não pôde ser aberto. Criando novo arquivo.\n");
            file = fopen("persist/hospede.txt", "w+");
        }
        char line[300];
        while (fgets(line, 300, file)) {
            counter++;
        }
        fclose(file);
    }
    return counter;
}

/**
* Lista os dados de todos os hóspedes cadastrados no sistema
* @param
* @return
*/
void listarHospedes() {
    int hospedeCount = contarHospedes();
    carregarHospede(hospedeCount);
    for (int i = 0; i < hospedeCount; i++)
    {
        printf("Código: %d \n", hospedeList[i].hospCodigo);
        printf("Nome: %s \n", hospedeList[i].hospNome);
        printf("Cpf: %s \n", hospedeList[i].hospCpf);
        // TODO: mostrar todas as informações cadastradas sobre o hóspede
        printf("-------------\n");
    }
}

/**
* Lê os arquivos e tranforma em ponteiro
* @param counter numero de registros nos arquivos
* @return
*/
void carregarHospede(int counter){
    int tipoArquivo = opcaoArmazenamentoUsuario();
    Hospede *lista;
    FILE *file;
    lista = calloc(counter, sizeof(Hospede));
    if (tipoArquivo == 1) {
        file = fopen("persist/hospede.bin", "rb");
        if (file == NULL) {
            mostrarMensagemErroArquivo();
            return;
        }
        fread(lista, sizeof(Hospede), counter, file);
    } else if (tipoArquivo == 2){
        file = fopen("persist/hospede.txt", "r");
        if (file == NULL) {
            printf("o arquivo (texto) não pôde ser aberto. Criando um novo arquivo.\n");
            file = fopen("persist/hospede.txt", "w+");
        } else {
            char line[300];
            for (int i = 0; i < counter; i++) {
                fgets(line, 300, file);
                char *nome = strtok(line, ";");
                strcpy(lista[i].hospNome, nome);
                char *cpf = strtok(NULL, ";");
                strcpy(lista[i].hospCpf, cpf);
                char *email = strtok(NULL, ";");
                strcpy(lista[i].hospEmail, email);
                strcpy(&lista[i].hospSexo, strtok(NULL, ";"));
                char *hospEstadoCivil = strtok(NULL, ";");
                strcpy(lista[i].hospEstadoCivil, hospEstadoCivil);
                char *dataNasc = strtok(NULL, ";");
                strcpy(lista[i].hospDataNascimento, dataNasc);
                char *telefone = strtok(NULL, ";");
                strcpy(lista[i].hospTelefone, telefone);
                char *rua = strtok(NULL, ";");
                strcpy(lista[i].hospEndereco.endRua, rua);
                char *bairro = strtok(NULL, ";");
                strcpy(lista[i].hospEndereco.endBairro, bairro);
                char *cidade = strtok(NULL, ";");
                strcpy(lista[i].hospEndereco.endCidade, dataNasc);
                char *estado = strtok(NULL, ";");
                strcpy(lista[i].hospEndereco.endEstado, estado);
                int cep = atoi(strtok(NULL, ";"));
                lista[i].hospEndereco.endCep = cep;
                char *complemento = strtok(NULL, ";");
                strcpy(lista[i].hospEndereco.endComplemento, complemento);
                lista[i].hospEndereco.endNumero = atoi(strtok(NULL, ";"));
                lista[i].hospCodigo = atoi(strtok(NULL, ";\n"));
            }
        }
    }
    fclose(file);
    hospedeList = lista;
}

/**
 * Função para editar hospede
 * @param codigo do hospede que sera editado
 * @param hospede dados novos do hospede
 * @return int
 */
int editarHospede(int cod, Hospede hospede) {
    int hospedeCount = contarHospedes();
    carregarHospede(hospedeCount);
    Hospede *oldHospedes = hospedeList;
    FILE *file = NULL;
    int opc = opcaoArmazenamentoUsuario();
    if (opc == 1) {
        file = fopen("persist/hospede.bin", "wb");
        if (file == NULL) {
            mostrarMensagemErroArquivo();
            return 0;
        }
        fclose(file);
    } else if (opc == 2){
        file = fopen("persist/hospede.txt", "w");
        if (file == NULL) {
            return 0;
        }
        fclose(file);
    }
    for (int i = 0; i < hospedeCount; i++) {
        if (oldHospedes[i].hospCodigo == cod) {
            oldHospedes[i] = hospede;
        }
    }
    for (int j = 0; j < hospedeCount; j++) {
        addFileHospede(oldHospedes[j]);
    }
}

/**
 * Verifica se o Usuario deseja realmente excluir o registro
 * @param codigo por paramentro
 * @return int para tratar erro
 */
int excluirHospede(int cod) {
    setbuf(stdin, NULL);
    char escolha;
    printf("Tem certeza que deseja excluir o registro desse hospede? (s ou n) ");
    scanf("%c", &escolha);
    setbuf(stdin, NULL);
    if (escolha == 's' || escolha == 'S'){
        excluirHospedePorCodigo(cod);
    }
}

/**
* Exclui o hospede cadastrado por parâmeto
* @param codigo
* @return void
*/
void excluirHospedePorCodigo(int codigo) {
    int count = contarHospedes();
    int opc = opcaoArmazenamentoUsuario();
    carregarHospede(count);
    FILE *file = NULL;
    if (opc == 1) {
        file = fopen("persist/hospede.bin", "w");
        if (file == NULL) {
            mostrarMensagemErroArquivo();
            return;
        }
        fclose(file);
    } else if (opc == 2) {
        file = fopen("persist/hospede.txt", "w");
        if (file == NULL){
            return;
        }
        fclose(file);
        for (int i = 0; i < count; i++) {
            if (hospedeList[i].hospCodigo != codigo) {
                addFileHospede(hospedeList[i]);
            }
        }
    }
}

/*
* Carrega uma lista de hospedes do arquivo, em um vetor local, e armazena a quantidade de hospedes na variavel passada
* @param: int* counter: endereço de uma variavel para armazenar a quantidade de hóspedes
* @return: endereço de um vetor dinamicamente alocado, contendo as informações dos hóspedes
*/
Hospede* loadHospedeList(int* counter) {
    int tipoArquivo = opcaoArmazenamentoUsuario();
    Hospede *lista = NULL;
    FILE *file = NULL;

    if (tipoArquivo == TipoArquivoBinario) {
        file = fopen("persist/hospede.bin", "rb");
        if (file == NULL) {
            mostrarMensagemErroArquivo();
            return NULL;
        }
        fseek(file, 0, SEEK_END);
        *counter = ftell(file) / sizeof(Hospede);
        rewind(file);
        lista = malloc(sizeof(Hospede) * (*counter));
        fread(lista, sizeof(Hospede), *counter, file);
        fclose(file);
    } else if (tipoArquivo == TipoArquivoTexto) {
        file = fopen("persist/hospede.txt", "r");
        if (file == NULL) {
            mostrarMensagemErroArquivo();
            return NULL;
        }

        int count = 0;
        char line[300];
        while (fgets(line, 300, file)) {
            count++;
        }
        rewind(file);
        *counter = count;
        lista = malloc(sizeof(Hospede) * (*counter));

        for (int i = 0; i < *counter; i++) {
            fgets(line, 300, file);
            char *nome = strtok(line, ";");
            strcpy(lista[i].hospNome, nome);
            char *cpf = strtok(NULL, ";");
            strcpy(lista[i].hospCpf, cpf);
            char *email = strtok(NULL, ";");
            strcpy(lista[i].hospEmail, email);
            char* sexo = strtok(NULL, ";");
            strcpy(&lista[i].hospSexo, sexo);
            char* estadoCivil = strtok(NULL, ";");
            strcpy(lista[i].hospEstadoCivil, estadoCivil);
            char *dataNasc = strtok(NULL, ";");
            strcpy(lista[i].hospDataNascimento, dataNasc);
            char *telefone = strtok(NULL, ";");
            strcpy(lista[i].hospTelefone, telefone);
            char *rua = strtok(NULL, ";");
            strcpy(lista[i].hospEndereco.endRua, rua);
            char *bairro = strtok(NULL, ";");
            strcpy(lista[i].hospEndereco.endBairro, bairro);
            char *cidade = strtok(NULL, ";");
            strcpy(lista[i].hospEndereco.endCidade, cidade);
            char *estado = strtok(NULL, ";");
            strcpy(lista[i].hospEndereco.endEstado, estado);
            int cep = atoi(strtok(NULL, ";"));
            lista[i].hospEndereco.endCep = cep;
            char *complemento = strtok(NULL, ";");
            strcpy(lista[i].hospEndereco.endComplemento, complemento);
            lista[i].hospEndereco.endNumero = atoi(strtok(NULL, ";"));
            lista[i].hospCodigo = atoi(strtok(NULL, ";\n"));
        }
        fclose(file);
    }
    return lista;
}

/**
* Busca hospede com o nome descrito
* @param char *nome: O nome do hospede
* @return Hospede com o nome desejado ou um hospede com endereço -1 caso este não exista
*/
Hospede encontrarHospedePorNome(char* nome) {
    int qtHospede = 0;
    Hospede* hospList = loadHospedeList(&qtHospede);
    for(int i=0; i < qtHospede; i++) {
        if (strcmp(nome, hospList[i].hospNome) == 0) {
            Hospede hospedeBuscado = hospList[i];
            free(hospList);
            return hospedeBuscado;
        }
    }
    Hospede vazio;
    vazio.hospCodigo = -1;
    free(hospList);
    return vazio;
}

/**
* Busca hospede com o cpf informado
* @param char *cpf: cpf do hospede
* @return Hospede com o cpf desejado ou um hospede com endereço -1 caso este não exista
*/
Hospede encontrarHospedePorCPF(char* cpf) {
    int qtHospede = 0;
    Hospede* hospList = loadHospedeList(&qtHospede);
    for(int i=0; i < qtHospede; i++) {
        if (strcmp(cpf, hospList[i].hospCpf) == 0) {
            Hospede hospedeBuscado = hospList[i];
            free(hospList);
            return hospedeBuscado;
        }
    }
    Hospede vazio;
    vazio.hospCodigo = -1;
    free(hospList);
    return vazio;
}

/**
* Busca hospede com o codigo informado
* @param char *codigo: codigo do hospede
* @return Hospede com o codigo desejado ou um hospede com endereço -1 caso este não exista
*/
Hospede encontrarHospedePorCodigo(int codigo) {
    int qtHospede = 0;
    Hospede* hospList = loadHospedeList(&qtHospede);
    for(int i=0; i < qtHospede; i++) {
        if (hospList[i].hospCodigo == codigo) {
            Hospede hospedeBuscado = hospList[i];
            free(hospList);
            return hospedeBuscado;
        }
    }
    Hospede vazio;
    vazio.hospCodigo = -1;
    free(hospList);
    return vazio;
}
