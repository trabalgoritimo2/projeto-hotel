#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "../models/variadics.h"

int isTableInXML(char* path, char* table);

void startXML(char* path) {
    FILE* file = fopen(path, "w");
    fprintf(file, "<dados>\n</dados>");
    fclose(file);
}

void deleteTable(char* path, char* table) {
    FILE* file = fopen(path, "r");

    if (file == NULL) {
        printf("ERRO\n");
    }

    fseek(file, 0, SEEK_END);
    int numbytes = ftell(file);
    rewind(file);
    char* buffer = calloc(numbytes+1, sizeof(char));
    fread(buffer, sizeof(char), numbytes, file);
    buffer[numbytes] = '\0';
    fclose(file);

    char* openTable = calloc(strlen(table) + strlen("\t<Tabela=>") + 1, sizeof(char));
    strcat(openTable, "\t<Tabela=");
    strcat(openTable, table);
    strcat(openTable, ">");
    // openTable[strlen(table) + strlen("\t<Tabela=>")] = '\0';

    char* tableStart = strstr(buffer, openTable);
    char* pre = calloc(strlen(buffer) - strlen(tableStart) + 1, sizeof(char));
    for (int i=0; i<(strlen(buffer) - strlen(tableStart)); i++){
        pre[i] = buffer[i];
    }
    // pre[]

    char* post = strstr(tableStart, "</Tabela>") + strlen("</Tabela>\n");

    file = fopen(path, "w");

    fprintf(file, "%s", pre);
    fprintf(file, "%s", post);

    fclose(file);

    if (buffer != NULL) {
        free(buffer);
    }
    if (openTable != NULL){
        free(openTable);
    }
    if (pre != NULL) {
        free(pre);
    }
}

void createTable(char* path, char* table) {
    if (isTableInXML(path, table)) {
        deleteTable(path, table);
    }
    FILE* file = fopen(path, "r");
    fseek(file, 0, SEEK_END);
    int numbytes = ftell(file);
    // printf("bytes: %d\n", numbytes);
    rewind(file);
    char* buffer = calloc(numbytes+1, sizeof(char));
    fread(buffer, sizeof(char), numbytes, file);
    buffer[numbytes] = '\0';

    // printf("%s\n", buffer);

    char* openTag = calloc(strlen(table) + strlen("<Tabela=>") + 1, sizeof(char));
    strcat(openTag, "<Tabela=");
    strcat(openTag, table);
    strcat(openTag, ">");
    // printf("open: %s\n", openTag);

    // char* openTag = malloc(sizeof(char) * (strlen(table) + 2));
    // openTag[0] = '<';
    // strncpy(openTag+1, table, strlen(table));
    // openTag[strlen(table) + 1] = '>';
    // printf("open: %s\n", openTag);

    char* closeTag = "</Tabela>";

    // char* closeTag = malloc(sizeof(char) * (strlen(table) + 3));
    // closeTag[0] = '<';
    // closeTag[1] = '/';
    // strncpy(closeTag+2, table, strlen(table));
    // closeTag[strlen(table) + 2] = '>';
    // printf("close: %s\n", closeTag);

    char* post = strstr(buffer, "</dados>");
    // printf("post: %s\n", post);

    if (post == NULL) {
        printf("Tag de fechamento não foi encontrada\n");
        return;
    }

    // printf("buffer: %s; len: %ld\n", buffer, strlen(buffer));
    // printf("post: %s; len: %ld\n", post, strlen(post));
    //
    // printf("Diff: %ld\n", strlen(buffer) - strlen(post));

    // char* pre;
    char* pre = calloc(strlen(buffer) - strlen(post) + 1, sizeof(char));
    int i = 0;
    // pre[i] = buffer[i];
    while ((buffer+i) != post) {
        // printf("CREATETABLE -- PRE -- WHILE\n");
        pre[i] = buffer[i];
        i += sizeof(char);
    }
    // printf("pre: %s; len= %ld\n", pre, strlen(pre));



    fclose(file);
    file = fopen(path, "w");

    fprintf(file, "%s", pre);
    fprintf(file, "\t%s\n", openTag);
    fprintf(file, "\t%s\n", closeTag);
    fprintf(file, "%s", post);

    fclose(file);

    if (buffer != NULL) {
        free(buffer);
    }
    if (openTag != NULL) {
        free(openTag);
    }
    if (pre != NULL) {
        free(pre);
    }
}

int isTableInXML(char* path, char* table) {
    FILE* file = fopen(path, "r");
    fseek(file, 0, SEEK_END);
    int numbytes = ftell(file);
    rewind(file);
    char* buffer = calloc(numbytes+1, sizeof(char));
    fread(buffer, sizeof(char), numbytes, file);
    buffer[numbytes] = '\0';

    char* tabela = calloc(strlen(table) + strlen("Tabela=") + 1, sizeof(char));
    strcat(tabela, "Tabela=");
    strcat(tabela, table);

    int isTableIn = 0;
    if (strstr(buffer, tabela) != NULL) {
        isTableIn = 1;
    }

    if (buffer != NULL) {
        free(buffer);
    }
    if (tabela != NULL) {
        free(tabela);
    }
    fclose(file);
    return isTableIn;
}

void registerToXML(char* path, char* table, char* formats, ...) {
    // garante que a tabela requisitada existe
    if (!(isTableInXML(path, table))) {
        printf("Esta tabela não existe\n");
    }
    FILE* file = fopen(path, "r");
    if (file == NULL) {
        printf("PROBLEM\n");
        return;
    }

    // copia os dados do arquivo para um buffer
    fseek(file, 0, SEEK_END);
    int numbytes = ftell(file);
    rewind(file);
    char* buffer = calloc(numbytes+1, sizeof(char));
    fread(buffer, sizeof(char), numbytes, file);
    buffer[numbytes] = '\0';
    fclose(file);

    // recria a tag de tabela pedida
    char* tabela = calloc(strlen(table) + strlen("<Tabela=>") + 1, sizeof(char));
    strcat(tabela, "<Tabela=");
    strcat(tabela, table);
    strcat(tabela, ">");

    // encontra o ponto no buffer que se encontra o inicio da tabela
    char* initTabela = strstr(buffer, tabela);

    // ecnontra o ponto no buffer para copiar após a inserção
    char* post = strstr(initTabela, "\t</Tabela>");

    // encontra o ponto no buffer que se encontra a tag de fechamento dessa tabela
    // char* postPtr = strstr(initTabela, "\t</Tabela>");
/*
    // copia toda a parte posterior com a tag de fechamento inclusa
    char* post = NULL;
    int i=0;
    while (postPtr[i] != '\0') {
        post = realloc(post, sizeof(char) * (i+1));
        post[i] = postPtr[i];
        i++;// += sizeof(char);
    }
    post = realloc(post, sizeof(char) * (i+1));
    post[i] = '\0';
    i++;
*/

    char* pre = calloc(strlen(buffer) - strlen(post) + 1, sizeof(char));
    int i=0;
    while (buffer+i != post) {
        // printf("RETISTER_TO_TABLE -- PRE -- WHILE\n");
        pre[i] = buffer[i];
        i++;
    }
/*    i = 0;
    while ((buffer+i) != postPtr) {
        pre = realloc(pre, sizeof(char) * (i+1));
        pre[i] = buffer[i];
        i++;// += sizeof(char);
    }
    pre = realloc(pre, sizeof(char) * (i+1));
    pre[i] = '\0';
*/


    file = fopen(path, "w");

    fprintf(file, "%s", pre);

    fprintf(file, "\t\t<registro>\n");

    // char* insert = calloc(strlen("\t\t<registro>\n"), sizeof(char));
    // strcat(insert, "\t\t<registro>\n");

    int argCount = 0;
    for (i=0; i<strlen(formats); i++) {
        if (formats[i] == ';') {
            argCount++;
        }
    }

    va_list ptr;
    va_start(ptr, formats);

    char* cursor = formats;
    char* lastComma = cursor;
    // char* remainArgFormats = strstr(formats, ";");
    for (i=0; i<argCount; i++) {
        while ((*cursor) != ';') {
            // printf("CREATETABLE -- CURSOR_SEEK_; -- WHILE\n");
            cursor++;
        }

        char* argFormat = calloc(strlen(lastComma) - strlen(cursor) + 1, sizeof(char));
        int j=0;
        while (lastComma+j != cursor) {
            // printf("CREATETABLE -- LASCOMMA_TO_CURSOR -- WHILE\n");
            argFormat[j] = lastComma[j];
            j++;
        }
        cursor++;
        lastComma = cursor;

        j=0;
        while (argFormat[j] != '=') {
            // printf("CREATETABLE -- ARGFORMAT_SEEK_= -- WHILE\n");
            j++;
        }

        char* tag = calloc(j+1, sizeof(char));
        int k=0;
        while (k < j) {
            // printf("CREATETABLE -- TAG -- WHILE\n");
            tag[k] = argFormat[k];
            k++;
        }


/*
        char* tag = NULL;
        char* tagCursor = argFormat;
        int k = 0;
        while (*tagCursor != '=') {
            tag = realloc(tag, sizeof(char) * (k+1));
            tag[k] = *tagCursor;
            k++;
            tagCursor++;
        }
        tag = realloc(tag, sizeof(char) * (k+1));
        tag[k] = '\0';
        k++;
        tagCursor++;
*/

        char* format = strstr(argFormat, "%");

        char* openTag = calloc(strlen(tag) + strlen("\t\t\t<>") + 1, sizeof(char));
        strcat(openTag, "\t\t\t<");
        strcat(openTag, tag);
        strcat(openTag, ">");

        char* closeTag = calloc(strlen(tag) + strlen("</>\n") + 1, sizeof(char));
        strcat(closeTag, "</");
        strcat(closeTag, tag);
        strcat(closeTag, ">\n");

        //printf("TAGS CREATED\n");
        //printf("currect tag: %s\n", openTag);

        fprintf(file, "%s", openTag);

        char* result = NULL;
        if (strcmp(format, "%d") == 0) {
            int argInt = va_arg(ptr, int);
            result = calloc(30, sizeof(char));
            sprintf(result, "%d", argInt);
        }
        if (strcmp(format, "%f") == 0) {
            float argFloat = va_arg(ptr, double);
            result = calloc(30, sizeof(char));
            sprintf(result, "%f", argFloat);
        }
        if (strcmp(format, "%s") == 0) {
            char* argString = va_arg(ptr, char *);
            result = calloc(strlen(argString) + 1, sizeof(char));
            strcpy(result, argString);
        }
        // REMOVAME se a passagem de char unico não estiver funcionando
        if (strcmp(format, "%c") == 0) {
            char argChar = va_arg(ptr, int);
            result = calloc(2, sizeof(char));
            result[0] = argChar;
        }
        // REMOVAME ATE AQUI

        //printf("RESULT EXTRACTED\n");

        fprintf(file, "%s", result);
        fprintf(file, "%s", closeTag);

        //printf("RESULT AND CLOSETAG WRITEN\n");

        if (argFormat != NULL) {
            free(argFormat);
        }
        if (tag != NULL) {
            free(tag);
        }
        if (openTag != NULL) {
            free(openTag);
        }
        if (closeTag != NULL) {
            free(closeTag);
        }
        if (result != NULL) {
            free(result);
        }

        //printf("END OF LOOP %d\n", i);
    }

    fprintf(file, "\t\t</registro>\n");
    fprintf(file, "%s", post);

    fclose(file);

    if (buffer != NULL) {
        free(buffer);
    }
    if (tabela != NULL) {
        free(tabela);
    }
    if (pre != NULL) {
        free(pre);
    }

    //printf("END OF REGISTER FUNCTION\n");
}

int registersInTable(char* path, char* table) {
    int count = 0;
    if (!isTableInXML(path, table)) {
        return count;
    }

    FILE* file = fopen(path, "r");
    fseek(file, 0, SEEK_END);
    int numbytes = ftell(file);
    rewind(file);
    char* buffer = calloc(numbytes+1, sizeof(char));
    fread(buffer, sizeof(char), numbytes, file);
    buffer[numbytes] = '\0';
    fclose(file);

    char* tagTable = calloc(strlen(table) + strlen("<Tabela=>") + 1, sizeof(char));
    strcat(tagTable, "<Tabela=");
    strcat(tagTable, table);
    strcat(tagTable, ">");

    char* startOfTable = strstr(buffer, tagTable);
    char* endOfTable = strstr(startOfTable, "</Tabela>");

    // Cuidado ao usar 'register' para nomear variaveis. Esta é uma keyword de C.
    char* registers = calloc(strlen(startOfTable) - strlen(endOfTable) + 1, sizeof(char));
    int i=0;
    while (startOfTable+i != endOfTable) {
        registers[i] = startOfTable[i];
        i++;
    }
    registers[i] = '\0';


    char* reg = registers;
    while (reg = strstr(reg, "<registro>")) {
        count++;
        reg += strlen("<registro>");
    }

    if (buffer != NULL) {
        free(buffer);
    }
    if (tagTable != NULL) {
        free(tagTable);
    }
    if (registers != NULL) {
        free(registers);
    }

    return count;
}

void readFromXML(char* path, char* table, int index, char* formats, ...) {
    if (!isTableInXML(path, table)) {
        return;
    }

    FILE* file = fopen(path, "r");
    fseek(file, 0, SEEK_END);
    int numbytes = ftell(file);
    rewind(file);
    char* buffer = calloc(numbytes+1, sizeof(char));
    fread(buffer, sizeof(char), numbytes, file);
    buffer[numbytes] = '\0';
    fclose(file);

    char* tagTable = calloc(strlen(table) + strlen("<Tabela=>") + 1, sizeof(char));
    strcat(tagTable, "<Tabela=");
    strcat(tagTable, table);
    strcat(tagTable, ">");

    char* startOfTable = strstr(buffer, tagTable);
    char* endOfTable = strstr(startOfTable, "</Tabela>");

    // Cuidado au usar 'register' para nomear variaveis. Esta é uma keyword de C.
    char* registers = calloc(strlen(startOfTable) - strlen(endOfTable) + 1, sizeof(char));
    int i=0;
    while (startOfTable+i != endOfTable) {
        registers[i] = startOfTable[i];
        i++;
    }
    registers[i] = '\0';

    char* reg = strstr(registers, "<registro>")+strlen("<registro>");
    for (i=0; i<index; i++) {
        reg = strstr(reg, "<registro>")+strlen("<registro>");
    }

    char* endOfReg = strstr(reg, "</registro>");

    char* regData = calloc(strlen(reg) - strlen(endOfReg) + 1, sizeof(char));
    i=0;
    while (reg+i != endOfReg) {
        regData[i] = reg[i];
        i++;
    }

    int argCount = 0;
    for (i=0; i<strlen(formats); i++) {
        if (formats[i] == ';') {
            argCount++;
        }
    }

    char* cursor = formats;
    char* lastComma = cursor;
    va_list ptr;
    va_start(ptr, formats);
    for (i=0; i<argCount; i++) {
        while ((*cursor) != ';') {
            // printf("CREATETABLE -- CURSOR_SEEK_; -- WHILE\n");
            cursor++;
        }

        char* argFormat = calloc(strlen(lastComma) - strlen(cursor) + 1, sizeof(char));
        int j=0;
        while (lastComma+j != cursor) {
            // printf("CREATETABLE -- LASCOMMA_TO_CURSOR -- WHILE\n");
            argFormat[j] = lastComma[j];
            j++;
        }
        cursor++;
        lastComma = cursor;

        j=0;
        while (argFormat[j] != '=') {
            // printf("CREATETABLE -- ARGFORMAT_SEEK_= -- WHILE\n");
            j++;
        }

        char* tag = calloc(j+1, sizeof(char));
        int k=0;
        while (k < j) {
            // printf("CREATETABLE -- TAG -- WHILE\n");
            tag[k] = argFormat[k];
            k++;
        }

        char* format = strstr(argFormat, "%");

// codigo velho com alocação dinamica ruim
/*
        char* argFormat = NULL;
        int j=0;
        while ((*cursor) != ';') {
            argFormat = realloc(argFormat, sizeof(char) * (j+1));
            argFormat[j] = *cursor;
            j++;
            cursor++;
        }
        argFormat = realloc(argFormat, sizeof(char) * (j+1));
        argFormat[j] = '\0';
        j++;
        cursor++;

        char* tag = NULL;
        char* tagCursor = argFormat;
        int k = 0;
        while (*tagCursor != '=') {
            tag = realloc(tag, sizeof(char) * (k+1));
            tag[k] = *tagCursor;
            k++;
            tagCursor++;
        }
        tag = realloc(tag, sizeof(char) * (k+1));
        tag[k] = '\0';
        k++;
        tagCursor++;

        char* format = strstr(argFormat, "%");
*/

        char* openTag = calloc(strlen(tag) + strlen("\t\t\t<>") + 1, sizeof(char));
        strcat(openTag, "\t\t\t<");
        strcat(openTag, tag);
        strcat(openTag, ">");

        char* closeTag = calloc(strlen(tag) + strlen("</>\n") + 1, sizeof(char));
        strcat(closeTag, "</");
        strcat(closeTag, tag);
        strcat(closeTag, ">\n");

        char* start = strstr(regData, openTag) + strlen(openTag);
        char* end = strstr(regData, closeTag);

        char* data = calloc(strlen(start) - strlen(end) + 1, sizeof(char));
        strncpy(data, start, strlen(start) - strlen(end));
        data[strlen(start) - strlen(end)] = '\0';
        //printf("DATA: %s\n", data);

        if (strcmp(format, "%d") == 0) {
            int* argInt = va_arg(ptr, int *);
            (*argInt) = atoi(data);
        }
        if (strcmp(format, "%f") == 0) {
            float* argFloat = (float*) va_arg(ptr, double *);
            (*argFloat) = atof(data);
        }
        if (strcmp(format, "%s") == 0) {
            char* argString = va_arg(ptr, char *);
            strcpy(argString, data);
        }
        // REMOVAME se a leitura de char unico não estiver funcionando
        if (strcmp(format, "%c") == 0) {
            char* argChar = va_arg(ptr, char *);
            (*argChar) = data[0];
        }
        // REMOVAME ATE AQUI

        if (argFormat != NULL) {
            free(argFormat);
        }
        if (tag != NULL) {
            free(tag);
        }
        if (openTag != NULL) {
            free(openTag);
        }
        if (closeTag != NULL) {
            free(closeTag);
        }
        if (data != NULL) {
            free(data);
        }
    }


    if (buffer != NULL) {
        free(buffer);
    }
    if (tagTable != NULL) {
        free(tagTable);
    }
    if (registers != NULL) {
        free(registers);
    }
    if (regData != NULL) {
        free(regData);
    }
}

// APENAS COMO REFERENCIA
//TODO: apagar depois
/*
int main() {
    char* path = "dudus.xml";
    startXML(path);


    createTable(path, "hospede");
    createTable(path, "reserva");

    registerToXML(path, "hospede", "codigo=%d;nome=%s;endereço=%s;", 1, "Leandro", "rua maluka");
    registerToXML(path, "hospede", "codigo=%d;nome=%s;endereço=%s;", 2, "Jean", "estrada velha");
    registerToXML(path, "hospede", "codigo=%d;nome=%s;endereço=%s;", 3, "Vitor", "Cidade Longe");

    registerToXML(path, "reserva", "ID=%d;hospede_id=%d;data_inicial=%s;valor_pagamento=%f;", 2, 4, "2/3/2022", 678.43);

    registersInTable(path, "reserva");

    printf("%d registros\n", registersInTable(path, "reserva"));

    int cod;
    char nome[50];
    char endere[100];
    readFromXML(path, "hospede", 2, "codigo=%d;nome=%s;endereço=%s;", &cod, &nome, &endere);

    printf("codigo: %d\n", cod);
    printf("nome: %s\n", nome);
    printf("endereço: %s\n", endere);

}
*/
