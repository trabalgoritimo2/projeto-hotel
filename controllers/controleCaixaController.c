/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../structs/moduloCadastroStructs.h"
#include "../models/controleCaixaModel.h"
#include "../models/funcoesModel.h"
#include "../models/configuracaoModel.h"

#define SUCCESS 1;
#define ERROR 0;

Contas_Receber_Pagar *conta2 = NULL;
Historico_Caixa *pagamentos22 = NULL;

/**
* Atualiza o valor do caixa do hotel a partir de um historico de caixa
* @param Historico_Caixa historicoCaixa: historicoCaixa já cadastrado, int tipoArmazenamento: 1- binário 2-txt
* @return 1- se atualizou com sucesso 0: se houve algum problema
*/
int atualizarValorCaixa(Historico_Caixa historicoCaixa, int tipoArmazenamento) {
    FILE *file = NULL;
    switch (tipoArmazenamento) {
        case 1:
            file = fopen("persist/caixa.bin", "r+b");
            if(file != NULL){
                float valorTotal;
                fread(&valorTotal, sizeof (float), 1, file);
                fclose(file);
                if(strcmp(historicoCaixa.entrada_saida, "entrada") == 0){
                    valorTotal += historicoCaixa.valor;
                } else {
                    valorTotal = valorTotal - historicoCaixa.valor;
                }
                file = fopen("persist/caixa.bin", "wb");
                fwrite(&valorTotal, sizeof (float), 1, file);
                fclose(file);
                return SUCCESS;
            }
            mostrarMensagemErroArquivo();
            return ERROR;
            break;
        case 2:
            file = fopen("persist/caixa.txt", "r+");
            if(file != NULL){
                char valorTotal[30];
                float valorTotalFloat;
                fgets(valorTotal, 30, file);
                valorTotalFloat = atoi(strtok(valorTotal, ";"));
                fclose(file);
                if(strcmp(historicoCaixa.entrada_saida, "entrada") == 0){
                    valorTotalFloat += historicoCaixa.valor;
                } else {
                    valorTotalFloat = valorTotalFloat - historicoCaixa.valor;
                }
                file = fopen("persist/caixa.txt", "w");
                fprintf(file, "%f;", valorTotalFloat);
                fclose(file);
                return SUCCESS;
            }
            mostrarMensagemErroArquivo();
            return ERROR;
            break;
        default:
            mostrarMensagemErroTipoArquivo();
            return ERROR;
            break;
    }
}

/**
* Cadastra o historico de caixa e atualiza o caixa do hotel
* @param Historico_Caixa historicoCaixa: historicoCaixa que deseja cadastrar no sistema
* @return 1- se cadastrou com sucesso 0: se houve algum problema
*/
int cadastrarHistorico (Historico_Caixa historicoCaixa) {
    FILE *file = NULL;
    int tipoArquivo = opcaoArmazenamentoUsuario();
    switch(tipoArquivo){
        case 1:
            file = fopen("persist/historico_caixa.bin", "a+b");
            // Verificando se o arquivo foi aberto de forma correta
            if (file != NULL) {
                // Buscando o código do próximo registro no arquivo binário
                historicoCaixa.codigo = sequenceArquivoBinario("persist/seq_historico_caixa.bin");
                if(historicoCaixa.codigo != 0){
                    fwrite(&historicoCaixa, sizeof (Historico_Caixa), 1, file);
                    fclose(file);
                    if(atualizarValorCaixa(historicoCaixa,tipoArquivo) == 1){
                         return SUCCESS;
                    }
                    return ERROR;
                }
            }
            mostrarMensagemErroArquivo();
            return ERROR;    
            break;
        case 2:
            file = fopen("persist/historico_caixa.txt", "a+");
            // Verificando se o arquivo foi aberto de forma correta
            if(file != NULL){
                // Buscando o código do próximo registro no arquivo TXT
                historicoCaixa.codigo = sequenceArquivoTXT("persist/seq_historico_caixa.txt");
                if(historicoCaixa.codigo != 0){
                    fprintf(file, "%d;", historicoCaixa.codigo);
                    fprintf(file, "%s;", historicoCaixa.entrada_saida);
                    fprintf(file, "%s;", historicoCaixa.tipo);
                    fprintf(file, "%d;", historicoCaixa.id_origem);
                    fprintf(file, "%f;", historicoCaixa.valor);
                    fprintf(file, "%d-%d-%d;\n", historicoCaixa.data.ano,historicoCaixa.data.mes,historicoCaixa.data.dia);
                    fclose(file);
                }
                if(atualizarValorCaixa(historicoCaixa, tipoArquivo) == 1){
                    return SUCCESS;
                }
                return ERROR;
            }
            mostrarMensagemErroArquivo();
            return ERROR;
            break;
        default:
            mostrarMensagemErroTipoArquivo();
            return ERROR;
            break;
    }
}


/**
* Cadastra contas a receber ou a pagar
* @param Contas_Receber_Pagar conta: conta que deseja cadastrar no sistema
* @return 1- se cadastrou com sucesso 0: se houve algum problema
*/
int cadastrarContasAReceberPagar (Contas_Receber_Pagar conta) {
    FILE *file = NULL;
    int tipoArquivo = opcaoArmazenamentoUsuario();
    switch(tipoArquivo){
        case 1:
            file = fopen("persist/contas_receber_pagar.bin", "a+b");
            // Verificando se o arquivo foi aberto de forma correta
            if (file != NULL) {
                // Buscando o código do próximo registro no arquivo binário
                conta.codigo = sequenceArquivoBinario("persist/seq_contas_receber_pagar.bin");
                if(conta.codigo != 0){
                    fwrite(&conta, sizeof (Contas_Receber_Pagar), 1, file);
                    fclose(file);
                    return SUCCESS;
                }
            }
            mostrarMensagemErroArquivo();
            return ERROR;    
            break;
        case 2:
            file = fopen("persist/contas_receber_pagar.txt", "a+");
            // Verificando se o arquivo foi aberto de forma correta
            if(file != NULL){
                // Buscando o código do próximo registro no arquivo TXT
                conta.codigo = sequenceArquivoTXT("persist/seq_contas_receber_pagar.txt");
                if(conta.codigo != 0){
                    fprintf(file, "%d;", conta.codigo);
                    fprintf(file, "%s;", conta.entrada_saida);
                    fprintf(file, "%s;", conta.tipo);
                    fprintf(file, "%s;", conta.status);
                    fprintf(file, "%d;", conta.id_origem);
                    fprintf(file, "%f;", conta.valor);
                    fprintf(file, "%d-%d-%d;", conta.data.ano, conta.data.mes, conta.data.dia);
                    fprintf(file, "%d-%d-%d;\n", conta.dataVencimento.ano,conta.dataVencimento.mes,conta.dataVencimento.dia);
                    fclose(file);
                }
                return SUCCESS;
            }
            mostrarMensagemErroArquivo();
            return ERROR;
            break;
        default:
            mostrarMensagemErroTipoArquivo();
            return ERROR;
            break;
    }
}

/**
* Monta a estrutura Historico_Caixa de acordo com os parâmetros informados, sem salvar nos arquivos
* @param float valor: valor que será abatido e/ou somado ao caixa
* @param char entrada_saida[] : se é "entrada" ou "saida" de valor do caixa
* @param int id_origem: //id_reserva, id_venda, id_entrada_produtos
* @param char tipo[]: // diaria, venda, pagamento
* @return Historico_Caixa a estrutura montada
*/
Historico_Caixa montaHistoricoCaixa(float valor,char entrada_saida[], int id_origem, char tipo[]) {
    int tipoArmazenamento = opcaoArmazenamentoUsuario();
    Historico_Caixa historicoCaixa;
    if(tipoArmazenamento == 1) {
        historicoCaixa.codigo = sequenceArquivoBinario("persist/seq_historico_caixa.bin");
    } else {
        historicoCaixa.codigo = sequenceArquivoTXT("persist/seq_historico_caixa.txt");
    }
    historicoCaixa.data = gerarDataAtual();
    historicoCaixa.id_origem = id_origem;
    historicoCaixa.valor = valor;
    strcpy(historicoCaixa.entrada_saida,entrada_saida);
    strcpy(historicoCaixa.tipo,tipo);
    return historicoCaixa;
}

/**
* Monta a estrutura Contas_Receber_Pagar de acordo com os parâmetros informados e salva
* @param Pagamento pagamento: estrutura com as informações sobre o pagamento
* @param char tipo[]: // diaria, venda, pagamento
* @param char entrada_saida[] : se é "entrada" ou "saida" de valor do caixa
* @param float valorTotal
* @param int id_origem: //id_reserva, id_venda, id_entrada_produtos
* @return 1- operação feita com sucesso, 0- erro
*/
int montaESalvaContaReceberPagar(Pagamento pagamento,char tipo[], char status[], char entrada_saida[], float valorTotal, int id_origem) {
    float valor = valorTotal;
    float valor_parcela;
    int size = 1;
    if(pagamento.numeroParcelas != 0) {
        size = pagamento.numeroParcelas;
        valor_parcela = valor/pagamento.numeroParcelas;
    } else {
        valor_parcela = valor;
    }

    Contas_Receber_Pagar *contas = malloc(size*sizeof(Contas_Receber_Pagar));
    
    if(pagamento.isEntrada == 1) {
        valor = valorTotal - pagamento.valorEntrada;
    }
     
    for(int i=0;i<size;i++) {
        contas[i].data = gerarDataAtual();
        strcpy(contas[i].entrada_saida,entrada_saida);
        strcpy(contas[i].status,status);
        strcpy(contas[i].tipo,tipo);
        contas[i].valor = valor_parcela;
        contas[i].id_origem = id_origem;
        contas[i].dataVencimento.ano = pagamento.dataVencimento.dia;
        contas[i].dataVencimento.mes = pagamento.dataVencimento.mes;
        contas[i].dataVencimento.dia = pagamento.dataVencimento.ano;
        if(cadastrarContasAReceberPagar(contas[i]) == 0){
            return ERROR;
        }
    }
    return SUCCESS;
}

/**
*  Mostra ao usuário o menu de pagamento.
* @param 
* @return 
*/
int intro_pagamento() {
    int modo;
    do {
        printf("\nModo de Pagamento: ");
        printf("\n1 - DINHEIRO.");
        printf("\n2 - CARTÃO DE CRÉDITO.");
        printf("\n--> ");
        scanf("%d",&modo);
    } while (modo != 1 && modo != 2); 
    if (modo == 1) {
        pag_dinheiro();
    }else if (modo == 2) {
        pag_cartao();
    }
}



/**
* Função para realizar o pagamento em cartão de crédito.
* @param 
* @return
*/
int pag_cartao() {
    Contas_Receber_Pagar conta = {};

    printf("===============================\n");
    printf("|       CONTROLE DE CAIXA     |\n");
    printf("===============================\n");


    printf("\nDigite se o pagamento é entrada ou saída: ");
    printf("\n--> ");
    setbuf(stdin, NULL);
    fgets(conta.entrada_saida,50,stdin);
    conta.entrada_saida[strcspn(conta.entrada_saida, "\n")] = 0;

    printf("\nInsira o tipo de pagamento (Ex.: Diária, Venda, Pagamento, etc): ");
    setbuf(stdin, NULL);
    fgets(conta.tipo,50,stdin);
    conta.tipo[strcspn(conta.tipo, "\n")] = 0;

    printf("\nStatus: ");
    fgets(conta.status,40,stdin);
    conta.status[strcspn(conta.status, "\n")] = 0;

    printf("\nID: ");
    scanf("%d",&conta.id_origem);

    printf("\nDigite o valor do pagamento: ");
    scanf("%f",&conta.valor);

    printf("\nDigite a data do pagamento (dd/mm/aaaa): ");
    printf("\n");
    scanf("%d%d%d",&conta.data.dia,&conta.data.mes,&conta.data.ano);

    printf("\nDigite a data de vencimento do pagamento (dd/mm/aaaa): ");
    printf("\n");
    scanf("%d%d%d",&conta.dataVencimento.dia,&conta.dataVencimento.mes,&conta.dataVencimento.ano);


    cadastrarContasAReceberPagar(conta); 
}


/**
* Função para realizar o pagamento em dinheiro.
* @param 
* @return
*/
int pag_dinheiro() {
    Historico_Caixa pagamentos = {};
    int tipoArmazenamento = opcaoArmazenamentoUsuario();

    printf("===============================\n");
    printf("|       CONTROLE DE CAIXA     |\n");
    printf("===============================\n");

    printf("\nDigite se o pagamento é entrada ou saída: ");
    printf("\n--> ");
    setbuf(stdin, NULL);
    fgets(pagamentos.entrada_saida,50,stdin);
    pagamentos.entrada_saida[strcspn(pagamentos.entrada_saida, "\n")] = 0;

    printf("\nInsira o tipo de pagamento (Ex.: Diária, Venda, Pagamento, etc): ");
    setbuf(stdin, NULL);
    fgets(pagamentos.tipo,50,stdin);
    pagamentos.tipo[strcspn(pagamentos.tipo, "\n")] = 0;

    printf("\nID: ");
    scanf("%d",&pagamentos.id_origem);

    printf("\nDigite o valor do pagamento: ");
    scanf("%f",&pagamentos.valor);

    printf("\nDigite a data do pagamento (dd/mm/aaaa): ");
    printf("\n");
    scanf("%d%d%d", &pagamentos.data.dia, &pagamentos.data.mes, &pagamentos.data.ano);
    cadastrarHistorico(pagamentos);

    atualizarValorCaixa(pagamentos,tipoArmazenamento);
}



void verificaValidadePagamento() {
    int tipoArquivo = opcaoArmazenamentoUsuario();
    carregarCReceberPagar();
    Data atual = gerarDataAtual();

    atualizarValorCaixa2(tipoArquivo,atual);
}


/**
* Função para salvar os pagamentos.
* @param 
* @return
*/
int salvar_pagamento(Historico_Caixa pagamentos) {
    int tipoFile = opcaoArmazenamentoUsuario();
    FILE *file = NULL;

    if (tipoFile == 1) {
        //arq bin;
        file = fopen("persist/historico_caixa.bin","ab"); //abre o arq;
        if (file == NULL){
            mostrarMensagemErroArquivo();
            exit(1);
        }      
        fclose(file);

    } else if (tipoFile == 2) {
        //arq txt;
        file = fopen("persist/historico_caixa.txt","a"); //abre o arq;
        
        if (file == NULL) {
            mostrarMensagemErroArquivo();
            exit(1);
        }
        fprintf(file,"%d;", pagamentos.codigo); 
        fprintf(file,"%s;", pagamentos.entrada_saida); 
        fprintf(file,"%s;", pagamentos.tipo);  
        fprintf(file,"%d;", pagamentos.id_origem);       
        fprintf(file,"%f;", pagamentos.valor);      
        fprintf(file,"%d-%d-%d;\n", pagamentos.data.dia, pagamentos.data.mes, pagamentos.data.ano);        

        fclose(file);
    }
}

int carregar_pagamentos() {
    int tipoFile = opcaoArmazenamentoUsuario();
    FILE *file = NULL;
    int cont = 0;
    char linha[800];

    if (tipoFile == 1) {
        //arq bin;
        file = fopen("persist/historico_caixa.bin","rb"); //abre o arq;
        if (file == NULL) {
            mostrarMensagemErroArquivo();
            exit(1);
        }

        fseek(file,0, SEEK_END);
        cont = ftell(file) / sizeof(Historico_Caixa);
        rewind(file);
        pagamentos22 = malloc(cont*sizeof(Historico_Caixa));
        fread(&pagamentos22,sizeof(Historico_Caixa),cont,file);

        fclose(file);

    } else if (tipoFile == 2) {
        //arq txt;
        file = fopen("persist/historico_caixa.txt","r"); //abre o arq;
        if (file == NULL) {
            mostrarMensagemErroArquivo();
            exit(1);
        }
        while (fgets(linha,800,file) != NULL){
            cont++;
        }


        rewind(file); //volta o ponteiro para o começo do arquivo;
        pagamentos22 = malloc(cont*sizeof(Historico_Caixa));
        
        for (int i = 0;i <cont;i++) {
            fgets(linha,800,file);
            
            pagamentos22[i].codigo = atoi(strtok(linha,"\n;"));
            strcpy(pagamentos22[i].entrada_saida,strtok(NULL,"\n;"));       
            strcpy(pagamentos22[i].tipo,strtok(NULL,"\n;"));           
            pagamentos22[i].id_origem = atoi(strtok(NULL,"\n;"));         
            pagamentos22[i].valor = atof(strtok(NULL,"\n;"));
            char *data = strtok(NULL,";");
            pagamentos22[i].data.ano = atoi(strtok(data,"-"));
            pagamentos22[i].data.mes = atoi(strtok(NULL,"-"));
            pagamentos22[i].data.dia = atoi(strtok(NULL,"-"));
                                   
        }
        fclose(file);
    }
    return cont;
}



int carregarCReceberPagar() {
    int tipoFile = opcaoArmazenamentoUsuario();
    FILE *file = NULL;
    int count = 0;
    char linha[800];

    if (tipoFile == 1) {
        //arq bin;
        file = fopen("persist/contas_receber_pagar.bin","rb"); //abre o arq;
        if (file == NULL) {
            mostrarMensagemErroArquivo();
            exit(1);
        }
        rewind(file);
        conta2 = malloc(count*sizeof(Contas_Receber_Pagar));
        fread(&conta2,sizeof(Contas_Receber_Pagar),count,file);

        fclose(file);

    }else if (tipoFile == 2) {
        file = fopen("persist/contas_receber_pagar.txt", "r");

        if (file == NULL) {
            mostrarMensagemErroArquivo();
            exit(1);
        }

        while (fgets(linha,800,file)) {
            count++;
        }
        rewind(file); //volta o ponteiro para o começo do arquivo;
        conta2 = malloc(count*sizeof(Contas_Receber_Pagar));

        for (int i = 0; i < count; i++){
            fgets(linha,800,file);
            conta2[i].codigo = atoi(strtok(linha,"\n;"));
            strcpy(conta2[i].entrada_saida,strtok(NULL,"\n;"));
            strcpy(conta2[i].tipo,strtok(NULL,"\n;"));
            strcpy(conta2[i].status,strtok(NULL,"\n;"));
            conta2[i].id_origem = atoi(strtok(NULL,"\n;"));
            conta2[i].valor = atof(strtok(NULL,"\n;")); 
            char *data1 = strtok(NULL,";");
            char *data2 = strtok(NULL,";");
            conta2[i].data.dia = atoi(strtok(data1,"-"));
            conta2[i].data.mes = atoi(strtok(NULL,"-"));
            conta2[i].data.ano = atoi(strtok(NULL,"-"));
            conta2[i].dataVencimento.dia = atoi(strtok(data2,"-"));
            conta2[i].dataVencimento.mes = atoi(strtok(NULL,"-"));
            conta2[i].dataVencimento.ano = atoi(strtok(NULL,"-"));
        }
        fclose(file);
    }
    return count;
}

int mostrarSaldoCaixa() {
    int tipoFile = opcaoArmazenamentoUsuario();
    FILE *file = NULL;

    float valorTotalFloat;
    char linha[50];


    if (tipoFile == 1)
    {
        //arq bin;
        file = fopen("persist/caixa.bin","rb"); //abre o arq;
        if (file == NULL) {
            mostrarMensagemErroArquivo();
            exit(1);
        }

        rewind(file);
        fread(&valorTotalFloat,sizeof(float),1,file);
        printf("\nSaldo Total do Caixa: %f \n",valorTotalFloat);

        fclose(file);
    }else if (tipoFile == 2) {
        file = fopen("persist/caixa.txt", "r");

        if (file == NULL) {
            mostrarMensagemErroArquivo();
            exit(1);
        }

        rewind(file);
        fgets(linha, 50, file);
        valorTotalFloat = atof(strtok(linha, ";"));
        printf("\nSaldo Total do Caixa: R$ %.2f \n",valorTotalFloat);

        fclose(file);
    }   
}

int atualizarContasAReceber(Contas_Receber_Pagar *contas, int tamanho) {
    FILE *file = NULL;
    int tipoArquivo = opcaoArmazenamentoUsuario();

    switch (tipoArquivo){
    case 1:
        file = fopen("persist/contas_receber_pagar.bin", "wb");
        if(file != NULL){
            fwrite(contas,sizeof(Contas_Receber_Pagar),tamanho,file);
            fclose(file);
            return SUCCESS;
        }
        mostrarMensagemErroArquivo();
        return ERROR;
        break;
    case 2:
        file = fopen("persist/contas_receber_pagar.txt", "w");
        if(file != NULL){
            for(int i=0;i<tamanho;i++){
                fprintf(file, "%d;", contas[i].codigo);
                fprintf(file, "%s;", contas[i].entrada_saida);
                fprintf(file, "%s;", contas[i].tipo);
                fprintf(file, "%s;", contas[i].status);
                fprintf(file, "%d;", contas[i].id_origem);
                fprintf(file, "%f;", contas[i].valor);
                fprintf(file, "%d-%d-%d;", contas[i].data.ano, contas[i].data.mes, contas[i].data.dia);
                fprintf(file, "%d-%d-%d;\n", contas[i].dataVencimento.ano,contas[i].dataVencimento.mes,contas[i].dataVencimento.dia);
            }
            fclose(file);
            return SUCCESS;
        }
        mostrarMensagemErroArquivo();
        return ERROR;
        break;
    default:
        break;
    }
}

int atualizarValorCaixa2(int tipoArmazenamento, Data atual) {
    FILE *file = NULL;
    int count = carregarCReceberPagar();
    char troca[10] = "lancado";
    switch (tipoArmazenamento) {
        case 1:
            file = fopen("persist/caixa.bin", "r+b");
            if(file != NULL){
                float valorTotal = 0;
                fread(&valorTotal, sizeof (float), 1, file);
                fclose(file);
                for (int i=0;i<count;i++) {
                    if(strcmp(conta2[i].status , "aguardando_lancamento") == 0){
                        if (conta2[i].dataVencimento.dia > atual.ano) {
                            valorTotal = valorTotal + conta2[i].valor;
                        }else if (conta2[i].dataVencimento.mes > atual.mes) {
                            valorTotal = valorTotal + conta2[i].valor;
                        }else if (conta2[i].dataVencimento.ano > atual.dia) {
                            valorTotal = valorTotal + conta2[i].valor;
                        }
                        strcpy(conta2[i].status , troca);
                    }
                }
                file = fopen("persist/caixa.bin", "wb");
                fwrite(&valorTotal, sizeof (float), 1, file);
                fclose(file);
                return SUCCESS;
            }
            mostrarMensagemErroArquivo();
            return ERROR;
            break;
        case 2:
            file = fopen("persist/caixa.txt", "r+");
            if(file != NULL){
                char valorTotal[30];
                float valorTotalFloat = 0;
                fgets(valorTotal, 30, file);
                valorTotalFloat = atof(strtok(valorTotal, ";"));
                fclose(file);
                for (int i=0;i<count;i++) {
                    if(strcmp(conta2[i].status , "aguardando_lancamento") == 0){
                        if (conta2[i].dataVencimento.ano < atual.ano) {
                            valorTotalFloat += conta2[i].valor;
                            strcpy(conta2[i].status , troca);
                        }else if (conta2[i].dataVencimento.mes < atual.mes) {
                            valorTotalFloat += conta2[i].valor;
                            strcpy(conta2[i].status , troca);
                        }else if (conta2[i].dataVencimento.dia < atual.dia) {
                            valorTotalFloat += conta2[i].valor;
                            strcpy(conta2[i].status , troca);
                        }
                    }
                }
                atualizarContasAReceber(conta2,count);
                file = fopen("persist/caixa.txt", "w");
                fprintf(file, "%f;", valorTotalFloat);
                fclose(file);
                return SUCCESS;
            }
            mostrarMensagemErroArquivo();
            return ERROR;
            break;
        default:
            mostrarMensagemErroTipoArquivo();
            return ERROR;
            break;
    }
}


int mostrarHistoricoLancRetirada() { 
    int count  = carregarCReceberPagar();
    int cont = carregar_pagamentos();
    printf("===============================\n");
    printf("|      HISTÓRICO DE CAIXA     |\n");
    printf("===============================\n");
   
    for (int i = 0; i < count; i++){
        if (strcmp(conta2[i].status, "lancado") == 0) {
            printf("\nEntrada/Saída: %s     |   Valor = %.2f",conta2[i].entrada_saida,conta2[i].valor); 
        }     
    }

    for (int i = 0; i < cont-1; i++){
       printf("\nEntrada/Saída: %s     |   Valor = %.2f", pagamentos22[i].entrada_saida, pagamentos22[i].valor);
   }
    printf("\n");  
}