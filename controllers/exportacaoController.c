/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#include "stdlib.h"
#include "stdio.h"
#include "../models/exportacaoModel.h"
#include "../models/reservaModel.h"
#include "../models/variadics.h"
#include "../models/usuarioModel.h"
#include "../models/hotelModel.h"
#include "../models/acomodacaoModel.h"
#include "../models/hospedeModel.h"
#include "../models/categoriaModel.h"
#include "../models/produtosModel.h"
#include "../models/fornecedorModel.h"

// funções de exportação
void exportarTabelaUsuario(char* path) {
    int count = qtdeRegistrosUsuario();
    Usuario* usuarios = listarUsuarios();

    if (usuarios != NULL) {
        createTable(path, "usuario");

        for (int i=0; i<count; i++) {
            registerToXML(path, "usuario", "codigo=%d;nome=%s;user=%s;senha=%s;permissao=%d;",
                                            usuarios[i].usuCodigo,
                                            usuarios[i].usuNome,
                                            usuarios[i].usuUsuario,
                                            usuarios[i].usuSenha,
                                            usuarios[i].usuPermissao);
        }

        free(usuarios);
    }
}

void exportarTabelaHotel(char* path) {
    int count = qtdeRegistrosHotel();
    Hotel* hoteis = carregar_hotel();

    if (hoteis != NULL) {
        createTable(path, "hotel");

        for (int i=0; i<count; i++) {
            registerToXML(path, "hotel", "codigo=%d;nome_fantasia=%s;razao_social=%s;inscricao_estadual=%d;cnpj=%d;telefone=%d;email=%s;nome_responsavel=%s;telefone_responsavel=%d;horario_checkIn=%s;horario_checkOut=%s;margem_lucro=%d;end_rua=%s;end_bairro=%s;end_cidade=%s;end_estado=%s;end_cep=%d;end_complemento=%s;end_numero=%d;",
                                        hoteis[i].codigo, hoteis[i].nome_fantasia, hoteis[i].razao_social, hoteis[i].inscricao_estadual, hoteis[i].cnpj, hoteis[i].telefone, hoteis[i].email, hoteis[i].nome_responsavel, hoteis[i].telefone_responsavel, hoteis[i].horario_checkIn, hoteis[i].horario_checkOut, hoteis[i].margem_lucro,
                                        hoteis[i].endereco.endRua, hoteis[i].endereco.endBairro, hoteis[i].endereco.endCidade, hoteis[i].endereco.endEstado, hoteis[i].endereco.endCep, hoteis[i].endereco.endComplemento, hoteis[i].endereco.endNumero);
        }

        free(hoteis);
    }
}

void exportarTabelaAcomodacao(char* path) {
    int count = contarAcomodacoes();
    Acomodacao* acomodacoes = carregarAcomodacao(count);

    if (acomodacoes != NULL) {
        createTable(path, "acomodacao");

        for (int i=0; i<count; i++) {
            registerToXML(path, "acomodacao", "codigo=%d;descricao=%s;caracteristicas=%s;categ_codigo=%d;categ_descricao=%s;categ_valorDiaria=%f;categ_CapacidadeDePessoas=%d;",
                                                acomodacoes[i].acoCodigo, acomodacoes[i].acoDescricao, acomodacoes[i].acoCaracteristicas, acomodacoes[i].acoCategoria.catCodigo, acomodacoes[i].acoCategoria.catDescricao, acomodacoes[i].acoCategoria.catValorDiaria, acomodacoes[i].acoCategoria.catQuantidadePessoas);
        }

        free(acomodacoes);
    }
}

void exportarTabelaHospedes(char* path) {
    int count;
    Hospede* hospedes = loadHospedeList(&count);

    if (hospedes != NULL) {
        createTable(path, "hospede");

        for (int i=0; i<count; i++) {
            registerToXML(path, "hospede", "codigo=%d;nome=%s;cpf=%s;telefone=%s;email=%s;sexo=%c;estado_civil=%s;data_de_nascimento=%s;end_rua=%s;end_bairro=%s;end_cidade=%s;end_estado=%s;end_cep=%d;end_complemento=%s;end_numero=%d;",
                                            hospedes[i].hospCodigo, hospedes[i].hospNome, hospedes[i].hospCpf, hospedes[i].hospTelefone, hospedes[i].hospEmail, hospedes[i].hospSexo, hospedes[i].hospEstadoCivil, hospedes[i].hospDataNascimento,
                                            hospedes[i].hospEndereco.endRua, hospedes[i].hospEndereco.endBairro, hospedes[i].hospEndereco.endCidade, hospedes[i].hospEndereco.endEstado, hospedes[i].hospEndereco.endCep, hospedes[i].hospEndereco.endComplemento, hospedes[i].hospEndereco.endNumero);
        }

        free(hospedes);
    }
}

void exportarTabelaCategoria(char* path) {
    int count = contarCategorias();
    Categoria* categorias = carregarCategorias(count);

    if (categorias != NULL) {
        createTable(path, "categoria");

        for (int i=0; i<count; i++) {
            registerToXML(path, "categoria", "codigo=%d;descricao=%s;valor_diaria=%f;capacidade_pessoas=%d;",
                                            categorias[i].catCodigo, categorias[i].catDescricao, categorias[i].catValorDiaria, categorias[i].catQuantidadePessoas);
        }

        free(categorias);
    }
}

void exportarTabelaProdutos(char* path) {
    int count = qtdeRegistrosProduto();
    Prod_Disp* produtos = carregar_produtos();

    if (produtos != NULL) {
        createTable(path, "produto");

        for (int i=0; i<count; i++) {
            registerToXML(path, "produto", "codigo=%d;descricao=%s;estoque=%d;estoque_minimo=%d;preço_de_custo=%f;preço_de_venda=%f;",
                                            produtos[i].codigo, produtos[i].descricao, produtos[i].estoque, produtos[i].min_estoque, produtos[i].preco_custo, produtos[i].preco_venda);
        }

        free(produtos);
    }
}

void exportatTabelaFornecedores(char* path) {
    int count = 0;
    Fornecedor* fornecedores = loadFornecedorList(&count);

    if (fornecedores != NULL) {
        createTable(path, "fornecedor");

        for (int i=0; i<count; i++) {
            registerToXML(path, "fornecedor", "codigo=%d;nome_fantasia=%s;razao_social=%s;inscricao_estadual=%d;cnpj=%s;telefone=%d;email=%s;end_rua=%s;end_bairro=%s;end_cidade=%s;end_estado=%s;end_cep=%d;end_complemento=%s;end_numero=%d;",
                                                fornecedores[i].forCodigo, fornecedores[i].forNomeFantasia, fornecedores[i].forRazaoSocial, fornecedores[i].forInscricaoEstadual, fornecedores[i].forCnpj, fornecedores[i].forTelefone, fornecedores[i].forEmail,
                                                fornecedores[i].forEndereco.endRua, fornecedores[i].forEndereco.endBairro, fornecedores[i].forEndereco.endCidade, fornecedores[i].forEndereco.endEstado, fornecedores[i].forEndereco.endCep, fornecedores[i].forEndereco.endComplemento, fornecedores[i].forEndereco.endNumero);
        }

        free(fornecedores);
    }
}

void exportarTabelaReserva(char* path) {
    int count = 0;
    Reserva* reservas = carregarReservas(&count);

    if (reservas != NULL) {
        createTable(path, "reserva");

        for (int i=0; i<count; i++) {
            registerToXML(path, "reserva", "codigo=%d;hospede_id=%d;acomodacao_id=%d;status_pagamento=%d;data_inicio_dia=%d;data_inicio_mes=%d;data_inicio_ano=%d;data_fim_dia=%d;data_fim_mes=%d;data_fim_ano=%d;data_chegada_dia=%d;data_chegada_mes=%d;data_chegada_ano=%d;data_chegada_hora=%d;data_chegada_minuto=%d;data_chegada_segundo=%d;data_saida_dia=%d;data_saida_mes=%d;data_saida_ano=%d;data_saida_hora=%d;data_saida_minuto=%d;data_saida_segundo=%d;",
                                            reservas[i].resCodigo, reservas[i].resHospedeId, reservas[i].resAcomodacaoId, reservas[i].resStatus_pagamento,
                                            reservas[i].resDataInicio.dia, reservas[i].resDataInicio.mes, reservas[i].resDataInicio.ano,
                                            reservas[i].resDataFim.dia, reservas[i].resDataFim.mes, reservas[i].resDataFim.ano,
                                            reservas[i].resDataTime_chegada.dia, reservas[i].resDataTime_chegada.mes, reservas[i].resDataTime_chegada.ano, reservas[i].resDataTime_chegada.hora, reservas[i].resDataTime_chegada.minuto, reservas[i].resDataTime_chegada.segundo,
                                            reservas[i].resDataTime_saida.dia, reservas[i].resDataTime_saida.mes, reservas[i].resDataTime_saida.ano, reservas[i].resDataTime_saida.hora, reservas[i].resDataTime_saida.minuto, reservas[i].resDataTime_saida.segundo);
        }

        free(reservas);
    }
}

// funções de importação
void importarTabelaUsuario(char* path) {
    int count = registersInTable(path, "usuario");
    Usuario* usuarios = malloc(sizeof(Usuario) * count);

    for (int i=0; i<count; i++) {
        readFromXML(path, "usuario", i, "codigo=%d;nome=%s;user=%s;senha=%s;permissao=%d;",
                                        &usuarios[i].usuCodigo,
                                        usuarios[i].usuNome,
                                        usuarios[i].usuUsuario,
                                        usuarios[i].usuSenha,
                                        &usuarios[i].usuPermissao);
    }

    FILE* file = fopen("persist/usuario.txt", "w");
    fclose(file);
    //TODO: salvar os usuarios direito

    free(usuarios);
}

void importarTabelaHotel(char* path) {
    int count = qtdeRegistrosHotel();
    Hotel* hoteis = carregar_hotel();

    if (hoteis != NULL) {
        createTable(path, "hotel");

        for (int i=0; i<count; i++) {
            registerToXML(path, "hotel", "codigo=%d;nome_fantasia=%s;razao_social=%s;inscricao_estadual=%d;cnpj=%d;telefone=%d;email=%s;nome_responsavel=%s;telefone_responsavel=%d;horario_checkIn=%s;horario_checkOut=%s;margem_lucro=%d;end_rua=%s;end_bairro=%s;end_cidade=%s;end_estado=%s;end_cep=%d;end_complemento=%s;end_numero=%d;",
                                        hoteis[i].codigo, hoteis[i].nome_fantasia, hoteis[i].razao_social, hoteis[i].inscricao_estadual, hoteis[i].cnpj, hoteis[i].telefone, hoteis[i].email, hoteis[i].nome_responsavel, hoteis[i].telefone_responsavel, hoteis[i].horario_checkIn, hoteis[i].horario_checkOut, hoteis[i].margem_lucro,
                                        hoteis[i].endereco.endRua, hoteis[i].endereco.endBairro, hoteis[i].endereco.endCidade, hoteis[i].endereco.endEstado, hoteis[i].endereco.endCep, hoteis[i].endereco.endComplemento, hoteis[i].endereco.endNumero);
        }
        free(hoteis);
    }
}

void importarTabelaAcomodacao(char* path) {
    int count = registersInTable(path, "acomodacao");
    Acomodacao* acomodacoes = malloc(sizeof(Acomodacao) * count);


    for (int i=0; i<count; i++) {
        readFromXML(path, "acomodacao", i, "codigo=%d;descricao=%s;caracteristicas=%s;categ_codigo=%d;categ_descricao=%s;categ_valorDiaria=%f;categ_CapacidadeDePessoas=%d;",
                                            &acomodacoes[i].acoCodigo, acomodacoes[i].acoDescricao, acomodacoes[i].acoCaracteristicas, &acomodacoes[i].acoCategoria.catCodigo, acomodacoes[i].acoCategoria.catDescricao, &acomodacoes[i].acoCategoria.catValorDiaria, &acomodacoes[i].acoCategoria.catQuantidadePessoas);
    }

    salvarAcomodacao(acomodacoes, count);
    free(acomodacoes);
}

void importarTabelaHospedes(char* path) {
    int count;
    Hospede* hospedes = loadHospedeList(&count);

    if (hospedes != NULL) {
        createTable(path, "hospede");

        for (int i=0; i<count; i++) {
            registerToXML(path, "hospede", "codigo=%d;nome=%s;cpf=%s;telefone=%s;email=%s;sexo=%c;estado_civil=%c;data_de_nascimento=%s;end_rua=%s;end_bairro=%s;end_cidade=%s;end_estado=%s;end_cep=%d;end_complemento=%s;end_numero=%d;",
                                            hospedes[i].hospCodigo, hospedes[i].hospNome, hospedes[i].hospCpf, hospedes[i].hospTelefone, hospedes[i].hospEmail, hospedes[i].hospSexo, hospedes[i].hospEstadoCivil, hospedes[i].hospDataNascimento,
                                            hospedes[i].hospEndereco.endRua, hospedes[i].hospEndereco.endBairro, hospedes[i].hospEndereco.endCidade, hospedes[i].hospEndereco.endEstado, hospedes[i].hospEndereco.endCep, hospedes[i].hospEndereco.endComplemento, hospedes[i].hospEndereco.endNumero);
        }
        free(hospedes);
    }
}

void importarTabelaCategoria(char* path) {
    int count = registersInTable(path, "categoria");
    Categoria* categorias = malloc(sizeof(Categoria) * count);

    for (int i=0; i<count; i++) {
        readFromXML(path, "categoria", i, "codigo=%d;descricao=%s;valor_diaria=%f;capacidade_pessoas=%d;",
                                        &categorias[i].catCodigo, categorias[i].catDescricao, &categorias[i].catValorDiaria, &categorias[i].catQuantidadePessoas);
    }

    salvarCategorias(categorias, count);

    free(categorias);
}

void importarTabelaProdutos(char* path) {
    int count = qtdeRegistrosProduto();
    Prod_Disp* produtos = carregar_produtos();

    if (produtos != NULL) {
        createTable(path, "produto");

        for (int i=0; i<count; i++) {
            registerToXML(path, "produto", "codigo=%d;descricao=%s;estoque=%d;estoque_minimo=%d;preço_de_custo=%f;preço_de_venda=%f;",
                                            produtos[i].codigo, produtos[i].descricao, produtos[i].estoque, produtos[i].min_estoque, produtos[i].preco_custo, produtos[i].preco_venda);
        }

        free(produtos);
    }
}

void importarTabelaFornecedores(char* path) {
    int count = 0;
    Fornecedor* fornecedores = loadFornecedorList(&count);

    if (fornecedores != NULL) {
        createTable(path, "fornecedor");

        for (int i=0; i<count; i++) {
            registerToXML(path, "fornecedor", "codigo=%d;nome_fantasia=%s;razao_social=%s;inscricao_estadual=%d;cnpj=%s;telefone=%d;email=%s;end_rua=%s;end_bairro=%s;end_cidade=%s;end_estado=%s;end_cep=%d;end_complemento=%s;end_numero=%d;",
                                                fornecedores[i].forCodigo, fornecedores[i].forNomeFantasia, fornecedores[i].forRazaoSocial, fornecedores[i].forInscricaoEstadual, fornecedores[i].forCnpj, fornecedores[i].forTelefone, fornecedores[i].forEmail,
                                                fornecedores[i].forEndereco.endRua, fornecedores[i].forEndereco.endBairro, fornecedores[i].forEndereco.endCidade, fornecedores[i].forEndereco.endEstado, fornecedores[i].forEndereco.endCep, fornecedores[i].forEndereco.endComplemento, fornecedores[i].forEndereco.endNumero);
        }
        free(fornecedores);
    }
}

void importarTabelaReserva(char* path) {
    int count = registersInTable(path, "reserva");
    Reserva* reservas = malloc(sizeof(Reserva) * count);
    for (int i=0; i<count; i++) {
        readFromXML(path, "reserva", i, "codigo=%d;hospede_id=%d;acomodacao_id=%d;status_pagamento=%d;data_inicio_dia=%d;data_inicio_mes=%d;data_inicio_ano=%d;data_fim_dia=%d;data_fim_mes=%d;data_fim_ano=%d;data_chegada_dia=%d;data_chegada_mes=%d;data_chegada_ano=%d;data_chegada_hora=%d;data_chegada_minuto=%d;data_chegada_segundo=%d;data_saida_dia=%d;data_saida_mes=%d;data_saida_ano=%d;data_saida_hora=%d;data_saida_minuto=%d;data_saida_segundo=%d;",
                                        &reservas[i].resCodigo, &reservas[i].resHospedeId, &reservas[i].resAcomodacaoId, &reservas[i].resStatus_pagamento,
                                        &reservas[i].resDataInicio.dia, &reservas[i].resDataInicio.mes, &reservas[i].resDataInicio.ano,
                                        &reservas[i].resDataFim.dia, &reservas[i].resDataFim.mes, &reservas[i].resDataFim.ano,
                                        &reservas[i].resDataTime_chegada.dia, &reservas[i].resDataTime_chegada.mes, &reservas[i].resDataTime_chegada.ano, &reservas[i].resDataTime_chegada.hora, &reservas[i].resDataTime_chegada.minuto, &reservas[i].resDataTime_chegada.segundo,
                                        &reservas[i].resDataTime_saida.dia, &reservas[i].resDataTime_saida.mes, &reservas[i].resDataTime_saida.ano, &reservas[i].resDataTime_saida.hora, &reservas[i].resDataTime_saida.minuto, &reservas[i].resDataTime_saida.segundo);
    }

    salvarReservas(reservas, count);
    free(reservas);
}

void exportarDados(char* path) {
    startXML(path);
    int escolha = 0;

    printf("Deseja exportar dados de usuario? 1-SIM; 2-NÃO\n");
    scanf("%d%*c", &escolha);
    if (escolha == 1) {
        exportarTabelaUsuario(path);
    }
    printf("Deseja exportar dados de hotel? 1-SIM; 2-NÃO\n");
    scanf("%d%*c", &escolha);
    if (escolha == 1) {
        exportarTabelaHotel(path);
    }
    printf("Deseja exportar dados de Acomodacao? 1-SIM; 2-NÃO\n");
    scanf("%d%*c", &escolha);
    if (escolha == 1) {
        exportarTabelaAcomodacao(path);
    }
    printf("Deseja exportar dados de Hóspedes? 1-SIM; 2-NÃO\n");
    scanf("%d%*c", &escolha);
    if (escolha == 1) {
        exportarTabelaHospedes(path);
    }
    printf("Deseja exportar dados de categoria? 1-SIM; 2-NÃO\n");
    scanf("%d%*c", &escolha);
    if (escolha == 1) {
        exportarTabelaCategoria(path);
    }
    printf("Deseja exportar dados de produto? 1-SIM; 2-NÃO\n");
    scanf("%d%*c", &escolha);
    if (escolha == 1) {
        exportarTabelaProdutos(path);
    }
    printf("Deseja exportar dados de fornecedor? 1-SIM; 2-NÃO\n");
    scanf("%d%*c", &escolha);
    if (escolha == 1) {
        exportatTabelaFornecedores(path);
    }
    printf("Deseja exportar dados de reserva? 1-SIM; 2-NÃO\n");
    scanf("%d%*c", &escolha);
    if (escolha == 1) {
        exportarTabelaReserva(path);
    }
}

void importarDados(char* path) {
    int escolha = 0;

    printf("Deseja importar dados de categoria? 1-SIM; 2-NÃO\n");
    scanf("%d%*c", &escolha);
    if (escolha == 1) {
        importarTabelaCategoria(path);
    }
    printf("Deseja importar dados de acomodacao? 1-SIM; 2-NÃO\n");
    scanf("%d%*c", &escolha);
    if (escolha == 1) {
        importarTabelaAcomodacao(path);
    }
    printf("Deseja importar dados de reserva? 1-SIM; 2-NÃO\n");
    scanf("%d%*c", &escolha);
    if (escolha == 1) {
        importarTabelaReserva(path);
    }
}
