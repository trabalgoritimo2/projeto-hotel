/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "../structs/moduloCadastroStructs.h"
#include "../models/configuracaoModel.h"
#include "../models/funcoesModel.h"
#include "../models/produtosModel.h"
#include "../models/controleCaixaModel.h"
#include "../models/hotelModel.h"

int i;
#define SUCCESS 1;
#define ERROR 0;


/**
 * Tela de cadastrar produto
 * @param hospede Objeto para salvar
 * @return int  valor para tratamento de erro
 */
void produtos() {
    Prod_Disp produto;
    printf("\nDescrição do produto: ");
    setbuf(stdin, NULL);
    fgets(produto.descricao,40,stdin);
    produto.descricao[strcspn(produto.descricao, "\n")] = 0;
    setbuf(stdin, NULL);
    //estoque
    printf("\nEstoque: ");
    scanf("%d",&produto.estoque);
    //estoque min
    printf("\nEstoque mínimo: ");
    scanf("%d",&produto.min_estoque);
    //preço de custo;
    printf("\nPreço de custo: ");
    scanf("%f",&produto.preco_custo);
    //preço de venda;
    printf("\nPreço de venda: ");
    scanf("%f",&produto.preco_venda);
    salvar_produtos(produto);
}

/**
 * Tela de cadastrar produto
 * @param hospede Objeto para salvar
 * @return int  valor para tratamento de erro
 */
int cadastrarProduto() {
    char escolha = 'S';
    //início da parte relacionada a adicionar um produto;
    while (escolha == 'S' || escolha == 's') {
        produtos();
        setbuf(stdin,NULL);
        printf("\nDeseja adicionar um produto (S/N)? ");
        scanf("%c%*c",&escolha);           
    }
}

/**
 * Realiza a exclusão de um produto;
 * @param 
 * @return int  //retorna 1 se o cod do produto NAO existe ainda; 0- se não existe
 */
int deletarProduto() {
    char escolha; //variavel para a escolha do usuario de apg;
    int codigo;
    int tipoArquivo = opcaoArmazenamentoUsuario(); 
    FILE *file = NULL;
    FILE *newFile = NULL;
    int qtdeInicial = qtdeRegistrosProduto(tipoArquivo);
    int qtdeFinal;
    printf("\nDeseja apagar/excluir um produto (S/N)? ");
    scanf("%c%*c",&escolha);
    while (escolha == 'S' || escolha == 's') {
        printf("\nInsira o código do produto: ");
        scanf("%d",&codigo);
        Prod_Disp produto = buscarProdutoPorCodigo(codigo);
        if (produto.codigo != -1) {
            switch (tipoArquivo) {
                case 1:
                    file = fopen("persist/produto.bin", "rb+");
                    newFile = fopen("persist/tempProduto.bin", "wb+");
                    rewind(file);
                    if (file != NULL) {
                        Prod_Disp tempProduto;
                        // Percorrendo cada linha procurando o produto
                        while (fread(&tempProduto, sizeof (Prod_Disp), 1, file)) {
                            if (!(tempProduto.codigo == produto.codigo)) {
                                fwrite(&tempProduto, sizeof (Prod_Disp), 1, newFile);
                            }
                        }
                    }
                    // Fechando os arquivos
                    fclose(file);
                    fclose(newFile);
                    // Apagando o arquivo original          
                    remove("persist/produto.bin");
                    // Renomeando o arquivo
                    rename("persist/tempProduto.bin", "persist/produto.bin");
                    break;
                case 2:
                    file = fopen("persist/produto.txt", "r+");
                    newFile = fopen("persist/tempProduto.txt", "w+");
                    if (file != NULL && newFile != NULL) {
                        char produtoString[500];
                        // Percorrendo cada linha procurando o usuário
                        while ((fgets(produtoString, 500, file)) != NULL) {
                            char aux[500];
                            strcpy(aux, produtoString);
                            char *token = strtok(produtoString, ";");
                            Prod_Disp tempProduto;
                            for (int i = 0; token != NULL; i++) {
                                switch (i) {
                                     case 0:
                                        tempProduto.codigo = atoi(token);
                                        break;
                                    case 1:
                                        strcpy(tempProduto.descricao, token);
                                        break;
                                    case 2:
                                        tempProduto.estoque = atoi(token);
                                        break;
                                    case 3:
                                        tempProduto.min_estoque = atoi(token);
                                        break;
                                    case 4:
                                        tempProduto.preco_custo = atoi(token);
                                        break;
                                    case 5:
                                        tempProduto.preco_venda = atoi(token);
                                        break;
                                    default:
                                        break;
                                }
                                token = strtok(NULL, ";");
                            }

                            if (tempProduto.codigo == produto.codigo) {
                                //  Se os dados forem excluídos, não copie para o novo arquivo
                                continue;
                            } else {
                                fputs(aux, newFile);
                            }
                        }
                    }
                    // Fechando os arquivos
                    fclose(file);
                    fclose(newFile);
                    // Apagando o arquivo original             
                    remove("persist/produto.txt");
                    // Renomeando o arquivo
                    rename("persist/tempProduto.txt", "persist/produto.txt");
                    break;
            }
            qtdeFinal = qtdeRegistrosProduto(tipoArquivo);
            // verificando se realmente ocorreu a exclusão
            if (qtdeInicial > qtdeFinal) {
                return SUCCESS;
            }
            return ERROR;       

        } else {
            printf("Produto não encontrado! \n");
        }
        setbuf(stdin, NULL);
        printf("\nDeseja apagar/excluir um produto (S/N)? ");
        scanf("%c%*c",&escolha);
    }   
    
}

/**
 * Salva o produto em arquivo (txt/bin)
 * @param 
 * @return 
 */
int salvar_produtos(Prod_Disp produto) {
    FILE *file = NULL;
    int tipoArquivo = opcaoArmazenamentoUsuario();
    switch(tipoArquivo){
        case 1:
            file = fopen("persist/produto.bin", "a+b");
            // Verificando se o arquivo foi aberto de forma correta
            if (file != NULL) {
                // Buscando o código do próximo registro no arquivo binário
                produto.codigo = sequenceArquivoBinario("persist/seq_produto.bin");
                if(produto.codigo != 0){
                    fwrite(&produto, sizeof (Prod_Disp), 1, file);
                    fclose(file);
                    return SUCCESS;
                }
            }
            mostrarMensagemErroArquivo();
            return ERROR;
            break;
        case 2:
            file = fopen("persist/produto.txt", "a+");
            // Verificando se o arquivo foi aberto de forma correta
            if(file != NULL) {
                // Buscando o código do próximo registro no arquivo TXT
                produto.codigo = sequenceArquivoTXT("persist/seq_produto.txt");
                if(produto.codigo != 0){
                        fprintf(file, "%d;", produto.codigo);
                        fprintf(file, "%s;", produto.descricao);
                        fprintf(file, "%d;", produto.estoque);
                        fprintf(file, "%d;", produto.min_estoque);
                        fprintf(file, "%f;", produto.preco_custo);
                        fprintf(file, "%f;\n", produto.preco_venda);
                        fclose(file);
                        return SUCCESS;
                    }
                }
                mostrarMensagemErroArquivo();
                return ERROR;
            
            break;
        default:
            mostrarMensagemErroTipoArquivo();
            return ERROR;
            break;
    }
}

/**
 * Busca todos os produtos cadastrados no sistema
 * @param 
 * @return 
 */
Prod_Disp *carregar_produtos() {
    int tipoArquivo = opcaoArmazenamentoUsuario();
    FILE *file = NULL;
    Prod_Disp *produtos;
    int size = qtdeRegistrosProduto(tipoArquivo);
    //    List to be returned
    produtos = (Prod_Disp *) malloc(sizeof (Prod_Disp) * size);
    Prod_Disp tempProduto;
    switch (tipoArquivo) {
        case 1:
            file = fopen("persist/produto.bin", "rb+");
            if (file != NULL) {
                int index = 0;
                while (fread(&tempProduto, sizeof (Prod_Disp), 1, file)) {
                    produtos[index] = tempProduto;
                    index++;
                }
                fclose(file);
                return produtos;
            }
            break;
        case 2:
            file = fopen("persist/produto.txt", "r+");
            if (file != NULL) {
                int index = 0;
                char produtoString[500];
                while (fgets(produtoString, 500, file) != NULL) {
                    char *token = strtok(produtoString, ";");
                    for (int i = 0; token != NULL; i++) {
                        switch (i) {
                            case 0:
                                tempProduto.codigo = atoi(token);
                                break;
                            case 1:
                                strcpy(tempProduto.descricao, token);
                                break;
                            case 2:
                                tempProduto.estoque = atoi(token);
                                break;
                            case 3:
                                tempProduto.min_estoque = atoi(token);
                                break;
                            case 4:
                                tempProduto.preco_custo = atof(token);
                                break;
                            case 5:
                                tempProduto.preco_venda = atof(token);
                                break;
                            default:
                                break;
                        }

                        token = strtok(NULL, ";");
                    }

                    produtos[index] = tempProduto;
                    index++;
                }
                fclose(file);
                return produtos;
            }
            break;
    }

    if (file == NULL) {
        mostrarMensagemErroArquivo();
    }
    return NULL;
}

/**
* Busca o produto por codigo
* @param char codigo: codigo do produto
* @return produto encontrado com o código desejado
*/
Prod_Disp buscarProdutoPorCodigo(int codigo) {
    int tipoArquivo = opcaoArmazenamentoUsuario();
    FILE *file = NULL;
    Prod_Disp produto;
    switch (tipoArquivo) {
        case 1:
            file = fopen("persist/produto.bin", "rb+");
            // Verificando se o arquivo foi aberto de forma correta
            if (file != NULL) {
                // Percorrendo cada linha procurando pelo código do produto
                while (fread(&produto, sizeof (Prod_Disp), 1, file)) {
                    if (produto.codigo == codigo) {
                        break;
                    }
                }
                // Fechando o arquivo
                fclose(file);
                // Se id for encontrado, então retorna o produto
                if (produto.codigo == codigo) {
                    return produto;
                }
                // Retorna produto vazio com id == -1
                Prod_Disp produtoVazio;
                produto.codigo = -1;
                return produtoVazio;
            }
            break;
        case 2:
            file = fopen("persist/produto.txt", "r+");
            //verificando se o arquivo foi aberto de forma correta
            if (file != NULL) {
                char produtoString[600];
                // Percorrendo cada linha procurando código do usuário
                while (fgets(produtoString, 600, file) != NULL) {
                    char *token = strtok(produtoString, ";");
                    for (int i = 0; token != NULL; i++) {
                        switch (i) {
                            case 0:
                                produto.codigo = atoi(token);
                                break;
                            case 1:
                                strcpy(produto.descricao, token);
                                break;
                            case 2:
                                produto.estoque = atoi(token);
                                break;
                            case 3:
                                produto.min_estoque = atoi(token);
                                break;
                            case 4:
                                produto.preco_custo = atoi(token);
                                break;
                            case 5:
                                produto.preco_venda = atoi(token);
                                break;
                        }
                        token = strtok(NULL, ";");
                    }
                    if (produto.codigo == codigo) {
                        break;
                    }
                }
                fclose(file);

                // Se id for encontrado, então retorna o produto
                if (produto.codigo == codigo) {
                    return produto;
                } 
                // Retorna produto vazio com id == -1
                Prod_Disp produtoVazio;
                produtoVazio.codigo = -1;
                return produtoVazio;
            }
            break;
        default:
            mostrarMensagemErroTipoArquivo();
            break;
    }
    if (file == NULL) {
        mostrarMensagemErroArquivo();
    }
}


/**
* Atualiza os dados do produto
* @param Prod_Disp produto: produto a ser atualizado
* @return  1- atualizado com sucesso 0- erro ao atualizar
*/
int atualizarProduto (Prod_Disp produto) {
    int tipoArquivo = opcaoArmazenamentoUsuario();
    FILE *file = NULL;
    FILE *newFile = NULL;
    switch (tipoArquivo) {
        case 1:
            file = fopen("persist/produto.bin", "rb+");
            if (file != NULL) {
                Prod_Disp tempProduto;
                while (fread(&tempProduto, sizeof (Prod_Disp), 1, file)) {
                    if (tempProduto.codigo == produto.codigo) {
                        fseek(file, -(long) sizeof (Prod_Disp), 1);
                        fwrite(&produto, sizeof (Prod_Disp), 1, file);
                    }
                }
            }
            // Fechando o arquivo
            fclose(file);
            return SUCCESS;
            break;
        case 2:
            file = fopen("persist/produto.txt", "r+");
            newFile = fopen("persist/tempProduto.txt", "w+");
                if (file != NULL && newFile != NULL) {
                    char produtoString[500];
                    while ((fgets(produtoString, 500, file)) != NULL) {
                        char aux[500];
                        strcpy(aux, produtoString);
                        char *token = strtok(produtoString, ";");
                        Prod_Disp tempProduto;
                        for (int i = 0; token != NULL; i++) {
                            switch (i) {
                                case 0:
                                    tempProduto.codigo = atoi(token);
                                    break;
                                case 1:
                                    strcpy(tempProduto.descricao, token);
                                    break;
                                case 2:
                                    tempProduto.estoque = atoi(token);
                                    break;
                                case 3:
                                    tempProduto.min_estoque = atoi(token);
                                    break;
                                case 4:
                                    tempProduto.preco_custo = atoi(token);
                                    break;
                                case 5:
                                    tempProduto.preco_venda = atoi(token);
                                    break;
                            }
                            token = strtok(NULL, ";");
                        }
                        if (tempProduto.codigo == produto.codigo) {
                            fprintf(newFile, "%d;", produto.codigo);
                            fprintf(newFile, "%s;", produto.descricao);
                            fprintf(newFile, "%d;", produto.estoque);
                            fprintf(newFile, "%d;", produto.min_estoque);
                            fprintf(newFile, "%f;", produto.preco_custo);
                            fprintf(newFile, "%f;\n", produto.preco_venda);
                        } else {
                            fputs(aux, newFile);
                        }    
                    }
                }

            // Fechando os arquivos
            fclose(file);
            fclose(newFile);
            //Apagando o arquivo original
            remove("persist/produto.txt");
            //Renomeando o arquivo
            rename("persist/tempProduto.txt", "persist/produto.txt");
            break;
            return SUCCESS;
        }
}

/**
* Obtém a quantidade de registros gravados nos arquivos de produto
* @param
* @return int: quantidade de registros 
*/
int qtdeRegistrosProduto() {
    int tipoArquivo = opcaoArmazenamentoUsuario();
    printf("%d", tipoArquivo);
    FILE *file = NULL;
    int size = 0;
    if (tipoArquivo == 1) {
        file = fopen("persist/produto.bin", "rb+");
        if (file != NULL) {
            Prod_Disp produto;
            while (fread(&produto, sizeof (Prod_Disp), 1, file)) {
                size++;
            }
            fclose(file);
        }
    } else if (tipoArquivo == 2) {
        file = fopen("persist/produto.txt", "r+");

        if (file != NULL) {
            char produtoString[500];
            while (fgets(produtoString, 500, file) != NULL) {
                size++;
            }
            fclose(file);
        }
    }
    return size;
}

/**
 * Tela de listagem dos produtos cadastrados no sistema
 * @param 
 * @return 
 */
void listarProdutos() {
    int qtdeProdutos = qtdeRegistrosProduto();
    Prod_Disp *produtos;
    produtos = carregar_produtos();
    if (qtdeProdutos != 0) {
        printf("\n===================================\n");
        printf("|       LISTAGEM DOS PRODUTOS     |\n");
        printf("====================================\n");
        for (i = 0; i <qtdeProdutos; i++) {
            printf("\nProduto %d: ",i+1);
            printf("\nCódigo: %d",produtos[i].codigo);
            printf("\nDescrição: %s",produtos[i].descricao);
            printf("\nEstoque: %d",produtos[i].estoque);
            printf("\nEstoque mínimo: %d",produtos[i].min_estoque);
            printf("\nPreço de Custo: R$%.2f",produtos[i].preco_custo);
            printf("\nPreço de Venda: R$%.2f",produtos[i].preco_venda); 
            printf("\n---------------------------------- \n");        
        }
    }else {
        printf("\nNão há produtos cadastrados no sistema!");
    }
}

/**
* Printa menu com informações do produto
* @param
* @return
*/
void printaInformacoesProduto() {
    printf("\nEscolha sua alteração: ");
    printf("\n1 - Código.");
    printf("\n2 - Descrição.");
    printf("\n3 - Estoque.");
    printf("\n4 - Estoque Mínimo.");
    printf("\n5 - Preço de Custo.");
    printf("\n6 - Preço de Venda.");
    printf("\n7 - Sair.");
    printf("\n --> ");
}

/**
 * Realiza a edição de um produto;
 * @param 
 * @return 
 */
int editar_produto() {
    char escolha;
    int codigo,alt;
    Prod_Disp produto;
    printf("\nDeseja editar um produto (S/N)? ");
    scanf("%c%*c",&escolha);
    while (escolha == 'S' || escolha == 's') {
        printf("\nDigite o código do produto: ");
        scanf("%d",&codigo);
        produto = buscarProdutoPorCodigo(codigo);
        if (produto.codigo != -1) {
            printaInformacoesProduto();
            scanf("%d",&alt);
            while (alt != 7) {             
                switch (alt) {
                    case 1:
                        printf("\nDigite o código do produto: ");
                        scanf("%d",&produto.codigo);
                        break;
                    case 2:
                        printf("\nDescrição do produto: ");
                        setbuf(stdin, NULL);
                        fgets(produto.descricao,40,stdin);
                        produto.descricao[strcspn(produto.descricao, "\n")] = 0;
                        break;
                    case 3:
                        printf("\nEstoque: ");
                        scanf("%d",&produto.estoque);
                        break;
                    case 4:
                        printf("\nEstoque mínimo: ");
                        scanf("%d",&produto.min_estoque);
                        break;
                    case 5:
                        printf("\nPreço de custo: ");
                        scanf("%f",&produto.preco_custo);
                        break;
                    case 6:
                        printf("\nPreço de venda: ");
                        scanf("%f",&produto.preco_venda);
                        break;                                   
                    default: 
                        mostrarMensagemErroMenu();
                        break;
                }

                if (alt != 7) {
                    printaInformacoesProduto();
                    scanf("%d",&alt);
                }    
                atualizarProduto(produto);
            }
            if (alt == 7) {
                escolha = 'N';
            } else {
                setbuf(stdin, NULL);
                printf("\nDeseja editar um produto (S/N)? ");
                scanf("%c%*c",&escolha);
            }
        }else{
            printf("Produto não encontrado! \n");
        }
    }
}

/**
 * Realiza a pesquisa de produtos
 * @param 
 * @return 
 */
void pesquisar_produto() {
    char opcao;
    Prod_Disp produto;
    int codigo;
    printf("\nDeseja pesquisar um produto (S/N)? ");
    scanf("%c%*c",&opcao);
    while (opcao == 'S' || opcao == 's') {
        printf("\nDigite o código do produto: ");
        scanf("%d",&codigo);
        produto = buscarProdutoPorCodigo(codigo); //verifica a existência do produto;
        if (produto.codigo != -1) {
            printf("\nCódigo: %d",produto.codigo);
            printf("\nDescrição: %s",produto.descricao);
            printf("\nEstoque: %d",produto.estoque);
            printf("\nEstoque mínimo: %d",produto.min_estoque);
            printf("\nPreço de Custo: %.2f",produto.preco_custo);
            printf("\nPreço de Venda: %.2f",produto.preco_venda);
        }else {
            printf("\nErro: Código não existe!");
        }
        setbuf(stdin,NULL);
        printf("\nDeseja pesquisar um produto (S/N)? ");
        scanf("%c%*c",&opcao);
    }
}

/**
* Lança pagamentos e/ou contas a pagar
* @param Entrada_Produtos entrada: dados da entrada de produtos
* @param Pagamento pagamento: dados da forma de pagamento
* @return 
*/
void montarContasPagar(Entrada_Produtos entrada, Pagamento pagamento) {
    if (pagamento.formaPagamento == 1) {
        Historico_Caixa historico_Caixa;
        historico_Caixa.data = gerarDataAtual(); 
        strcpy(historico_Caixa.entrada_saida,"saida");
        strcpy(historico_Caixa.tipo,"pagamento");
        historico_Caixa.id_origem = entrada.codigo;
        historico_Caixa.valor = entrada.valor_total;
        // cadastrando o histórico e atualizando o caixa;
        cadastrarHistorico(historico_Caixa);
        return;
    }
    montaESalvaContaReceberPagar(pagamento,"pagamento","aguardando_lancamento","saida", entrada.valor_total, entrada.codigo);
}

int ReduzProdutos(int id, int qte)
{
        
     int qtdeProdutos = qtdeRegistrosProduto();
     Prod_Disp *produtos;
     produtos = carregar_produtos();
     for (int i = 0; i < qtdeProdutos; i++)
     {
         if(produtos[i].codigo == id){
        int value = (produtos[i].estoque-qte);
        produtos[i].estoque = value; 
         atualizarProduto(produtos[i]);
         }
     }
     
    
    
}

/**
* Monta e printa na tela nota fiscal da entrada de produtos
* @param Entrada_Produtos_Item *itens: lista com os itens do produto
* @param int qtdeItens: quantidade de itens de produto em cada entrada
* @param Entrada_Produtos entrada: dados da entrada de produtos
* @param Fornecedor fornecedor: dados do fornecedor dos produtos
* @return 
*/
void montarNotaFiscal(Entrada_Produtos_Item *itens, int qtde, Entrada_Produtos entrada, Fornecedor fornecedor) {
    Prod_Disp *produtos;
    produtos = carregar_produtos();
    int qtdeProdutos = qtdeRegistrosProduto();
    printf("============================================================================ \n");
    printf("FORNECEDOR:            | %s \n",fornecedor.forRazaoSocial);
    printf("CNPJ:                  | %s \n",fornecedor.forCnpj);
    printf("FRETE:                 | R$ %.2f           |IMPOSTO:       |R$%.2f \n",entrada.preco_frete,entrada.imposto);
    printf("\t \t \t PRODUTOS\n");
    printf("Descrição          | Preço custo (R$) | Quantidade (Unid)  | Total (R$) \n");
    for(int i=0;i<qtde;i++) {
        for(int j=0;j<qtdeProdutos;j++) {
            if(itens[i].id_produto == produtos[j].codigo) {
                printf("%s              | %.2f               | %d                 |%.2f           \n", produtos[j].descricao, itens[i].valor_compra, itens[i].quantidade, itens[i].valor_compra*itens[i].quantidade);
            }
        }
    }
    printf("\t Total da nota (produtos + frete + impostos):        |R$%.2f \n", entrada.valor_total);
    printf("============================================================================ \n");
}

/**
* Salva a entrada de produtos industrilizados no hotel: atualiza caixa, estoque
* @param Entrada_Produtos_Item *itens: lista com os itens do produto
* @param int qtdeItens: quantidade de itens de produto em cada entrada
* @param int qtdeTotal: quantidade de produtos cadastrados
* @param Entrada_Produtos entrada: dados da entrada de produtos
* @param Pagamento pagamento: dados com a forma de pagamento
* @param Fornecedor fornecedor: dados do fornecedor dos produtos
* @param char *nomeFantasia: nome fantasia do hotel
* @return 1- se foi salvo com sucesso 0: erro ao executar a ação
*/
int salvarEntradaProdutos(Entrada_Produtos_Item *itens, int qtdeItens,int qtdeTotal, Entrada_Produtos entrada, Pagamento pagamento, Fornecedor fornecedor, char *nomeFantasia) {
    FILE *file = NULL;
    FILE *file2 = NULL;
    float precoVendaAntigo,precoVendaNovo, preco_frete,imposto;
    preco_frete = entrada.preco_frete/(float)qtdeTotal;
    imposto = entrada.imposto/(float)qtdeTotal;
    int isNotaFiscal;
    int tipoArquivo = opcaoArmazenamentoUsuario();
    Hotel hotel;
    hotel = buscarHotelPorNomeFantasia(nomeFantasia);
    switch (tipoArquivo) {
        case 1:
            file = fopen("persist/entrada_produtos.bin", "a+b");
            // Verificando se o arquivo foi aberto de forma correta
            if (file != NULL) {
                entrada.codigo = sequenceArquivoBinario("persist/seq_entrada_produtos.bin");
                entrada.valor_total = 0;

                for(int i=0;i<qtdeItens;i++) {
                    file2 = fopen("persist/entrada_produtos_item.bin", "a+b");
                    Prod_Disp produto = buscarProdutoPorCodigo(itens[i].id_produto);
                    if(produto.codigo != -1 && file2 != NULL){
                        produto.estoque += itens[i].quantidade;
                        precoVendaAntigo = produto.preco_venda;
                        precoVendaNovo = (itens[i].valor_compra + imposto + preco_frete) * (((float)hotel.margem_lucro/(float)100) + 1); 
                        produto.preco_venda = (precoVendaAntigo + precoVendaNovo)/(float)2;
                        atualizarProduto(produto);    
                        itens[i].codigo = sequenceArquivoBinario("persist/seq_entrada_produtos_item.bin");
                        itens[i].id_entrada_produtos = entrada.codigo;
                        itens[i].preco_venda = produto.preco_venda;
                        entrada.valor_total = entrada.valor_total + (itens[i].valor_compra * itens[i].quantidade);
                        fwrite(&itens[i], sizeof (Entrada_Produtos_Item), 1, file2);
                        fclose(file2);
                    }
                }
                entrada.valor_total += entrada.preco_frete + entrada.imposto;
                fwrite(&entrada, sizeof (Entrada_Produtos), 1, file);
                fclose(file);
                montarContasPagar(entrada,pagamento);
                return SUCCESS;
            }
            mostrarMensagemErroArquivo();
            return ERROR;
            break;
        case 2:
            file = fopen("persist/entrada_produtos.txt", "a+");
            // Verificando se o arquivo foi aberto de forma correta
            if(file != NULL){
                entrada.codigo = sequenceArquivoTXT("persist/seq_entrada_produtos.txt");
                entrada.valor_total = 0;
                for(int i=0;i<qtdeItens;i++) {
                    file2 = fopen("persist/entrada_produtos_item.txt", "a+");
                    Prod_Disp produto = buscarProdutoPorCodigo(itens[i].id_produto);
                    if(produto.codigo != -1 || file2 != NULL) {
                        produto.estoque += itens[i].quantidade;
                        precoVendaAntigo = produto.preco_venda;
                        precoVendaNovo = (itens[i].valor_compra + imposto + preco_frete) * (((float)hotel.margem_lucro/(float)100) + 1) ; 
                        produto.preco_venda = (precoVendaAntigo + precoVendaNovo)/(float)2;
                        itens[i].preco_venda = produto.preco_venda;
                        entrada.valor_total = entrada.valor_total + (itens[i].valor_compra * itens[i].quantidade);
                        atualizarProduto(produto);    
                        itens[i].codigo = sequenceArquivoTXT("persist/seq_entrada_produtos_item.txt");
                        itens[i].id_entrada_produtos = entrada.codigo;
                        fprintf(file2, "%d;", itens[i].codigo);
                        fprintf(file2, "%d;", itens[i].id_entrada_produtos);
                        fprintf(file2, "%d;", itens[i].id_produto);
                        fprintf(file2, "%f;", itens[i].preco_venda);
                        fprintf(file2, "%d;", itens[i].quantidade);
                        fprintf(file2, "%f; \n", itens[i].valor_compra);
                        fclose(file2);
                    }
                }
                entrada.valor_total += entrada.preco_frete + entrada.imposto;
                fprintf(file, "%d;", entrada.codigo);
                fprintf(file, "%d;", entrada.id_fornecedor);
                fprintf(file, "%f;", entrada.imposto);
                fprintf(file, "%f;", entrada.preco_frete);
                fprintf(file, "%f;\n", entrada.valor_total);
                fclose(file);
                montarContasPagar(entrada,pagamento);
                printf("Deseja imprimir nota fiscal? 1- Sim   2-Não\n");
                scanf("%d",&isNotaFiscal);
                if (isNotaFiscal == 1) {
                    montarNotaFiscal(itens,qtdeItens,entrada,fornecedor);
                    return SUCCESS;
                }
                return SUCCESS;
            }
            mostrarMensagemErroArquivo();
            return ERROR;
        default:
            mostrarMensagemErroTipoArquivo();
            return ERROR;
            break;
    }
}