/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "../models/funcoesModel.h"
#include "../structs/moduloCadastroStructs.h"
#define SUCCESS 1;
#define ERROR 0;

/*
* Mostra a mensagem de erro padrão ao tentar abrir o arquivo
*/
void mostrarMensagemErroArquivo() {
    printf("\n Não foi possível abrir o arquivo de armazenamento \n");
}

/*
* Mostra a mensagem de erro padrão para quando o usuário escolher um tipo de arquivo inválido
*/
void mostrarMensagemErroTipoArquivo() {
    printf("Opção de armazenamento inválida! \n");
    printf("Tente 1- Binário 2-TXT \n");
}

/*
* Mostra a mensagem de erro padrão para quando o usuário escolher uma opção inválida no menu
*/
void mostrarMensagemErroMenu() {
    printf("Opção inválida! \n");
}

/*
* Mostra a mensagem de erro padrão para quando o registro já existe no armazenamento do sistema
*/
void mostrarMensagemRegistroExiste() {
    printf("O registro já existe em nossa base de dados! \n");
}

/*
* Mostra a mensagem de erro padrão para quando quiser realizar uma ação sem estar logado no sistema
*/
void mostrarMensagemUsuarioDeslogado() {
    printf("É necessário estar logado no sistema para fazer está ação! \n");
}

/*
* Mostra a mensagem de retornando menu
*/
void mostrarRetornandoMenu() {
    printf("RETORNANDO...\n");
}


/**
* Obtém a sequência de código no arquivo TXT informado
* @param char nomeArquivo[60]: nome do arquivo
* @return int: o código do próximo registro
*/
int sequenceArquivoTXT(char nomeArquivo[60]) {
    FILE *file = NULL;
    file = fopen(nomeArquivo, "r+");
    if(file != NULL){
        char seq[2];
        int seqInt = 0;
        fgets(seq, 2, file);
        seqInt = atoi(strtok(seq, ";"));
        fclose(file);
        seqInt++;
        file = fopen(nomeArquivo, "w");
        fprintf(file, "%d;", seqInt);
        fclose(file);
        return seqInt;
    }
    mostrarMensagemErroArquivo();
    return ERROR;
}

/**
* Obtém a sequência de código do produto no arquivo Binário
* @param
* @return int: o código do próximo registro
*/
int sequenceArquivoBinario(char nomeArquivo[]) {
    FILE *file = NULL;
    file = fopen(nomeArquivo, "r+b");
    if(file != NULL){
        int seq;
        fread(&seq, sizeof (int), 1, file);
        fclose(file);
        seq++;
        file = fopen(nomeArquivo, "wb");
        fwrite(&seq, sizeof (int), 1, file);
        fclose(file);
        return seq;
    }
    mostrarMensagemErroArquivo();
    return ERROR;
}

void mostrarDataEHora(DataTime data) {
    printf("data: %d/%d/%d, hora: %02d:%02d, %d segundos\n", data.dia, data.mes, data.ano, data.hora, data.minuto, data.segundo);
}

/*
* gera um objeto DataTime preenchido com o tempo atual
*/
DataTime gerarDataEHoraAtual() {
    DataTime atual;
    time_t rawtime;
    struct tm* info;
    time(&rawtime);
    info = localtime(&rawtime);
    atual.dia = info->tm_mday;
    atual.mes = info->tm_mon + 1;
    atual.ano = info->tm_year + 1900;
    atual.hora = info->tm_hour;
    atual.minuto = info->tm_min;
    atual.segundo = info->tm_sec;
    return atual;
}

/*
* gera um objeto Data preenchido com o tempo atual
*/
Data gerarDataAtual() {
    Data atual;
    time_t rawtime;
    struct tm* info;
    time(&rawtime);
    info = localtime(&rawtime);
    atual.dia = info->tm_mday;
    atual.mes = info->tm_mon + 1;
    atual.ano = info->tm_year + 1900;
    return atual;
}

/*
* cria e retorna uma estrutura DataTime com todos os valores 0
*/
DataTime dataTimeZero(){
    DataTime zero;
    zero.dia = 0;
    zero.mes = 0;
    zero.ano = 0;
    zero.hora = 0;
    zero.minuto = 0;
    zero.segundo = 0;
    return zero;
}

/*
* Recebe input para uma string (usa fgets em vez de scanf para ser mais segura)
*/
void inputString(char s[], int len) {
    setbuf(stdin, NULL);
    fgets(s, len, stdin);
    s[strcspn(s, "\n")] = 0;
    setbuf(stdin, NULL);
}

/**
* Compara se uma data é posterior ou anterior a outra
* @param Data data1: data de comparação
* @param Data data2: data de referencia
* @return -1 se data1 é anterior a data2, 1 se data1 é posterior, 0 se elas são iguais
*/
int compararDatas(Data data1, Data data2) {
    if (data1.ano > data2.ano) {
        return 1;
    } else if (data1.ano < data2.ano) {
        return -1;
    } else if (data1.ano == data2.ano) {
        if(data1.mes > data2.mes) {
            return 1;
        } else if (data1.mes < data2.mes) {
            return -1;
        }else if (data1.mes == data2.mes) {
            if(data1.dia > data2.dia) {
                return 1;
            } else if (data1.dia < data2.dia) {
                return -1;
            } else if(data1.dia == data2.dia) {
                return 0;
            }
        }
    }
}

/**
* Compara se 2 datas coincidem
* @param Data data1Inicio: a data inicial do "primeiro" periodo de tempo
* @param Data data1Fim: a data final do "primeiro" periodo de tempo
* @param Data data2Inicio: a data inicial do "segundo" periodo de tempo
* @param Data data2Fim: a data final do "segundo" periodo de tempo
* @return 1 se elas coincide, 0 se não há conflito
*/
int dataPeriodoCoincide(Data data1Inicio, Data data1Fim, Data data2Inicio, Data data2Fim) {
    // se a data de inicio é anterior a data de inicio da referencia
    if (compararDatas(data1Inicio, data2Inicio) == -1) {
        // então a data final deve ser menor que a data de inicio da referencia
        if (compararDatas(data1Fim, data2Inicio) == -1) {
            return 0; // data1 termina antes de começar data2
        } else {
            return 1; // data1 termina enquanto data2 ainda não
        }
        // se a data de inicio é posterior a data final da referencia
    } else if (compararDatas(data1Inicio, data2Fim) == 1) {
        return 0; // data1 começa depois que data2 termina
    }
    return 1;
}


int dias_mes[2][13] = {{0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
                       {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}};

/**
* Calcula quantos dias há entre di e df
* di DEVE ser anterior a df, caso não, a função nunca termina
* @param Data di: Data inicial
* @param Data df: Data final
* @return: quantidade de dias
*/
int calculaDiasEntreDatas(DataTime di, DataTime df) {
    // se di for posterior a df, trocar
    int count = 0;
    // objeto de data que será "movido" até a data final, para contagem e métricas
    DataTime transfer = di;

    // soma meses até chegar no mes e ano procurado
    while (transfer.ano != df.ano || transfer.mes != df.mes) {
        int bix;
        if (transfer.ano % 4 == 0) {
            bix = 1;
        }
        else {
            bix = 0;
        }

        count += dias_mes[bix][transfer.mes];
        transfer.mes++;

        if (transfer.mes > 12) {
            transfer.ano++;
            transfer.mes = 1;
        }
    }

    // estando o transfer na mesma data, simplesmente soma a diferença dos dias restantes
    count += df.dia - transfer.dia;
    return count;
}
