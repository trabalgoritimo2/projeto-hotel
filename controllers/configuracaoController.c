/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#include <stdio.h>
#include <stdlib.h>
#include "../models/configuracaoModel.h"
#include "../models/usuarioModel.h"
#include "../models/funcoesModel.h"
#include "../structs/moduloCadastroStructs.h"

/**
* Cria os arquivos de armazenamento de todo o sistema de acordo com o tipo de arquivo
* @param int tipoArquivo: 1: Binário 2: TXT
* @return
*/
void criarArquivos(int tipoArquivo) {
    // Ponteiro File
    FILE *file = NULL;
    switch (tipoArquivo) {
        case 1:
            // Módulo Reserva
            file = fopen("persist/reserva.bin", "ab");
            if (file == NULL) {
               //Se não abrir o arquivo, cria um
                file = fopen("persist/reserva.bin", "wb+");
            }
            fclose(file);

            // Módulo Gestão
            // Arquivo Binário Hotel 
            file = fopen("persist/hotel.bin", "ab");
            if (file == NULL) {
               //Se não abrir o arquivo, cria um
                file = fopen("../persist/hotel.bin", "wb+");
            }
            fclose(file);

            // Arquivo binário: Acomodacao
            file = fopen("persist/acomodacao.bin", "ab");
            if (file == NULL) {
               //Se não abrir o arquivo, cria um
                file = fopen("persist/acomodacao.bin", "wb+");
            }
            fclose(file);

            //Arquivo binário: Categoria
            file = fopen("persist/categoria.bin", "ab");
            if (file == NULL) {
               //Se não abrir o arquivo, cria um
                file = fopen("persist/categoria.bin", "wb+");
            }
            fclose(file);

            // Arquivo binário: Fornecedor
            file = fopen("persist/fornecedor.bin", "ab");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/fornecedor.bin", "wb+");
            }
            fclose(file);

            // Arquivo binário: Hospede
            file = fopen("persist/hospede.bin", "ab");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/hospede.bin", "wb+");
            }
            fclose(file);

            // Arquivo binário: Produto
            file = fopen("persist/produto.bin", "ab");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/produto.bin", "wb+");
            }
            fclose(file);

            // Arquivo binário: Usuario
            file = fopen("persist/usuario.bin", "ab");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/usuario.bin", "wb+");
            }

            // Arquivo binário: Conta_Hospede
            file = fopen("persist/conta_hospede.bin", "ab");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/conta_hospede.txt", "wb+");
            }
            fclose(file);

            // Arquivo binário: Historico_Caixa
            file = fopen("persist/historico_caixa.bin", "ab");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/historico_caixa.bin", "wb+");
            }
            fclose(file);

            // Arquivo binário: Caixa
            file = fopen("persist/caixa.bin", "ab");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                float valorInicial = 0;
                file = fopen("persist/caixa.bin", "wb+");
                fwrite(&valorInicial, sizeof (float), 1, file);
            }
            fclose(file);

            // Arquivo binário: Contas_Receber_Pagar
            file = fopen("persist/contas_receber_pagar.bin", "ab");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/contas_receber_pagar.bin", "wb+");
            }
            fclose(file);

            // Arquivo binário: Entrada_Produtos
            file = fopen("persist/entrada_produtos.bin", "ab");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/entrada_produtos.bin", "wb+");
            }
            fclose(file);

            // Arquivo binário: Entrada_Produtos_Item
            file = fopen("persist/entrada_produtos_item.bin", "ab");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/entrada_produtos_item.bin", "wb+");
            }
            fclose(file);

            // Arquivo binário: Venda
            file = fopen("persist/venda.bin", "ab");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/venda.bin", "w+");
            }
            fclose(file);
            
            // Arquivo binário: Venda_Produtos
            file = fopen("persist/venda_produtos.bin", "ab");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/venda_produtos.bin", "wb+");
            }
            fclose(file);
            break;
        case 2:
            // Módulo Reserva
            file = fopen("persist/reserva.txt", "a");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/reserva.txt", "w+");
            }
            fclose(file);

            // Módulo Gestão
            // Arquivo txt: Hotel
            file = fopen("persist/hotel.txt", "a");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/hotel.txt", "w+");
            }
            fclose(file);

            // Arquivo txt: Acomodacao
            file = fopen("persist/acomodacao.txt", "a");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/acomodacao.txt", "w+");
            }
            fclose(file);

            // Arquivo txt: Categoria
            file = fopen("persist/categoria.txt", "a");
            if (file == NULL) {
               //Se não abrir o arquivo, cria um
                file = fopen("persist/categoria.txt", "w+");
            }
            fclose(file);

            // Arquivo txt: Fornecedor
            file = fopen("persist/fornecedor.txt", "a");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/fornecedor.txt", "w+");
            }
            fclose(file);

            // Arquivo txt: Hospede
            file = fopen("persist/hospede.txt", "a");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/hospede.txt", "w+");
            }
            fclose(file);

            // Arquivo txt: Produto
            file = fopen("persist/produto.txt", "a");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/produto.txt", "w+");
            }
            fclose(file);

            // Arquivo txt: Usuário
            file = fopen("persist/usuario.txt", "a");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/usuario.txt", "w+");
            }
            fclose(file);

            // Arquivo txt: Conta_Hospede
            file = fopen("persist/conta_hospede.txt", "a");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/conta_hospede.txt", "w+");
            }
            fclose(file);

            // Arquivo txt: Historico_Caixa
            file = fopen("persist/historico_caixa.txt", "a");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/historico_caixa.txt", "w+");
            }
            fclose(file);

            // Arquivo txt: Caixa
            file = fopen("persist/caixa.txt", "a");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/caixa.txt", "w+");
                float valorInicial = 0;
                fprintf(file, "%f;", valorInicial);
            }
            fclose(file);

            // Arquivo txt: Contas_Receber_Pagar
            file = fopen("persist/contas_receber_pagar.txt", "a");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/contas_receber_pagar.txt", "w+");
            }
            fclose(file);

            // Arquivo txt: Entrada_Produtos
            file = fopen("persist/entrada_produtos.txt", "a");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/entrada_produtos.txt", "w+");
            }
            fclose(file);

            // Arquivo txt: Entrada_Produtos_Item
            file = fopen("persist/entrada_produtos_item.txt", "a");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/entrada_produtos_item.txt", "w+");
            }
            fclose(file);

            // Arquivo txt: Venda
            file = fopen("persist/venda.txt", "a");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/venda.txt", "w+");
            }
            fclose(file);
            
            // Arquivo txt: Venda_Produtos
            file = fopen("persist/venda_produtos.txt", "a");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/venda_produtos.txt", "w+");
            }
            fclose(file);
            break;
        default:
            break;
    }
}

/**
* Cria os arquivos de armazenamento das sequencias de todo o sistema de acordo com o tipo de arquivo
* @param int tipoArquivo: 1: Binário 2: TXT
* @return
*/
void criarArquivosSeq(int tipoArquivo) {
    // Ponteiro File
    FILE *file = NULL;
    int seqInicial = 0;
    switch (tipoArquivo) {
        case 1:
            // Módulo Reserva
            file = fopen("persist/seq_reserva.bin", "r+b");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/seq_reserva.bin", "wb");
                fwrite(&seqInicial, sizeof (int), 1, file);
            }
            fclose(file);

            // Módulo Gestão
            // Arquivo Binário Hotel 
            file = fopen("persist/seq_hotel.bin", "r+b");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/seq_hotel.bin", "wb");
                fwrite(&seqInicial, sizeof (int), 1, file);
            }
            fclose(file);

            // Arquivo binário: Acomodacao
            file = fopen("persist/seq_acomodacao.bin", "r+b");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/seq_acomodacao.bin", "wb");
                fwrite(&seqInicial, sizeof (int), 1, file);
            }

            //Arquivo binário: Categoria
            file = fopen("persist/seq_categoria.bin", "r+b");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/seq_categoria.bin", "wb");
                fwrite(&seqInicial, sizeof (int), 1, file);
            }
            fclose(file);

            // Arquivo binário: Fornecedor
            file = fopen("persist/seq_fornecedor.bin", "r+b");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/seq_fornecedor.bin", "wb");
                fwrite(&seqInicial, sizeof (int), 1, file);
            }
            fclose(file);

            // Arquivo binário: Hospede
            file = fopen("persist/seq_hospede.bin", "r+b");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/seq_hospede.bin", "wb");
                fwrite(&seqInicial, sizeof (int), 1, file);
            }
            fclose(file);

            // Arquivo binário: Produto
            file = fopen("persist/seq_produto.bin", "r+b");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/seq_produto.bin", "wb");
                fwrite(&seqInicial, sizeof (int), 1, file);
            }
            fclose(file);

            // Arquivo binário: Usuario
            file = fopen("persist/seq_usuario.bin", "r+b");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/seq_usuario.bin", "wb");
                fwrite(&seqInicial, sizeof (int), 1, file);
            }

            // Arquivo binário: Conta_Hospede
            file = fopen("persist/seq_conta_hospede.bin", "r+b");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/seq_conta_hospede.bin", "wb");
                fwrite(&seqInicial, sizeof (int), 1, file);
            }

            // Arquivo binário: Historico_Caixa
            file = fopen("persist/seq_historico_caixa.bin", "r+b");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/seq_historico_caixa.bin", "wb");
                fwrite(&seqInicial, sizeof (int), 1, file);
            }

            // Arquivo binário: Contas_Receber_Pagar
            file = fopen("persist/seq_contas_receber_pagar.bin", "r+b");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/seq_contas_receber_pagar.bin", "wb");
                fwrite(&seqInicial, sizeof (int), 1, file);
            }

            // Arquivo binário: Entrada_Produtos
            file = fopen("persist/seq_entrada_produtos.bin", "r+b");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/seq_entrada_produtos.bin", "wb");
                fwrite(&seqInicial, sizeof (int), 1, file);
            }

            // Arquivo binário: Entrada_Produtos_Item
            file = fopen("persist/seq_entrada_produtos_item.bin", "r+b");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/seq_entrada_produtos_item.bin", "wb");
                fwrite(&seqInicial, sizeof (int), 1, file);
            }

            // Arquivo binário: Entrada_Produtos_Item
            file = fopen("persist/seq_venda.bin", "r+b");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/seq_venda.bin", "wb");
                fwrite(&seqInicial, sizeof (int), 1, file);
            }

            // Arquivo binário: Entrada_Produtos_Item
            file = fopen("persist/seq_venda_produtos.bin", "r+b");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/seq_venda_produtos.bin", "wb");
                fwrite(&seqInicial, sizeof (int), 1, file);
            }
            break;
        case 2:
            // Módulo Reserva
            file = fopen("persist/seq_reserva.txt", "r+");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/seq_reserva.txt", "w");
                fprintf(file, "%d;", seqInicial);
            }
            fclose(file);

            // Módulo Gestão
            // Arquivo txt: Hotel
            file = fopen("persist/seq_hotel.txt", "r+");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/seq_hotel.txt", "w");
                fprintf(file, "%d;", seqInicial);
            }
            fclose(file);

            // Arquivo txt: Acomodacao
            file = fopen("persist/seq_acomodacao.txt", "r+");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/seq_acomodacao.txt", "w");
                fprintf(file, "%d;", seqInicial);
            }
            fclose(file);

            // Arquivo txt: Categoria
            file = fopen("persist/seq_categoria.txt", "r+");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/seq_categoria.txt", "w");
                fprintf(file, "%d;", seqInicial);
            }
            fclose(file);

            // Arquivo txt: Fornecedor
            file = fopen("persist/seq_fornecedor.txt", "r+");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/seq_fornecedor.txt", "w");
                fprintf(file, "%d;", seqInicial);
            }
            fclose(file);

            // Arquivo txt: Hospede
            file = fopen("persist/seq_hospede.txt", "r+");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/seq_hospede.txt", "w");
                fprintf(file, "%d;", seqInicial);
            }
            fclose(file);

            // Arquivo txt: Produto
            file = fopen("persist/seq_produto.txt", "r+");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/seq_produto.txt", "w");
                fprintf(file, "%d;", seqInicial);
            }
            fclose(file);

            // Arquivo txt: Usuário
            file = fopen("persist/seq_usuario.txt", "r+");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/seq_usuario.txt", "w");
                fprintf(file, "%d;", seqInicial);
            }
            fclose(file);

            // Arquivo txt: Conta_Hospede
            file = fopen("persist/seq_conta_hospede.txt", "r+");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/seq_conta_hospede.txt", "w");
                fprintf(file, "%d;", seqInicial);
            }
            fclose(file);

            // Arquivo txt: Historico_Caixa
            file = fopen("persist/seq_historico_caixa.txt", "r+");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/seq_historico_caixa.txt", "w");
                fprintf(file, "%d;", seqInicial);
            }
            fclose(file);

            // Arquivo txt: Contas_Receber_Pagar
            file = fopen("persist/seq_contas_receber_pagar.txt", "r+");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/seq_contas_receber_pagar.txt", "w");
                fprintf(file, "%d;", seqInicial);
            }
            fclose(file);

            // Arquivo txt: Entrada_Produtos
            file = fopen("persist/seq_entrada_produtos.txt", "r+");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/seq_entrada_produtos.txt", "w");
                fprintf(file, "%d;", seqInicial);
            }
            fclose(file);

            // Arquivo txt: Entrada_Produtos_Item
            file = fopen("persist/seq_entrada_produtos_item.txt", "r+");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/seq_entrada_produtos_item.txt", "w");
                fprintf(file, "%d;", seqInicial);
            }
            fclose(file);

            // Arquivo txt: Venda
            file = fopen("persist/seq_venda.txt", "r+");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/seq_venda.txt", "w");
                fprintf(file, "%d;", seqInicial);
            }
            fclose(file);

            // Arquivo txt: Venda_Produtos
            file = fopen("persist/seq_venda_produtos.txt", "r+");
            if (file == NULL) {
                //Se não abrir o arquivo, cria um
                file = fopen("persist/seq_venda_produtos.txt", "w");
                fprintf(file, "%d;", seqInicial);
            }
            fclose(file);
            break;
        default:
            break;
    }
}

/**
* Cria o usuário administrador com permissão à todos os módulos
* @param int tipoArquivo: 1: Binário 2: TXT
* @return
*/
void criarUsuarioAdm(int tipoArquivo){
    FILE *file = NULL;
    Usuario user = buscarUsuarioPorNomeUsuario("root");
    // se já não tiver criado
    if(user.usuCodigo == -1) {
         Usuario admin = {
            0,
            "Administrador",
            "root",
            "root",
            1
        };

    switch(tipoArquivo) {
        case 1:
            file = fopen("persist/usuario.bin", "ab+");
            if (file != NULL) {
                fwrite(&admin, sizeof (Usuario), 1, file);
                fclose(file);
            }
            break;
        case 2:
            file = fopen("persist/usuario.txt", "a+");
            if (file != NULL) {
                fprintf(file, "%d;", admin.usuCodigo);
                fprintf(file, "%s;", admin.usuNome);
                fprintf(file, "%s;", admin.usuUsuario);
                fprintf(file, "%s;", admin.usuSenha);
                fprintf(file, "%d;\n", admin.usuPermissao);
                fclose(file);
            }
        break;
        default:
            mostrarMensagemErroTipoArquivo();
            break;
        }
    }
       
}

/**
* Verifica qual o tipo de arquivo que o usuário escolheu para armazenar os dados do sistema
* E se ele ainda não escolheu, mostra a tela para o usuário escolher
* @param 
* @return int tipoArquivo: 1: Binário 2: TXT
*/
int verificaOpcaoArmazenamentoUsuario() {
    FILE *file = NULL;
    file = fopen("persist/armazenamento_usuario.txt", "r+");
    if(file != NULL){
        char escolhaUsuario[2];
        int escolhaUsuarioInt = 0;
        fgets(escolhaUsuario, 2, file);
        escolhaUsuarioInt = atoi(escolhaUsuario);
        fclose(file);
        if(escolhaUsuarioInt == 0){
            printf("Querido usuário, seja bem-vindo ao nosso sistema administrativo de Hoteis! \n");
            printf("Em que tipo de arquivo você deseja armazenar seus dados? 1- Arquivos binário 2- Arquivos TXT \n");
            printf("Observação: Por enquanto essa ação é irreversível! \n");
            scanf("%d%*c",&escolhaUsuarioInt);

            if(escolhaUsuarioInt == 1 || escolhaUsuarioInt == 2){
                file = fopen("persist/armazenamento_usuario.txt", "w");
                fprintf(file, "%d;", escolhaUsuarioInt);
                fclose(file);
                return escolhaUsuarioInt;
            } else {
                printf("Opção inválida!Tente novamente! \n");
                return 0;
            }
        }
        return escolhaUsuarioInt;
    }
    return 0;
}

/**
* Verifica qual o tipo de arquivo que o usuário escolheu para armazenar os dados do sistema
* @param 
* @return int tipoArquivo: 1: Binário 2: TXT
*/
int opcaoArmazenamentoUsuario() {
    FILE *file = NULL;
    file = fopen("persist/armazenamento_usuario.txt", "r+");
    if(file != NULL){
        char escolhaUsuario[2];
        fgets(escolhaUsuario, 2, file);
        return atoi(escolhaUsuario);
    }
    return 0;
}