/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#pragma once
#include <stdio.h>
#include <stdlib.h>
#include "../models/acomodacaoModel.h"
#include "../models/funcoesModel.h"

/**
* Tela do menu de acomodação
* @param Usuario usuarioLogado: usuário autenticado no sistema
* @return
*/
void abrirMenuAcomodacao(Usuario usuarioLogado){
    int opcao = 0;
    do {
        printf("=========================\n");
        printf("|       ACOMODAÇÃO      |\n");
        printf("=========================\n");
        printf("1- Cadastrar \n");
        printf("2- Listar \n");
        printf("3- Pesquisar \n");
        printf("4- Atualizar \n");
        printf("5- Deletar \n");
        printf("6- Retornar \n");
        printf("Escolha uma opção: ");
        scanf("%d%*c", &opcao);

        switch(opcao){
            case 1: 
                cadastrarAcomodacao();
                break;
            case 2:
                mostrarAcomodacao();
                break;
            case 3:
                pesquisarAcomodacao();
                break;
            case 4:
                editarAcomodacao();
                break;
            case 5:
                excluirAcomodacao();
                break;
            case 6:
                mostrarRetornandoMenu();
                break;
            default: 
                mostrarMensagemErroMenu(); 
                break;
        }
    } while(opcao != 6);
    mostrarMenu(usuarioLogado);
}