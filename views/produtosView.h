/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#pragma once
#include <stdio.h>
#include <stdlib.h>
#include "../models/produtosModel.h"
#include "../structs/moduloCadastroStructs.h"
#include "../models/funcoesModel.h"
#include "../models/fornecedorModel.h"

/**
* Tela do menu de produto
* @param Usuario usuarioLogado: usuário autenticado no sistema
* @return
*/
void abrirMenuProduto(Usuario usuarioLogado) {
    int opcao = 0;
    do {
        printf("=====================\n");
        printf("|       PRODUTO     |\n");
        printf("=====================\n");
        printf("1- Cadastrar \n");
        printf("2- Listar \n");
        printf("3- Pesquisar \n");
        printf("4- Atualizar \n");
        printf("5- Deletar \n");
        printf("6- Retornar  \n");
        printf("Escolha uma opção: ");
        scanf("%d%*c", &opcao);
        switch(opcao){
            case 1: 
                cadastrarProduto();
                break;
            case 2:
                listarProdutos();
                break;
            case 3:
                pesquisar_produto();
                break;
            case 4:
                editar_produto();
                break;
            case 5:
                deletarProduto();
                break;
            case 6:
                mostrarRetornandoMenu();
                break;
            default: 
                mostrarMensagemErroMenu(); 
                break;
        }
    } while(opcao != 6);
    mostrarMenu(usuarioLogado);
}


void entrada_produtos(){
    char forCnpj[50];
    char nomeFantasia[55];
    int isInserir = 2;
    int isInserirProduto = 1;
    int qtdeProdutosEntrada = 0;
    int qtdeTotal = 0;
    Pagamento pagamento;
    Fornecedor fornecedor;
    Entrada_Produtos entrada_produtos;
    Entrada_Produtos_Item *itens = malloc(50*sizeof(Prod_Disp));
    printf("=================================\n");
    printf("|       ENTRADA DE PRODUTOS     |\n");
    printf("=================================\n");
    do {
        printf("Insira o nome do Hotel: \n");
        fgets(nomeFantasia, 55, stdin);
        nomeFantasia[strcspn(nomeFantasia, "\n")] = 0;
        setbuf(stdin, NULL);
        printf("Insira o CNPJ  do fornecedor: \n");
        fgets(forCnpj, 50, stdin);
        forCnpj[strcspn(forCnpj, "\n")] = 0;
        setbuf(stdin, NULL);
        fornecedor = buscaFornecedorPorCNPJ(forCnpj);
        if(fornecedor.forCodigo != -1) {
            printf("Insira o valor do imposto: \n");
            scanf("%f",&entrada_produtos.imposto);
            printf("Insira o valor do frete: \n");
            scanf("%f",&entrada_produtos.preco_frete);
            entrada_produtos.id_fornecedor = fornecedor.forCodigo;
            do {
                Prod_Disp *produtos;
                produtos = carregar_produtos();
                int qtdeProdutos = qtdeRegistrosProduto();
                printf("-----------Produtos---------- \n");
                for (int i = 0; i< qtdeProdutos; i++) {
                    printf("Código:%d  Produto: %s \n",produtos[i].codigo,produtos[i].descricao);      
                }
                printf("\n");
                printf("Insira o código do produto: \n");
                scanf("%d",&itens[qtdeProdutosEntrada].id_produto);
                printf("Insira o valor de compra: \n");
                scanf("%f",&itens[qtdeProdutosEntrada].valor_compra);
                printf("Insira a quantidade: \n");
                scanf("%d",&itens[qtdeProdutosEntrada].quantidade);
                qtdeTotal += itens[qtdeProdutosEntrada].quantidade;
                printf("Deseja inserir mais um produto? 1-Sim 2-Não \n");
                scanf("%d",&isInserirProduto);
                qtdeProdutosEntrada++;
            } while(isInserirProduto == 1);
            printf("Forma de pagamento: \n");
            printf("1- À vista \n");
            printf("2- À prazo \n");
            scanf("%d%*c",&pagamento.formaPagamento);
            if(pagamento.formaPagamento == 2){
                printf("Deseja dar uma entrada? 1- sim   2- não \n");
                scanf("%d%*c",&pagamento.isEntrada);
                if(pagamento.isEntrada == 1) {
                    printf("Insira o valor da entrada: \n");
                    scanf("%f",&pagamento.valorEntrada);
                }
                printf("Deseja dividir em quantas vezes? \n");
                scanf("%d",&pagamento.numeroParcelas);
                printf("Data de vencimento \n");
                printf("Dia:");
                scanf("%d",&pagamento.dataVencimento.dia);
                printf("Mês:");
                scanf("%d",&pagamento.dataVencimento.mes);
                printf("Ano:");
                scanf("%d",&pagamento.dataVencimento.ano);
            }
            if(salvarEntradaProdutos(itens,qtdeProdutosEntrada, qtdeTotal, entrada_produtos, pagamento, fornecedor,nomeFantasia) == 1){
                printf("Entrada de produtos realizada com sucesso! \n");
            }
            
        } else {
            printf("Fornecedor não encontrado! \n");
            printf("Deseja inserir outro CNPJ? 1-Sim   2-Não \n");
            scanf("%d",&isInserir);
        }
    } while(isInserir == 1);
}