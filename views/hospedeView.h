/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/

#include <stdio.h>
#include <stdlib.h>
#include "../models/hospedeModel.h"
#include "../models/funcoesModel.h"

/**
* Tela de cadastro do hóspede
* @param
* @return
*/
void cadastrarHospede() {
    printf("=============================\n");
    printf("|    CADASTRAR HÓSPEDE      |\n");
    printf("=============================\n");
    Hospede hospede;
    Endereco endereco;
    printf("Informe o nome:\n");
    scanf(" %s", hospede.hospNome);
    printf("Informe o CPF:\n");
    scanf(" %s", hospede.hospCpf);
    printf("Informe o telefone:\n");
    scanf(" %s", hospede.hospTelefone);
    printf("Informe o email\n");
    scanf(" %s", hospede.hospEmail);
    printf("Informe o sexo: (F-Feminino  M-Masculino)\n");
    scanf(" %c%*c", &hospede.hospSexo);
    printf("Informe o estado civil:\n");
    scanf(" %c", &hospede.hospEstadoCivil);
    printf("Informe o data de nascimento:\n");
    inputString(hospede.hospDataNascimento, 12);
    // scanf(" %s", hospede.hospDataNascimento);
    printf("Informe a rua:\n");
    scanf(" %s", endereco.endRua);
    printf("Informe o número:\n");
    scanf("%d%*c", &endereco.endNumero);
    printf("Informe o bairro:\n");
    scanf(" %s", endereco.endBairro);
    printf("Informe a cidade:\n");
    scanf(" %s", endereco.endCidade);
    printf("Informe o estado:\n");
    scanf(" %s", endereco.endEstado);
    printf("Informe a CEP:\n");
    scanf("%d", &endereco.endCep);
    printf("Informe o complemento do endereço:\n");
    scanf(" %s", endereco.endComplemento);
    hospede.hospEndereco = endereco;
    int success = cadastrarHospContoller(hospede);
    if (success == 1) {
        printf("Hóspede cadastrado com sucesso! \n");
    } else {
        printf("Erro ao realizar o cadastro do hóspede");
    }
}

/**
* Tela de atualização do cadastro de hóspede
* @param
* @return int: 1- Atualizado com sucesso  0- Houve erro
*/
int atualizarHospede() {
    printf("=============================\n");
    printf("|     ATUALIZAR HÓSPEDE     |\n");
    printf("=============================\n");
    Hospede hospede;
    Endereco endereco;
    int teste = 0;
    int cod = 0;
    listarHospedes();
    printf("Informe o codigo do hospede que deseja atualizar:\n");
    scanf(" %d", &cod);
    printf("Informe o nome do hospede:\n");
    scanf(" %s", hospede.hospNome);
    printf("Informe o CPF do hóspede:\n");
    scanf(" %s", hospede.hospCpf);
    printf("Informe o telefone do hóspede:\n");
    scanf(" %s", hospede.hospTelefone);
    printf("Informe o email do hóspede:\n");
    scanf(" %s", hospede.hospEmail);
    printf("Informe o sexo do hóspede:\n");
    scanf(" %c", &hospede.hospSexo);
    printf("Informe o Estado Civil do hóspede:\n");
    scanf(" %c", &hospede.hospEstadoCivil);
    printf("Informe o data de nascimento do hóspede:\n");
    scanf(" %s", hospede.hospDataNascimento);
    printf("Informe a rua do hóspede:\n");
    scanf(" %s", endereco.endRua);
    printf("Informe o numero do endereço:\n");
    scanf(" %d", &endereco.endNumero);
    printf("Informe o Bairro do hóspede:\n");
    scanf(" %s", endereco.endBairro);
    printf("Informe a Cidade do hóspede:\n");
    scanf(" %s", endereco.endCidade);
    printf("Informe o estado do hóspede:\n");
    scanf(" %s", endereco.endEstado);
    printf("Informe a CEP do hóspede:\n");
    scanf(" %d", &endereco.endCep);
    printf("Informe o complemento do endereço:\n");
    scanf(" %s", endereco.endComplemento);
    hospede.hospEndereco = endereco;
    int success = atualizarHospedeController(cod, hospede);
    if (success == 1) {
        printf("Hóspede atualizado com sucesso!\n");
        return 1;
    }
    printf("Erro ao cadastrar hóspede\n");
    return 0;
}

/**
* Tela de listagem de todos os hóspedes cadastrados
* @param
* @return
*/
void listarHospedeView() {
    printf("=============================\n");
    printf("|       LISTAR HÓSPEDES     |\n");
    printf("=============================\n");
    listarHospController();
}

/**
* Tela de exclusão de algum hóspede
* @param
* @return
*/
void deletarHospedeView() {
    printf("==============================\n");
    printf("|     DELETAR HÓSPEDE     |\n");
    printf("==============================\n");
    Hospede hospede;
    Endereco endereco;
    int teste = 0;
    int cod = 0;
    listarHospedes();
    printf("Informe o código do hospede que deseja excluir:\n");
    scanf(" %d", &cod);

    int success = deletarHospedeController(cod);
    if (success == 1) {
        printf("Hóspede cadastrado com sucesso! \n");
        return;
    }
    printf("Erro ao cadastrar o hóspede!\n");
}

/**
* Tela do menu do hóspede
* @param Usuario usuarioLogado: usuário autenticado no sistema
* @return
*/
void menuHospede(Usuario usuarioLogado) {
    int opc = 0;
    while (opc != 5) {
    printf("========================\n");
    printf("|       HÓSPEDE        |\n");
    printf("========================\n");
    printf("1- Cadastrar \n");
    printf("2- Listar\n");
    printf("3- Atualizar \n");
    printf("4- Deletar \n");
    printf("5- Retornar\n");
    scanf(" %d", &opc);
    switch (opc) {
        case 1:
            cadastrarHospede();
            mostrarMenu(usuarioLogado);
            break;
        case 2:
            listarHospedeView();
            mostrarMenu(usuarioLogado);
            break;
        case 3:
            atualizarHospede();
            mostrarMenu(usuarioLogado);
            break;
        case 4:
            deletarHospedeView();
            mostrarMenu(usuarioLogado);
            break;
        case 5:
            mostrarRetornandoMenu();
            mostrarMenu(usuarioLogado);
            break;
        default:
            mostrarMensagemErroMenu();
            break;
        }
    }
}
