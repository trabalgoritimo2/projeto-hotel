#pragma once

#include "../models/exportacaoModel.h"
#include "../models/funcoesModel.h"

char* DEFAULT_XML_PATH = "dados_do_hotel.xml";

void exportarDadosView() {
    int escolha = 0;
    printf("Deseja inserir o caminho do arquivo de exportação? 1-SIM; 2-NÂO\n");
    scanf("%d%*c", &escolha);
    if (escolha == 1) {
        char path[500];
        printf("Digite o caminho do arquivo:\n");
        inputString(path, 500);
        exportarDados(path);
    }
    else {
        printf("O arquivo será salvo no caminho padrão\n");
        exportarDados(DEFAULT_XML_PATH);
    }
}

void importarDadosView() {
    int escolha = 0;
    printf("Deseja inserir o caminho do arquivo de importação? 1-SIM; 2-NÂO\n");
    scanf("%d%*c", &escolha);
    if (escolha == 1) {
        char path[500];
        printf("Digite o caminho do arquivo:\n");
        inputString(path, 500);
        importarDados(path);
    }
    else {
        printf("O arquivo será salvo no caminho padrão\n");
        importarDados(DEFAULT_XML_PATH);
    }
}
