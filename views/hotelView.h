/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#pragma once
#include <stdio.h>
#include <stdlib.h>
#include "../models/categoriaModel.h"
#include "../models/funcoesModel.h"
#include "../models/hotelModel.h"


void printaInformacoesHotel(Hotel hotel) {
    printf("\n\t\t DADOS DO HOTEL");
    printf("\nNome Fantasia: %s",hotel.nome_fantasia);
    printf("\nRazao Social: %s",hotel.razao_social);
    printf("\nInscrição Estadual: %d",hotel.inscricao_estadual);
    printf("\nCNPJ: %d",hotel.cnpj);
    printf("\nTelefone: %d",hotel.telefone);
    printf("\nEmail: %s",hotel.email);
    printf("\nTelefone do responsável: %d",hotel.telefone_responsavel);
    printf("\nNome do responsável: %s",hotel.nome_responsavel);
    printf("\n----------Campos do Endereço--------");
    printf("\nRua: %s",hotel.endereco.endRua);
    printf("\nNúmero: %d",hotel.endereco.endNumero);
    printf("\nBairro: %s",hotel.endereco.endBairro);
    printf("\nCidade: %s",hotel.endereco.endCidade);
    printf("\nEstado: %s",hotel.endereco.endEstado);
    printf("\nCEP: %d",hotel.endereco.endCep);
    printf("\nComplemeto: %s",hotel.endereco.endComplemento);
    printf("\n----------Horários------------");
    printf("\nCheck-In: às %s hora(s).",hotel.horario_checkIn);
    printf("\nCheck-Out: às %s hora(s).",hotel.horario_checkOut);
    printf("\n-------Margem de Lucro---------");
    printf("\nMargem de lucro: %d%% \n",hotel.margem_lucro);
}

void pesquisarHotel(){
    char *nomeFantasia;
    Hotel hotel;
    printf("Informe o nome fantasia: \n");
    fgets(nomeFantasia, 55, stdin);
    nomeFantasia[strcspn(nomeFantasia, "\n")] = 0;
    setbuf(stdin, NULL);
    hotel = buscarHotelPorNomeFantasia(nomeFantasia);
    if (hotel.codigo != -1){
        printf("Hotel encontrado! \n");
        printaInformacoesHotel(hotel);
        return;
    }
    printf("Hotel não encontrado! \n");
}
/**
* Tela do menu do hotel
* @param Usuario usuarioLogado: usuário autenticado no sistema
* @return
*/
void abrirMenuHotel(Usuario usuarioLogado){
    int opcao = 0;
    char *nomeFantasia;
    do {
        printf("====================\n");
        printf("|       HOTEL     |\n");
        printf("====================\n");
        printf("1- Cadastrar \n");
        printf("2- Listar \n");
        printf("3- Pesquisar \n");
        printf("4- Atualizar \n");
        printf("5- Retornar \n");
        printf("Escolha uma opção: ");
        scanf("%d%*c", &opcao);
        switch(opcao) {
            case 1: 
                cadastrar_hotel();
                break;
            case 2:
                listar_hotel();
                break;
            case 3:
                pesquisarHotel();
                break;
            case 4:
                editar_hotel();
                break;
            case 5:
                mostrarRetornandoMenu();
                break;
            default: 
                mostrarMensagemErroMenu(); 
                break;
        }
    } while(opcao != 5);
    mostrarMenu(usuarioLogado);
}