/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#pragma once
#include "../models/usuarioModel.h"
#include "../models/funcoesModel.h"
#include "../structs/moduloCadastroStructs.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hotelView.h"
#include "produtosView.h"
#include "categoriaView.h"
#include "acomodacaoView.h"
#include "reservaView.h"
#include "hospedeView.h"
#include "fornecedorView.h"
#include "checkinView.h"
<<<<<<< HEAD
=======
#include "exportacaoView.h"
>>>>>>> 4de6d50a42c8b2c66581d6db27d80189ca32e8b6
#include "SaidaDeProdutosViews.h"
#include "caixaView.h"

Usuario usuarioLogado;

/**
* Tela de login do sistema
* @param
* @return: Usuario logado no sistema, se as credênciais estiverem erradas retorna usuCodigo = -1
*/
Usuario login () {
    char usuUsuario[25];
    char usuSenha[25];
    printf("===================\n");
    printf("|       LOGIN     |\n");
    printf("===================\n");
    //lendo as credenciais do login
    printf("Informe seu nome de usuário: \n");
    fgets(usuUsuario, 24, stdin);
    usuUsuario[strcspn(usuUsuario, "\n")] = 0;
    setbuf(stdin, NULL);
    printf("Informe sua senha: \n");
    fgets(usuSenha, 24, stdin);
    usuSenha[strcspn(usuSenha, "\n")] = 0;
    setbuf(stdin, NULL);

    usuarioLogado = buscarUsuarioPorNomeUsuario(usuUsuario);
    //chamando o método de autenticação
    if (strcmp(usuarioLogado.usuSenha, usuSenha) != 0){
        Usuario usuarioVazio;
        usuarioVazio.usuCodigo = -1;
        return usuarioVazio;
    }
    return usuarioLogado;
}

/**
* Tela de cadastro do usuário
* @param
* @return: o usuário cadastrado no sistema
*/
Usuario cadastrarUsuarioView () {
    Usuario usuario;
    int tipoArquivo;
    printf("===============================\n");
    printf("|       CADASTRAR USUÁRIO     |\n");
    printf("===============================\n");
    printf("\n Insira o nome:");
    setbuf(stdin, NULL);
    fgets(usuario.usuNome, 24, stdin);
    usuario.usuNome[strcspn(usuario.usuNome, "\n")] = 0;
    setbuf(stdin, NULL);
    printf("\n Insira o usuário:");
    fgets(usuario.usuUsuario, 24, stdin);
    usuario.usuUsuario[strcspn(usuario.usuUsuario, "\n")] = 0;
    setbuf(stdin, NULL);
    printf("\n Insira a senha:");
    fgets(usuario.usuSenha, 24, stdin);
    usuario.usuSenha[strcspn(usuario.usuSenha, "\n")] = 0;
    setbuf(stdin, NULL);
    printf("\n Permissões: \n");
    printf("1 - Administrador \n");
    printf("2 - Funcionário \n");
    printf("Insira o número da permissão que deseja dar a esse usuário: ");
    scanf("%d%*c",&usuario.usuPermissao);

    if(cadastrarUsuario(usuario) == 1){
        printf("Usuário cadastrado com sucesso! \n");
        abrirMenuUsuario();
    }
}

/**
* Tela de listagem dos usuários cadastrados no sistema
* @param
* @return void
*/
void listarUsuariosView () {
    Usuario *usuarios;
    usuarios = listarUsuarios();
    FILE *file = NULL;
    file = fopen("persist/usuario.txt","r");

    int size = qtdeRegistrosUsuario();

    printf("=================================\n");
    printf("|       LISTAGEM DOS USUÁRIOS     |\n");
    printf("=================================\n");
    if (usuarios != NULL) {
        for (int i = 0; i < size; i++) {
            printf("\n\n USUÁRIO %d:", usuarios[i].usuCodigo);
            printf("\n Nome: %s", usuarios[i].usuNome);
            printf("\n Usuário: %s", usuarios[i].usuUsuario);
            printf("\n Permissão:");
            if (usuarios[i].usuPermissao == 1) {
                printf("Administrador \n");
            } else if (usuarios[i].usuPermissao == 2) {
                printf("Funcionário \n");
            }
        }
    } else {
        printf("\n Nenhum usuário cadastrado");
    }
    abrirMenuUsuario();
}

/**
* Tela de exclusão de algum usuário cadastrado no sistema
* @param
* @return void
*/
void deletarUsuarioView () {
    char usuUsuario[25];
    int tipoArquivo;
    printf("=============================\n");
    printf("|       DELETAR USUÁRIO     |\n");
    printf("=============================\n");
    printf("\n Informe o nome do usuário que deseja exluir: ");
    setbuf(stdin, NULL);
    fgets(usuUsuario, 24, stdin);
    usuUsuario[strcspn(usuUsuario, "\n")] = 0;
    setbuf(stdin, NULL);

    if (deletarUsuario(usuUsuario) == 1) {
        printf("Usuário deletado com sucesso! \n");
    } else {
        printf("Erro ao deletar o usuário desejado! \n");
    }
    abrirMenuUsuario();
}

/**
* Método auxiliar na edição do usuário
* @param char *parametro: nome do parâmetro
* @return int: a opção escolhida pelo usuário
*/
int auxiliar (char *parametro) {
    int opcao;
    printf("Deseja atualizar o %s ?\n", parametro);
    printf("1- SIM       2- NÃO \n");
    scanf("%d%*c",&opcao);
    return opcao;
}

/**
* Tela de edição de algum usuário cadastrado no sistema
* @param
* @return void
*/
void atualizarUsuarioView () {
    char usuUsuario[25];
    int tipoArquivo;
    printf("===============================\n");
    printf("|       ATUALIZAR USUÁRIO     |\n");
    printf("===============================\n");
    printf("\n Informe o usuário que deseja atualizar: ");
    setbuf(stdin, NULL);
    fgets(usuUsuario, 24, stdin);
    usuUsuario[strcspn(usuUsuario, "\n")] = 0;
    setbuf(stdin, NULL);

    Usuario usuario = buscarUsuarioPorNomeUsuario(usuUsuario);

    if (usuario.usuCodigo != -1) {
        if(auxiliar("nome\0") == 1){
            printf("Informe o novo nome: ");
            fgets(usuario.usuNome, 100, stdin);
            usuario.usuNome[strcspn(usuario.usuNome, "\n")] = 0;
            setbuf(stdin, NULL);
        }
        if (auxiliar("usuário\0") == 1) {
            printf("Informe o novo usuário: ");
            fgets(usuario.usuUsuario, 24, stdin);
            usuario.usuUsuario[strcspn(usuario.usuUsuario, "\n")] = 0;
            setbuf(stdin, NULL);
        }
        if (auxiliar("senha\0") == 1) {
            printf("Informe a nova senha: ");
            fgets(usuario.usuSenha, 24, stdin);
            usuario.usuSenha[strcspn(usuario.usuSenha, "\n")] = 0;
            setbuf(stdin, NULL);
        }
        if (auxiliar("permissão\0") == 1) {
            printf("Informe a nova permissão: \n");
            printf("1- Administrador \n");
            printf("2- Funcionário \n");
            scanf("%d%*c", &usuario.usuPermissao);
        }
        Usuario usuarioAtualizado;
        usuarioAtualizado = atualizarUsuario(usuario, usuUsuario);
        if (usuarioAtualizado.usuCodigo != -1) {
            printf("Usuário atualizado com sucesso! \n");
            abrirMenuUsuario();
            return;
        }
    }
    printf("\n Usuário não encontrado! ");
    abrirMenuUsuario();
}

/**
* Tela de pesquisa de algum usuário pelo usuUsuario
* @param
* @return void
*/
void pesquisarUsuarioView () {
    char usuUsuario[25];
    printf("===============================\n");
    printf("|       PESQUISAR USUÁRIO     |\n");
    printf("===============================\n");
    printf("Informe o usuário que deseja pesquisar: \n");
    setbuf(stdin, NULL);
    fgets(usuUsuario, 24, stdin);
    usuUsuario[strcspn(usuUsuario, "\n")] = 0;
    setbuf(stdin, NULL);
    Usuario usuarioExistente;
    usuarioExistente = buscarUsuarioPorNomeUsuario(usuUsuario);

    if(usuarioExistente.usuCodigo != -1){
        printf("Usuário encontrado! Segue informações: \n");
        printf("\n\n USUÁRIO %d:", usuarioExistente.usuCodigo);
        printf("\n Nome: %s", usuarioExistente.usuNome);
        printf("\n Usuário: %s", usuarioExistente.usuUsuario);
        printf("\n Permissão:");
        if (usuarioExistente.usuPermissao == 1) {
            printf("Administrador \n");
        } else if (usuarioExistente.usuPermissao == 2) {
            printf("Funcionário \n");
        }
        printf(" \n \n");
        abrirMenuUsuario();
        return;
    }
    printf("\n Usuário não encontrado! \n");
    abrirMenuUsuario();
}

/**
* Tela do menu do usuário
* @param
* @return void
*/
void abrirMenuUsuario () {
    int opcao;
     printf("=====================\n");
    printf("|        USUÁRIO     |\n");
    printf("======================\n");
    printf("1- Cadastrar \n");
    printf("2- Listar \n");
    printf("3- Pesquisar \n");
    if(usuarioLogado.usuPermissao == 1){
        printf("4- Atualizar \n");
        printf("5- Excluir \n");
    }
    printf("6- Retornar \n");
    printf("Escolha uma opção: ");
    scanf("%d%*c",&opcao);

    switch(opcao){
        case 1:
            cadastrarUsuarioView();
            break;
        case 2:
            listarUsuariosView();
            break;
        case 3:
            pesquisarUsuarioView();
            break;
        case 4:
            if(usuarioLogado.usuPermissao == 1)
                atualizarUsuarioView();
            break;
        case 5:
            if(usuarioLogado.usuPermissao == 1)
                deletarUsuarioView();
            break;
        case 6:
            mostrarMenu(usuarioLogado);
            break;
        default:
            mostrarMensagemErroMenu();
            break;
    }
}

/**
* Chamando os menus secundários
* @param char opcao: número do menu secundário que deseja abrir
* @return void
*/
void abrirMenusSecundarios (int opcao) {
    do{
        switch (opcao) {
            case 1:
                abrirMenuUsuario();
                break;
            case 2:
                abrirMenuHotel(usuarioLogado);
                break;
            case 3:
                abrirMenuAcomodacao(usuarioLogado);
                break;
            case 4:
                menuHospede(usuarioLogado);
                break;
            case 5:
                abrirMenuCategoria(usuarioLogado);
                break;
            case 6:
                abrirMenuProduto(usuarioLogado);
                break;
            case 7:
                menuFornecedor(usuarioLogado);
                break;
            case 8:
                mostrarRetornandoMenu();
                break;
            default:
                mostrarMensagemErroMenu();
                break;
        }
    } while(opcao != 8);
    mostrarMenu(usuarioLogado);
}

/**
* Chamando os menus secundários de reserba
* @param char opcao: número do menu secundário que deseja abrir
* @return void
*/
void abrirMenuReserva (int opcao) {
    do{
        switch (opcao) {
            case 1:
                cadastrarReservaView();
                mostrarMenu(usuarioLogado);
                break;
            case 2:
                mostrarAcomodacoesDisponiveis();
                mostrarMenu(usuarioLogado);
                break;
            case 3:
                consultarReservaView();
                mostrarMenu(usuarioLogado);
                break;
            case 4:
                cancelarReservaView();
                mostrarMenu(usuarioLogado);
                break;
            case 5:
                listarReservas();
                mostrarMenu(usuarioLogado);
                break;
            case 6:
                abrirMenuCheckin(usuarioLogado);
                mostrarMenu(usuarioLogado);
                break;
            case 7:
                checkout();
                mostrarMenu(usuarioLogado);
                break;
            case 8:
                mostrarRetornandoMenu();
                break;
            default:
                mostrarMensagemErroMenu();
                break;
        }
    } while(opcao != 8);
    mostrarMenu(usuarioLogado);
}

void abrirMenuTransacoes(int opcao) {
    do{
        switch (opcao) {
            case 1:
                 abrirMenuSaidaDeProdutos();
                   mostrarMenu(usuarioLogado);
                break;
            case 2:
                abrirMenuControleCaixa(usuarioLogado);
                break;
            case 3:
            case 5:
                // contas a receber
                break;
            case 6:
                entrada_produtos();
            case 4:
            default:
                mostrarMensagemErroMenu();
                break;
        }
    } while(opcao != 4);
    mostrarMenu(usuarioLogado);
}

/*
* Tela do Menu Principal
* @param Usuario logado no sistenma
* @return void
*/
void mostrarMenu (Usuario user) {
    int menu1,menuEscolha;
    printf("=================\n");
    printf("|       MENU     |\n");
    printf("==================\n");
    //Verificando se o usuário está logado no sistema
    if (user.usuCodigo != -1) {
        printf("1- Cadastro e Gestão de Dados \n");
        printf("2- Reservas \n");
        printf("3- Transações \n");
        printf("4- Relatórios \n");
        printf("5- Importação/exportação de dados \n");
        printf("6- Sair \n");
        printf("Escolha uma opção: ");
        scanf("%d%*c",&menu1);

        switch(menu1){
            case 1:
                printf("========================================\n");
                printf("|       CADASTRO E GESTÃO DE DADOS     |\n");
                printf("========================================\n");
                printf("1- Usuário \n");
                printf("2- Hotel \n");
                printf("3- Acomodações \n");
                printf("4- Hóspedes \n");
                printf("5- Categoria de Acomodação \n");
                printf("6- Produtos \n");
                printf("7- Fornecedores\n");
                printf("8- Retornar\n");
                printf("Escolha uma opção: ");
                scanf("%d%*c",&menuEscolha);
                abrirMenusSecundarios(menuEscolha);
                break;
            case 2:
                printf("=====================\n");
                printf("|       RESERVA     |\n");
                printf("=====================\n");
                printf("1- Cadastrar \n");
                printf("2- Consultar acomodações disponíveis \n");
                printf("3- Consultar reserva realizada \n");
                printf("4- Cancelar reserva \n");
                printf("5- Listar todas as reservas registradas\n");
                printf("6- Check-in \n");
                printf("7- Check-out \n");
                printf("8- Retornar\n");
                printf("Escolha uma opção: ");
                scanf("%d%*c",&menuEscolha);
                abrirMenuReserva(menuEscolha);
                break;
            case 3:
                printf("========================\n");
                printf("|       TRANSAÇÕES     |\n");
                printf("========================\n");
                printf("1- Saída de produtos \n");
                printf("2- Controle de Caixa \n");
                printf("3- Entrada de produtos industrializados \n");
                printf("4- Retornar\n");
                printf("Escolha uma opção: ");
                scanf("%d%*c", &menuEscolha);
                abrirMenuTransacoes(menuEscolha);
                break;
            case 4:
                printf("======================\n");
                printf("|       FEEDBACK     |\n");
                printf("======================\n");
                printf("1- Gerar Relatório \n");
                printf("2- Retornar\n");
                break;
            case 5:
                printf("===========================================\n");
                printf("|       IMPORTAÇÃO/EXPORTACAO DE DADOS     |\n");
                printf("============================================\n");
                printf("1- Exportar dados\n");
                printf("2- Importar dados\n");
                printf("3- Retornar\n");
                scanf("%d%*c", &menuEscolha);
                if (menuEscolha == 1) {
                    exportarDadosView();
                    mostrarMenu(usuarioLogado);
                }
                if (menuEscolha == 2) {
                    importarDadosView();
                    mostrarMenu(usuarioLogado);
                }
                break;
            case 6:
                usuarioLogado.usuCodigo = -1;
                printf("Saindo... \n");
                break;
            default:
                mostrarMensagemErroMenu();
                break;
        }
        return;
    }
    mostrarMensagemUsuarioDeslogado();
}
