/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#pragma once
#include <stdio.h>
#include <stdlib.h>
#include "../models/categoriaModel.h"
#include "../models/funcoesModel.h"

/**
* Tela do menu de categoria
* @param Usuario usuarioLogado: usuário autenticado no sistema
* @return
*/
void abrirMenuCategoria(Usuario usuarioLogado){
    int opcao = 0;
    do {
        printf("========================\n");
        printf("|       CATEGORIA      |\n");
        printf("========================\n");
        printf("1- Cadastrar \n");
        printf("2- Listar \n");
        printf("3- Pesquisar \n");
        printf("4- Atualizar \n");
        printf("5- Deletar \n");
        printf("6- Retornar \n");
        printf("Escolha uma opção: ");
        scanf("%d%*c", &opcao);

        switch(opcao){
            case 1: 
                criarCategoria();
                break;
            case 2:
                mostrarCategorias();
                break;
            case 3:
                pesquisarCategorias();
                break;
            case 4:
                editarCategoria();
                break;
            case 5:
                excluirCategoria();
                break;
            case 6:
                mostrarRetornandoMenu();
                break;
            default: 
                mostrarMensagemErroMenu(); 
                break;
        }
    } while(opcao != 6);
    mostrarMenu(usuarioLogado);
}