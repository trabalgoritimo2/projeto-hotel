/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#pragma once
#include <stdio.h>
#include <stdlib.h>
#include "../models/categoriaModel.h"
#include "../models/funcoesModel.h"
#include "../models/controleCaixaModel.h"

/**
* Introdução ao menu do controle de caixa.
* @param Usuario usuarioLogado: usuário autenticado no sistema.
* @return
*/
void abrirMenuControleCaixa(Usuario usuarioLogado){
    int opcao = 0;
    do {
        printf("===============================\n");
        printf("|       CONTROLE DE CAIXA     |\n");
        printf("===============================\n");
        printf("1 - Saldo.\n");
        printf("2 - Histórico de Lançamentos/Retiradas.\n"); 
        printf("3 - Atualizar valor caixa \n");
        printf("4 - Retornar \n");
        printf("Escolha uma opção: ");
        scanf("%d%*c", &opcao);
        switch(opcao) {
            case 1: 
                mostrarSaldoCaixa();
                
                break;
            case 2:
                mostrarHistoricoLancRetirada();
                break;
            case 3:
                verificaValidadePagamento();
                break;
            case 4:
                mostrarRetornandoMenu();
                break;
            default: 
                mostrarMensagemErroMenu(); 
                break;
        }
    } while(opcao != 4);
    mostrarMenu(usuarioLogado);
}