/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/

#pragma once
#include <stdio.h>
#include <stdlib.h>
#include "produtosView.h"
#include "../models/SaidaDeProdutosModel.h"
#include "../models/funcoesModel.h"
#include "../models/controleCaixaModel.h"
#include "../models/produtosModel.h"
#include "../models/hospedeModel.h"
#include "../models/produtosModel.h"
#include "../models/configuracaoModel.h"

void abrirMenuSaidaDeProdutos(){
    int opcao;
    do {
        printf("===============================\n");
        printf("|       SAÍDA DE PRODUTOS     |\n");
        printf("===============================\n");
        printf(" 1- Realizar Venda \n");
        printf(" 2- Listar Vendas\n");
        printf(" 3- Sair\n");
        printf("   Escolha uma opção: ");
        scanf("%d%*c", &opcao);
        do
        {
            switch (opcao)
        {
        case 1:
        listar_produtos();
        printf("\nEscolha o Produto a qual foi vendido (Código):");
        scanf(" %d*c", &prod_id);
        printf("\n Quantidade de produtos vendidos:");
        scanf(" %d*c", &qde);
        printf("\nData da venda:");
        scanf( " %d*c", &data);
        printf("\nForma de pagamento:\n");
        printf("1 - A vista\n");
        printf("2 - Adicionar a Conta\n");
        scanf(" %d*c", &formPag);
            if(formPag == 1){
            int resp = addValorAoCaixaController(prod_id, qde);
             if(resp == 0){
                        printf("\nVenda realizada com sucesso   ");
                    }else {
                        printf("\nErro ao cadastrar venda");
                    }
            } else if(formPag ==2) {
                    listarHospedeView();
                    printf("\nAdicionar o Produto a qual conta(Codigo do Hospede):");
                    int hosp_id;
                    scanf(" %d*c", &hosp_id);
                    int resp = addProdutoEmContaHospController(prod_id, hosp_id, qde, data);
                    if(resp == 1){
                        printf("\nVenda realizada com sucesso   ");
                    }else {
                        printf("\nErro ao cadastrar venda");
                    }
            }else {
                printf("Comando Invalido\n");
            }
            break;
            case 2:
                listarVendas();
                break;
            default:
                    return;
                break;
            } 
     }while (opcao != 4);
}

void realizarVenda() {
    Venda_Produtos *venda_Produtos = NULL;
    Venda venda;
    Hospede hospede;
    int index = 0;
    int formaPagamento;
    float valorTotal = 0;
    char cpf[14];
    venda_Produtos = malloc(sizeof(Venda_Produtos));
    int isAdicionarProduto = 1;
    Historico_Caixa historico_Caixa;
    Prod_Disp produto;
    int tipoArquivo = opcaoArmazenamentoUsuario();
    printf("Informe o CPF do hóspede: \n");
    scanf("%s",cpf);
    hospede = encontrarHospedePorCPF(cpf);
    if(hospede.hospCodigo == -1) {
        printf("Hospede não encontrado! \n");
        return;
    }
    listarProdutos();
    do {
        printf("\n Insira o código do produto:");
        scanf(" %d%*c", &venda_Produtos[index].id_produto);
        printf("\n Quantidade de produtos:");
        scanf(" %d%*c", &venda_Produtos[index].quantidade);
        printf("Deseja adicionar mais um produto à venda: 1- Sim  2-Não");
        scanf("%d",&isAdicionarProduto);
        if(isAdicionarProduto == 1){
            venda_Produtos = realloc(venda_Produtos, sizeof(Venda_Produtos)*(index + 1));
            index++;
        }
       
    } while(isAdicionarProduto == 1);

    if(tipoArquivo == 1){
        venda.codigo = sequenceArquivoBinario("persist/seq_venda.bin");
    }else{
        venda.codigo = sequenceArquivoTXT("persist/seq_venda.txt");
    }

    for(int i = 0;i<index+1;i++){
       produto = buscarProdutoPorCodigo(venda_Produtos[i].id_produto);
       valorTotal += produto.preco_venda * venda_Produtos[i].quantidade;
       produto.estoque =   produto.estoque - venda_Produtos[i].quantidade;
       atualizarProduto(produto);
    }  
    venda.valor_total = valorTotal;
    venda.data = gerarDataAtual();
    venda.id_hospede = hospede.hospCodigo;
    printf("Valor total da venda: R$%.2f \n",valorTotal);
    printf("\nForma de pagamento:\n");
    printf("1 - À vista\n");
    printf("2 - Adicionar à Conta\n");
    scanf(" %d", &formaPagamento);

    if(formaPagamento == 1) {
        historico_Caixa = montaHistoricoCaixa(valorTotal,"entrada",venda.codigo,"venda");
        if(cadastrarHistorico(historico_Caixa) == 1){
            printf("Pagamento realizado com sucesso! \n");
        }
        if(cadastrarVenda(venda,venda_Produtos,index+1) == 1){
            printf("Venda realizada com sucesso! \n");
        }
    } else if(formaPagamento == 2) {
        Conta_Hospede contaHospede;
        contaHospede = buscarContaHospede(hospede.hospCodigo);
        if(contaHospede.codigo != -1){
            contaHospede.valorTotal += valorTotal;
            atualizarContaHospede(contaHospede);
        } else {
            contaHospede.id_hospede = hospede.hospCodigo;
            contaHospede.valorTotal = valorTotal;
            if(cadastrarContaHospede(contaHospede) == 1){
                printf("Adicionado a conta do cliente! \n");
            } 
        }

<<<<<<< HEAD
            case 3:
            listarHospedeView();
                    printf("\nDar baixa em qual conta (Codigo do Hospede):");
                    int id = 0;
                    scanf(" %d*c", &id);
                deletarVendasHosp(id);
            break;
        default:
            break;
=======
        if(cadastrarVenda(venda,venda_Produtos,index+1) == 1){
            printf("Venda realizada com sucesso! \n");
>>>>>>> 4de6d50a42c8b2c66581d6db27d80189ca32e8b6
        }
    }
}