/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#pragma once

#include <stdio.h>
#include <stdlib.h>
#include "../models/reservaModel.h"
#include "../models/hospedeModel.h"
#include "../models/acomodacaoModel.h"
#include "../models/controleCaixaModel.h"
#include "../models/saidaDeProdutosModel.h"

/**
* Tela de cadastro da reserva
* @param
* @return
*/
void cadastrarReservaView() {
    Reserva reserva;
    reserva.resAcomodacaoId = 0;
    reserva.resHospedeId = 0;
    int catCodigo;
    int controle = 0;
    char hosNome[50];
    int hosID = 0;
    int qtdeCategorias = contarCategorias();
    Categoria* categorias = carregarCategorias(qtdeCategorias);
    printf("===============================\n");
    printf("|       CADASTRAR RESERVA     |\n");
    printf("===============================\n");

    // encerra o programa se não houverem categorias cadastradas
    if (qtdeCategorias <= 0) {
        printf("\n Nenhuma categoria cadastrada! \n");
        printf("Cadastre uma categoria e uma acomodação antes de fazer uma reserva.\n");
        free(categorias);
        return;
    }

    //verifica se hospede existe
    int hospedeEncontrado = 0;
    do {
        printf("Informe o nome do hóspede: \n");
        inputString(hosNome, 50);
        Hospede hospede = encontrarHospedePorNome(hosNome);

        if (hospede.hospCodigo == -1) {
            printf("Não foi encontrado um hóspede com este nome. Deseja tentar novamente? 1-Sim; 2-Não: \n");
            int escolhaContinuar = 1;
            scanf("%d", &escolhaContinuar);
            if (escolhaContinuar == 2) {
                free(categorias);
                return;
            }
        }
        else {
            hosID = hospede.hospCodigo;
            hospedeEncontrado = 1;
        }
    } while (!hospedeEncontrado);

    reserva.resHospedeId = hosID;
    // dados da reserva e acomodação
    do {
        Data dataInicio;
        Data dataFim;
        controle = 0;

        printf("Informe a data de inicio \n");
        printf("Dia:");
        scanf("%d", &dataInicio.dia);
        printf("Mês:");
        scanf("%d", &dataInicio.mes);
        printf("Ano:");
        scanf("%d", &dataInicio.ano);
        printf("Informe a data final \n");
        printf("Dia:");
        scanf("%d", &dataFim.dia);
        printf("Mês:");
        scanf("%d", &dataFim.mes);
        printf("Ano:");
        scanf("%d", &dataFim.ano);
        printf("************************ \n");

        reserva.resDataInicio = dataInicio;
        reserva.resDataFim = dataFim;

        // estrutura de parametros que auxilia a seleção de acomodações
        // pesquisa por data é selecionada por padrão
        ParamPesq parametros;
        parametros.flags = 0;
        parametros.dtInicial = dataInicio;
        parametros.dtFinal = dataFim;
        parametros.flags |= ParData;

        int escolha = 0;
        printf("Deseja pesquisar acomodações por categoria? 1-SIM; 2-NÃO: ");
        scanf("%d%*c", &escolha);
        if (escolha == 1) {
            for (int i = 0; i < qtdeCategorias; i++) {
                printf("\n\n CATEGORIA %d:", categorias[i].catCodigo);
                printf("\n Descrição: %s", categorias[i].catDescricao);
                printf("\n Valor da diária: %f", categorias[i].catValorDiaria);
                printf("\n Quantidade de pessoas: %d", categorias[i].catQuantidadePessoas);
            }
            printf("\n");
            printf("Digite o código da categoria escolhida\n");
            scanf("%d%*c", &parametros.categCodigo);
            parametros.flags |= ParCategCodigo;
        }

        printf("Deseja pesquisar acomodações por quantidade de pessoas? 1-SIM; 2-NÃO: ");
        scanf("%d%*c", &escolha);
        if (escolha == 1) {
            printf("Digite a quantidade de pessoas que ficarão no quarto\n");
            scanf("%d%*c", &parametros.qtPessoas);
            parametros.flags |= ParQtPessoas;
        }

        printf("Deseja pesquisar acomodações por caracteristicas? 1-SIM; 2-NÃO: ");
        scanf("%d%*c", &escolha);
        if (escolha == 1) {
            printf("Digite a caracteristica buscada: ");
            fgets(parametros.caracteristicas, 99, stdin);
            parametros.caracteristicas[strcspn(parametros.caracteristicas, "\n")] = 0;
            setbuf(stdin, NULL);
            parametros.flags |= ParCaracteristicas;
        }

        printf("Deseja pesquisar acomodações por preço da diaria? 1-SIM; 2-NÃO: ");
        scanf("%d%*c", &escolha);
        if (escolha == 1) {
            printf("Digite o valor máximo desejado: ");
            scanf("%f%*c", &parametros.valorMaximo);
            parametros.flags |= ParValorMaximo;
        }

        // carrega uma lista de acomodações que atende os padrões passados
        int buscaCounter = 0;
        Acomodacao* acomodacoesEncontradas = filtrarAcomodacaoPorFlags(&buscaCounter, parametros);

        if (buscaCounter > 0) {
            printf("As acomodações disponiveis são:\n");
            escreverAcomodacao(acomodacoesEncontradas, buscaCounter);
            int codigoDaAcomodacao = 0;
            printf("\nDigite o código da acomodação escolhida: ");
            scanf("%d%*c", &codigoDaAcomodacao);

            // verifica se o código digitado é de uma acomodação filtrada válida
            for (int i=0; i<buscaCounter; i++) {
                if (codigoDaAcomodacao == acomodacoesEncontradas[i].acoCodigo) {
                    reserva.resAcomodacaoId = codigoDaAcomodacao;
                    break;
                }
            }

            if (reserva.resAcomodacaoId != 0) {
                if (cadastrarReserva(reserva) == 1) {
                    printf("Reserva realizada com sucesso! \n");
                    free(categorias);
                    free(acomodacoesEncontradas);
                    return;
                } else {
                    printf("Houve um problema ao tentar realizar a reserva! \n");
                    free(categorias);
                    free(acomodacoesEncontradas);
                    return;
                }
            }
            else {
                printf("A acomodação com código digitado não está disponível\n");
                free(categorias);
                free(acomodacoesEncontradas);
                return;
            }
        }
        else {
            printf("Não foi encontrada nenhuma acomodação nestas circunstâncias\n");
            printf("Deseja tentar novamente? 1-SIM; 2-NÃO\n");
            scanf("%d%*c", &controle);
        }

        free(acomodacoesEncontradas);
    } while(controle == 1);

    free(categorias);
    return;
}

/**
* Tela de consultar de reserva já realizada
* @param
* @return
*/
void consultarReservaView () {
    Data resDataInicio;
    Data resDataFim;
    Reserva reserva;
    Acomodacao acomodacao;
    Hospede hospede;
    char nomeHospede[50];
    printf("===============================\n");
    printf("|       CONSULTAR RESERVA     |\n");
    printf("===============================\n");
    printf("Informe data início \n");
    printf("Dia:");
    scanf("%d",&resDataInicio.dia);
    printf("Mês:");
    scanf("%d",&resDataInicio.mes);
    printf("Ano:");
    scanf("%d",&resDataInicio.ano);
    printf("Informe data final \n");
    printf("Dia:");
    scanf("%d",&resDataFim.dia);
    printf("Mês:");
    scanf("%d",&resDataFim.mes);
    printf("Ano:");
    scanf("%d",&resDataFim.ano);
    printf("Informe o nome do hóspede: \n");
    scanf("%s",nomeHospede);
    hospede = encontrarHospedePorNome(nomeHospede);
    if(hospede.hospCodigo != -1) {
        reserva = buscarReservaRealizadas(resDataInicio, resDataFim, hospede.hospCodigo);
        if(reserva.resCodigo != -1){
            acomodacao = buscarAcomodacaoReserva(reserva.resAcomodacaoId);
            printf("Reserva encontrada! Segue informações: \n");
            printf("=================== \n");
            printf("RESERVA %d: \n", reserva.resCodigo);
            printf("Data início: %d-%d-%d \n",reserva.resDataInicio.dia,reserva.resDataInicio.mes,reserva.resDataInicio.ano);
            printf("Data fim: %d-%d-%d \n",reserva.resDataFim.dia,reserva.resDataFim.mes,reserva.resDataFim.ano);
            printf("Acomodação escolhida %d: \n", acomodacao.acoCodigo);
            printf("    Características: %s \n", acomodacao.acoCaracteristicas);
            printf("    Descrição: %s \n", acomodacao.acoDescricao);
            printf("\n");
            printf("=================== \n");
        }
        return;
    }
    printf("Hospede não encontrado! \n");
}

/**
* Tela de exibir acomodações disponíveis
* @param
* @return
*/
void mostrarAcomodacoesDisponiveis() {
    Reserva reserva;
    int catCodigo;
    int qtdeCategorias = contarCategorias();
    Categoria* categorias = carregarCategorias(qtdeCategorias);
    printf("Informe data inicio \n");
    printf("Dia:");
    scanf("%d",&reserva.resDataInicio.dia);
    printf("Mês:");
    scanf("%d",&reserva.resDataInicio.mes);
    printf("Ano:");
    scanf("%d",&reserva.resDataInicio.ano);
    printf("Informe data final \n");
    printf("Dia:");
    scanf("%d",&reserva.resDataFim.dia);
    printf("Mês:");
    scanf("%d",&reserva.resDataFim.mes);
    printf("Ano:");
    scanf("%d",&reserva.resDataFim.ano);
    printf("************************ \n");
    printf("Categorias de acomodação: \n");
    if (qtdeCategorias > 0) {
        for (int i = 0; i < qtdeCategorias; i++) {
            printf("\n\n CATEGORIA %d:", categorias[i].catCodigo);
            printf("\n Descrição: %s", categorias[i].catDescricao);
            printf("\n Valor da diária: %f", categorias[i].catValorDiaria);
            printf("\n Quantidade de pessoas: %d \n", categorias[i].catQuantidadePessoas);
        }
    } else {
        printf("\n Nenhuma categoria cadastrada! \n");
    }
    printf("Informe o número da categoria: \n");
    scanf("%d",&catCodigo);
    printf("************************ \n");
    printf("Acomodações Disponíveis: \n");
    buscarAcomodacoesDisponiveis(reserva,catCodigo);
}

/**
* Tela para o cancelamento da reserva realizada
* @param
* @return
*/
void cancelarReservaView() {
    printf("==============================\n");
    printf("|       CANCELAR RESERVA     |\n");
    printf("==============================\n");
    int codigoReserva = 0;
    printf("Informe o código da reserva a ser cancelada: ");
    scanf("%d%*c", &codigoReserva);
    cancelarReservaPorCodigo(codigoReserva);
}

void salvarReserva(Reserva reserva) {
    int size = 0;
    Reserva* reservas = carregarReservas(&size);

    for (int i=0;i<size;i++) {
        if(reservas[i].resCodigo == reserva.resCodigo) {
            reservas[i] = reserva;
        }
    }
    salvarReservas(reservas, size);
    free(reservas);
}

void checkout() {
    char hosCpf[11];
    Hospede hospede;
    Reserva reserva;
    reserva.resCodigo = -1;
    Acomodacao acomodacao;
    Pagamento pagamento;
    Historico_Caixa historico_Caixa;
    Conta_Hospede contaHospede;
    float valorTotalCheckout = 0;
    int quantidadeDias;
    printf("======================\n");
    printf("|       CHECKOUT     |\n");
    printf("======================\n");
    printf("Insira o CPF do hóspede: \n");
    inputString(hosCpf,12);
    hospede = encontrarHospedePorCPF(hosCpf);
    if (hospede.hospCodigo != -1) {
        int reservasEncontradas = 0;
        Reserva* reservas = encontrarReservasDoHospede(hospede.hospCodigo, &reservasEncontradas);
        if (reservasEncontradas > 0) {
            printf("Nome do hóspede: %s \n", hospede.hospNome);
            printf("Reservas do hóspede:\n\n");
            for (int i=0; i<reservasEncontradas; i++) {
                printf(" Código: %d\n", reservas[i].resCodigo);
                acomodacao = buscarAcomodacaoPorCodigo(reservas[i].resAcomodacaoId);
                printf(" Acomodação: %d: %s\n",acomodacao.acoCodigo, acomodacao.acoDescricao);
                printf(" Data de inicio: %d/%d/%d\n", reservas[i].resDataInicio.dia, reservas[i].resDataInicio.mes, reservas[i].resDataInicio.ano);
                printf(" Data de término: %d/%d/%d\n", reservas[i].resDataFim.dia, reservas[i].resDataFim.mes, reservas[i].resDataFim.ano);
                printf("\n");
            }
            // definir qual o metodo de encontrar a reserva, e encontrar a tal reserva
            int idReserva = 0;
            printf("Digite o código da reserva: ");
            scanf("%d%*c", &idReserva);
            for (int i = 0; i < reservasEncontradas; i++) {
                if (reservas[i].resCodigo == idReserva) {
                    reserva.resCodigo = reservas[i].resCodigo;
                    reserva.resAcomodacaoId = reservas[i].resAcomodacaoId;
                    reserva.resStatus_pagamento = reservas[i].resStatus_pagamento;
                    reserva.resHospedeId = reservas[i].resHospedeId;
                    reserva.resDataFim.ano = reservas[i].resDataFim.ano;
                    reserva.resDataFim.mes = reservas[i].resDataFim.mes;
                    reserva.resDataFim.dia = reservas[i].resDataFim.dia;
                    reserva.resDataInicio.ano = reservas[i].resDataFim.ano;
                    reserva.resDataInicio.mes = reservas[i].resDataFim.mes;
                    reserva.resDataInicio.dia = reservas[i].resDataFim.dia;
                    reserva.resDataTime_chegada.ano = reservas[i].resDataTime_chegada.ano;
                    reserva.resDataTime_chegada.mes = reservas[i].resDataTime_chegada.mes;
                    reserva.resDataTime_chegada.dia = reservas[i].resDataTime_chegada.dia;
                    reserva.resDataTime_chegada.hora = reservas[i].resDataTime_chegada.hora;
                    reserva.resDataTime_chegada.minuto = reservas[i].resDataTime_chegada.minuto;
                    reserva.resDataTime_chegada.segundo = reservas[i].resDataTime_chegada.segundo;
                    reserva.resDataTime_saida.ano = reservas[i].resDataTime_saida.ano;
                    reserva.resDataTime_saida.mes = reservas[i].resDataTime_saida.mes;
                    reserva.resDataTime_saida.dia = reservas[i].resDataTime_saida.dia;
                    reserva.resDataTime_saida.hora = reservas[i].resDataTime_saida.hora;
                    reserva.resDataTime_saida.minuto = reservas[i].resDataTime_saida.minuto;
                    reserva.resDataTime_saida.segundo = reservas[i].resDataTime_chegada.segundo;
                    break;
                }
            }
            if(reserva.resCodigo != -1) {
                reserva.resDataTime_saida = gerarDataEHoraAtual();
                contaHospede = buscarContaHospede(hospede.hospCodigo);
                if(contaHospede.codigo != -1) {
                    valorTotalCheckout += contaHospede.valorTotal;
                }
                if (reserva.resStatus_pagamento == 2) {
                    if(acomodacao.acoCodigo != -1) {
                        quantidadeDias = calculaDiasEntreDatas(reserva.resDataTime_chegada, reserva.resDataTime_saida);
                        valorTotalCheckout += quantidadeDias * acomodacao.acoCategoria.catValorDiaria;
                    }
                }
                printf("Valor total do checkout (diárias + compras): R$%.2f\n",valorTotalCheckout);
                if(valorTotalCheckout > 0) {
                    printf("Forma de pagamento: \n");
                    printf("1- À vista \n");
                    printf("2- No cartão \n");
                    scanf("%d%*c",&pagamento.formaPagamento);
                    if(pagamento.formaPagamento == 2) {
                        printf("Deseja dar uma entrada? 1- sim   2- não \n");
                        scanf("%d%*c",&pagamento.isEntrada);
                        if(pagamento.isEntrada == 1) {
                            printf("Insira o valor da entrada: \n");
                            scanf("%f",&pagamento.valorEntrada);
                        }
                        printf("Deseja dividir em quantas vezes? \n");
                        scanf("%d",&pagamento.numeroParcelas);
                        printf("Data de vencimento \n");
                        printf("Dia:");
                        scanf("%d",&pagamento.dataVencimento.dia);
                        printf("Mês:");
                        scanf("%d",&pagamento.dataVencimento.mes);
                        printf("Ano:");
                        scanf("%d",&pagamento.dataVencimento.ano);
                        if(montaESalvaContaReceberPagar(pagamento,"diaria","aguardando_lancamento","entrada", valorTotalCheckout, reserva.resCodigo) == 1) {
                            reserva.resStatus_pagamento = 1;
                            salvarReserva(reserva);
                            printf("Pagamento realizado com sucesso! \n");
                        }
                    } else if(pagamento.formaPagamento == 1) {
                        historico_Caixa = montaHistoricoCaixa(valorTotalCheckout,"entrada",reserva.resCodigo,"diaria");
                        if(cadastrarHistorico(historico_Caixa) == 1) {
                            reserva.resStatus_pagamento = 1;
                             salvarReserva(reserva);
                            printf("Pagamento realizado com sucesso! \n");
                        }
                    }
                    if(contaHospede.codigo != -1 && apagarContaHospede(contaHospede) == 1){
                        printf("Conta do cliente apagada! \n");
                    }
                }
            }
        } else {
            printf("Não foram encontradas reservas para este hóspede nesta data.\n");
            free(reservas);
            return;
        }

    } else {
        printf("Hóspede não encontrado! \n");
    }
}