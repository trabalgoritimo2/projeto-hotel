/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#pragma once
#include <stdio.h>
#include <stdlib.h>
#include "../models/checkinModel.h"
#include "../models/funcoesModel.h"

void abrirMenuCheckin(Usuario usuarioLogado){
    int opcao = 0;
    do {
        printf("\n==================== \n");
        printf(" CHECK-IN \n");
        printf("==================== \n");
        printf("1- Realizar Check-in \n");
        
        printf("2- Retornar \n");
        printf("Escolha uma opção: ");
        scanf("%d%*c", &opcao);

        switch(opcao){
            case 1:
                fazerCheckin();
                break;
            case 2:
                mostrarRetornandoMenu();
                break;
            default: 
                mostrarMensagemErroMenu(); 
                break;
        }
    } while(opcao != 6);
    mostrarMenu(usuarioLogado);
}