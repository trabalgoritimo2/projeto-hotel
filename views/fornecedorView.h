/* INTEGRANTES:
*  Danielle Leal Silva;
*  Lucas de O. Souza Barbosa;
*  Tiago Meneses de Oliveira;
*  Marcos Vinícius Silva.
*/
#include <stdio.h>
#include <stdlib.h>
#include "../structs/moduloCadastroStructs.h"
#include "../models/fornecedorModel.h"
#include "../models/funcoesModel.h"

/**
* Tela de cadastro do fornecedor
* @param
* @return
*/
void cadastrarFornecedorView(){
    Fornecedor fornecedor;
    Endereco endereco;
    printf("==============================\n");
    printf("|   CADASTRAR FORNECEDOR     |\n");
    printf("==============================\n \n");
    printf("===== INFORMAÇÕES BÁSICAS =========\n");
    printf("Informe o nome:\n");
    scanf(" %s", fornecedor.forNomeFantasia);
    printf("Informe o email:\n");
    scanf(" %s", fornecedor.forEmail);
    printf("Informe o telefone:\n");
    scanf(" %d", &fornecedor.forTelefone);
    printf("======= DADOS DA EMPRESA =========\n");
    printf("Informe a Razao Social:\n");
    scanf(" %s", fornecedor.forRazaoSocial);
    printf("Informe o CNPJ:\n");
    scanf(" %s", fornecedor.forCnpj);
    printf("Informe a inscricao estadual:\n");
    scanf(" %d", &fornecedor.forInscricaoEstadual);
    printf("======== LOCALIDADE =========\n");
    printf("Informe a rua:\n");
    scanf(" %s", endereco.endRua);
    printf("Informe o número do endereço:\n");
    scanf(" %d", &endereco.endNumero);
    printf("Informe o Bairro:\n");
    scanf(" %s", endereco.endBairro);
    printf("Informe a Cidade:\n");
    scanf(" %s", endereco.endCidade);
    printf("Informe o estado:\n");
    scanf(" %s", endereco.endEstado);
    printf("Informe a CEP:\n");
    scanf(" %d", &endereco.endCep);
    printf("Informe o complemento do endereço:\n");
    scanf(" %s", endereco.endComplemento);
    fornecedor.forEndereco = endereco;
    int success = cadastrarFornecedorController(fornecedor);
    if (success == 1) {
        printf("Sucesso ao cadastrar Fornecedor\n");
        return;
    }
    printf("Erro ao cadastrar Fornecedor\n");
}

/*
* Tela atualização do fornecedor
* @param
* @return int: 1- Atualizado com sucesso  0- Houve erro
*/
int atualizarFornecedor() {
    printf("================================\n");
    printf("|     ATUALIZAR FORNECEDOR     |\n");
    printf("================================\n");
    Fornecedor fornecedor;
    Endereco endereco;
    int cod = 0;
    listarFornecedor();
    printf("Informe o código do fornecedor que deseja atualizar:\n");
    scanf(" %d", &cod);
    printf("===== INFORMAÇÕES BÁSICAS =========\n");
    printf("Informe o nome:\n");
    scanf(" %s", fornecedor.forNomeFantasia);
    printf("Informe o email:\n");
    scanf(" %s", fornecedor.forEmail);
    printf("Informe o telefone:\n");
    scanf(" %d", &fornecedor.forTelefone);
    printf("======= DADOS DA EMPRESA =========\n");
    printf("Informe a Razao Social:\n");
    scanf(" %s", fornecedor.forRazaoSocial);
    printf("Informe o CNPJ:\n");
    scanf(" %s", fornecedor.forCnpj);
    printf("Informe a inscricao estadual:\n");
    scanf(" %d", &fornecedor.forInscricaoEstadual);
    printf("======== LOCALIDADE =========\n");
    printf("Informe a rua:\n");
    scanf(" %s", endereco.endRua);
    printf("Informe o número do endereço:\n");
    scanf(" %d", &endereco.endNumero);
    printf("Informe o Bairro:\n");
    scanf(" %s", endereco.endBairro);
    printf("Informe a Cidade:\n");
    scanf(" %s", endereco.endCidade);
    printf("Informe o estado:\n");
    scanf(" %s", endereco.endEstado);
    printf("Informe a CEP:\n");
    scanf(" %d", &endereco.endCep);
    printf("Informe o complemento do endereço:\n");
    scanf(" %s", endereco.endComplemento);
    fornecedor.forEndereco = endereco;
    int success = atualizarFornecedorController( cod ,fornecedor);
    if (success == 1) {
         printf("Fornecedor atualizado com sucesso! \n");
         return success;
    }
    printf("Erro ao realizar a atualização do fornecedor \n");
    return 0;
}

/**
* Tela listagem do fornecedor
* @param
* @return
*/
void listarFornecedorView() {
    printf("================================\n");
    printf("|      LISTAR FORNECEDORES     |\n");
    printf("================================\n");
    listarForController();
}

/**
* Tela do menu do fornecedor
* @param Usuario usuarioLogado: usuário autenticado no sistema
* @return
*/
void menuFornecedor(Usuario usuarioLogado) {
    int opc =0;
    while (opc != 5) {
    printf("=========================\n");
    printf("|       FORNECEDOR      |\n");
    printf("=========================\n");
    printf("1- Cadastrar\n");
    printf("2- Listar\n");
    printf("3- Atualizar \n");
    printf("4- Deletar\n");
    printf("5- Retornar\n");
    scanf(" %d", &opc);
    switch(opc) {
        case 1:
            cadastrarFornecedorView();
            mostrarMenu(usuarioLogado);
            break;
        case 2:
            listarFornecedorView();
            mostrarMenu(usuarioLogado);
            break;
        case 3:
            atualizarFornecedor();
            mostrarMenu(usuarioLogado);
            break;
        case 4:
            mostrarMenu(usuarioLogado);
            break;
        case 5:
            mostrarRetornandoMenu();
            mostrarMenu(usuarioLogado);
            break;
        default:
            mostrarMensagemErroMenu();
            break;
        }
    }
}
